package objHandler


import (
	"MarsXserver/tcpserver"
	"MarsXserver/common"
	"MarsXserver/objMessages"
	"MarsXserver/redserver/app"
)

type SSCollectRatingsHandler struct{

}


func (handler *SSCollectRatingsHandler) Handle(connector *tcpserver.Connector, header interface{}, body interface{}) error{

	var err, errAll error
	var ok bool
	var hourIdx int64
	var offset, newOffset int
	var records []objMessages.SSRatingUserItem
	var req *objMessages.SSCollectRatingsRequest
	var rsp *objMessages.SSCollectRatingsResponse


	reqHeader := header.(*objMessages.ObjRequestHeader)

	suInf := connector.Server.GetParentContext()
	server, ok := suInf.(*app.RedServer)
	if !ok {
		errAll = common.ErrorLog("red server is not in parent context")
		goto output
	}

	req, ok = body.(*objMessages.SSCollectRatingsRequest)
	if ok == false{
		errAll = common.ErrorLog("body is not save records request")
		goto output
	}

	hourIdx = req.HourIdx
	offset = req.Offset


	records, newOffset, err =server.CollectRatings(hourIdx, offset)
	if err != nil{
		errAll = err
		goto output
	}


output:

	rsp = &objMessages.SSCollectRatingsResponse{
		Stat: 0,
		Offset:	newOffset,
		Records: records,
	}

	if errAll != nil{

		rsp.Stat = -1
	}

	if err := connector.WriteObjResponseMesage(reqHeader,0, rsp); err != nil {
		common.ErrorLog("write msg response error", err, " msg id:", int(objMessages.MessageId_Rating_Collect))
		return err
	}


	return nil

}































