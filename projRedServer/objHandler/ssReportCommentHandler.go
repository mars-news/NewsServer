package objHandler


import (
	"MarsXserver/tcpserver"
	"MarsXserver/common"
	"MarsXserver/objMessages"
	"MarsXserver/redserver/app"
)

type SSReportCommentHandler struct{

}


func (handler *SSReportCommentHandler) Handle(connector *tcpserver.Connector, header interface{}, body interface{}) error{

	var errAll, err error
	var ok bool
	var req *objMessages.SSReportCommentRequest
	var rsp *objMessages.SSReportCommentResponse
	var uid int32

	reqHeader := header.(*objMessages.ObjRequestHeader)

	suInf := connector.Server.GetParentContext()
	server, ok := suInf.(*app.RedServer)
	if !ok {
		errAll = common.ErrorLog("red server is not in parent context")
		goto output
	}

	req, ok = body.(*objMessages.SSReportCommentRequest)
	if ok == false{
		errAll = common.ErrorLog("body is not forbid comment request")
		goto output
	}


	uid, err = server.ReportComment(req)
	if err != nil{
		errAll = err
		goto output
	}


output:

	rsp = &objMessages.SSReportCommentResponse{
		Stat: 0,
		Uid: uid,
	}

	if errAll != nil{

		rsp.Stat = -1
	}

	if err := connector.WriteObjResponseMesage(reqHeader,0, rsp); err != nil {
		common.ErrorLog("write msg response error", err, " msg id:", int(objMessages.MessageId_Vote))
		return err
	}


	return nil

}











