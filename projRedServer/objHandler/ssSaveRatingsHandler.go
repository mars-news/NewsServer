package objHandler


import (
	"MarsXserver/tcpserver"
	"MarsXserver/common"
	"MarsXserver/objMessages"
	"MarsXserver/redserver/app"
)

type SSSaveRatingsHandler struct{

}


func (handler *SSSaveRatingsHandler) Handle(connector *tcpserver.Connector, header interface{}, body interface{}) error{


	var errAll error
	var ok bool
	var uid, before int
	var ratings []objMessages.SSRatingItem
	var rsp *objMessages.SSSaveRatingResponse
	var req *objMessages.SSSaveRatingRequst


	reqHeader := header.(*objMessages.ObjRequestHeader)

	suInf := connector.Server.GetParentContext()
	server, ok := suInf.(*app.RedServer)
	if !ok {
		errAll = common.ErrorLog("red server is not in parent context")
		goto output
	}

	req, ok = body.(*objMessages.SSSaveRatingRequst)
	if ok == false{
		errAll = common.ErrorLog("body is not save ratings request")
		goto output
	}

	//uid = req.Uid
	uid = req.Uid
	ratings = req.Ratings
	before = req.Before

	server.SaveRatings(uid, before, ratings)


output:

	rsp = &objMessages.SSSaveRatingResponse{
		Stat: 0,
	}

	if errAll != nil{

		rsp.Stat = -1
	}

	if err := connector.WriteObjResponseMesage(reqHeader,0, rsp); err != nil {
		common.ErrorLog("write msg response error", err, " msg id:", int(objMessages.MessageId_Rating_Save))
		return err
	}


	return nil

}































