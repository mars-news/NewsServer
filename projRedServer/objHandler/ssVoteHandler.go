package objHandler


import (
	"MarsXserver/tcpserver"
	"MarsXserver/common"
	"MarsXserver/objMessages"
	"MarsXserver/redserver/app"
)

type SSVoteHandler struct{

}


func (handler *SSVoteHandler) Handle(connector *tcpserver.Connector, header interface{}, body interface{}) error{

	var errAll error
	var ok bool
	var uid, nid, score int
	var votes []objMessages.SSVoteItem
	var req *objMessages.SSVoteRequest
	var rsp *objMessages.SSVoteResponse


	reqHeader := header.(*objMessages.ObjRequestHeader)

	suInf := connector.Server.GetParentContext()
	server, ok := suInf.(*app.RedServer)
	if !ok {
		errAll = common.ErrorLog("red server is not in parent context")
		goto output
	}

	req, ok = body.(*objMessages.SSVoteRequest)
	if ok == false{
		errAll = common.ErrorLog("body is not publish comment request")
		goto output
	}

	uid = req.Uid
	nid = req.Nid
	votes = req.Votes
	score = req.Score

	for _, vote := range votes{

		if vote.IsReply {
			server.AddReplyVote(nid, vote.Pid, vote.Rid)
		}else{

			server.AddCommentVote(nid, vote.Pid)
		}
	}


	if score > 0{

		server.SaveRating(uid, nid, score)
	}

output:

	rsp = &objMessages.SSVoteResponse{
		Stat: 0,
	}

	if errAll != nil{

		rsp.Stat = -1
	}

	if err := connector.WriteObjResponseMesage(reqHeader,0, rsp); err != nil {
		common.ErrorLog("write msg response error", err, " msg id:", int(objMessages.MessageId_Vote))
		return err
	}


	return nil

}











