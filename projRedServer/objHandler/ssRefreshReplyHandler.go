package objHandler


import (
	"MarsXserver/tcpserver"
	"MarsXserver/common"
	"MarsXserver/objMessages"
	"MarsXserver/redserver/app"
)

type SSRefreshReplyHandler struct{

}


func (handler *SSRefreshReplyHandler) Handle(connector *tcpserver.Connector, header interface{}, body interface{}) error{

	var err, errAll error
	var ok bool
	var nid, pid, offset int
	var rsp *objMessages.SSRefreshReplyResponse
	var req *objMessages.SSRefreshReplyRequest
	var replies []*objMessages.SSReply


	reqHeader := header.(*objMessages.ObjRequestHeader)

	suInf := connector.Server.GetParentContext()
	server, ok := suInf.(*app.RedServer)
	if !ok {
		errAll = common.ErrorLog("red server is not in parent context")
		goto output
	}

	req, ok = body.(*objMessages.SSRefreshReplyRequest)
	if ok == false{
		errAll = common.ErrorLog("body is not publish comment request")
		goto output
	}

	nid = req.Nid
	pid = req.Pid
	offset = req.Offset

	replies, err = server.GetRefreshReplies(nid, pid, offset)
	if err != nil{
		errAll = err
		goto output
	}



output:

	rsp = &objMessages.SSRefreshReplyResponse{
		Replies: replies,
	}

	if errAll != nil{

		rsp.Stat = -1
	}

	if err := connector.WriteObjResponseMesage(reqHeader,0, rsp); err != nil {
		common.ErrorLog("write msg response error", err, " msg id:", int(objMessages.MessageId_Refresh_Reply))
		return err
	}


	return nil

}











