package objHandler


import (
	"MarsXserver/tcpserver"
	"MarsXserver/common"
	"MarsXserver/objMessages"
	"MarsXserver/redserver/app"
)

type SSGetReplyCountHandler struct{

}


func (handler *SSGetReplyCountHandler) Handle(connector *tcpserver.Connector, header interface{}, body interface{}) error{

	var err, errAll error
	var ok bool
	var pid, nid, cnt int
	var rsp *objMessages.SSGetReplyCountResponse
	var req *objMessages.SSGetReplyCountRequest


	reqHeader := header.(*objMessages.ObjRequestHeader)

	suInf := connector.Server.GetParentContext()
	server, ok := suInf.(*app.RedServer)
	if !ok {
		errAll = common.ErrorLog("red server is not in parent context")
		goto output
	}

	req, ok = body.(*objMessages.SSGetReplyCountRequest)
	if ok == false{
		errAll = common.ErrorLog("body is not publish comment request")
		goto output
	}

	nid = req.Nid
	pid = req.Pid

	cnt = server.GetReplyCount(nid, pid)
	if err != nil{
		errAll = err
		goto output
	}



output:

	rsp = &objMessages.SSGetReplyCountResponse{
		Count: cnt,
	}

	if errAll != nil{

		rsp.Stat = -1
	}

	if err := connector.WriteObjResponseMesage(reqHeader,0, rsp); err != nil {
		common.ErrorLog("write msg response error", err, " msg id:", int(objMessages.MessageId_Reply_Count))
		return err
	}


	return nil

}











