package objHandler


import (
	"MarsXserver/tcpserver"
	"MarsXserver/common"
	"MarsXserver/objMessages"
	"MarsXserver/redserver/app"
	"time"
)

type SSPublishCommentHandler struct{

}


func (handler *SSPublishCommentHandler) Handle(connector *tcpserver.Connector, header interface{}, body interface{}) error{

	var err, errAll error
	var ssContent *objMessages.SSPublishContent
	var comment *objMessages.RedComment
	var reply *objMessages.RedReply
	var ok bool
	var pid, rid int
	var rsp *objMessages.SSPublishCommentResponse
	var req *objMessages.SSPublishCommentRequest

	reqHeader := header.(*objMessages.ObjRequestHeader)

	suInf := connector.Server.GetParentContext()
	server, ok := suInf.(*app.RedServer)
	if !ok {
		errAll = common.ErrorLog("crawler is not in parent context")
		goto output
	}

	req, ok = body.(*objMessages.SSPublishCommentRequest)
	if ok == false{
		errAll = common.ErrorLog("body is not publish comment request")
		goto output
	}

	ssContent = req.Content

	if ssContent.IsReply > 0{

		reply = &objMessages.RedReply{
			Nid : ssContent.Nid,
			Pid : ssContent.Reply,
			Uid : ssContent.Uid,
			Content : ssContent.Content,
			Timestamp : time.Now().UTC(),
		}

		rid, err = server.ReplySave(reply)
		if err != nil{
			errAll = err
			goto output
		}

		common.InfoLog("saved reply for user:", reply.Uid, ": pid ", ssContent.Reply, " rid", rid)

	}else {

		comment = &objMessages.RedComment{
			Nid : ssContent.Nid,
			Uid : ssContent.Uid,
			Content : ssContent.Content,
			Timestamp : time.Now().UTC(),
		}


		pid, err = server.CommentSave(comment)
		if err != nil{
			errAll = err
			goto output
		}

		common.InfoLog("saved comment for user:", comment.Uid, ":", pid)

	}


output:

	rsp = &objMessages.SSPublishCommentResponse{
		Pid: pid,
	}


	if errAll != nil{

		if _, ok := errAll.(*common.CommentCntOverflowError); ok == false{
			rsp.Stat = int(common.ErrorCode_Comment_Publish_Failed)
		}else{
			rsp.Stat = int(common.ErrorCode_CommentCnt_Overflow)
		}
	}

	if err := connector.WriteObjResponseMesage(reqHeader,0, rsp); err != nil {
		common.ErrorLog("write msg response error", err, " msg id:", int(objMessages.MessageId_Publish_Comment))
		return err
	}


	return nil

}











