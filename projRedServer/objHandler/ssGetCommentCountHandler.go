package objHandler


import (
	"MarsXserver/tcpserver"
	"MarsXserver/common"
	"MarsXserver/objMessages"
	"MarsXserver/redserver/app"
)

type SSGetCommentCountHandler struct{

}


func (handler *SSGetCommentCountHandler) Handle(connector *tcpserver.Connector, header interface{}, body interface{}) error{

	var err, errAll error
	var ok bool
	var nid, cnt int
	var nids, cnts []int
	var rsp *objMessages.SSGetCommentCountResponse
	var req *objMessages.SSGetCommentCountRequest


	reqHeader := header.(*objMessages.ObjRequestHeader)

	suInf := connector.Server.GetParentContext()
	server, ok := suInf.(*app.RedServer)
	if !ok {
		errAll = common.ErrorLog("red server is not in parent context")
		goto output
	}

	req, ok = body.(*objMessages.SSGetCommentCountRequest)
	if ok == false{
		errAll = common.ErrorLog("body is not publish comment request")
		goto output
	}

	nids = req.Nids
	cnts = make([]int, 0, len(nids))

	for _, nid = range nids{
		cnt = server.GetCommentCount(nid)
		if err != nil{
			errAll = err
			goto output
		}
		cnts = append(cnts, cnt)
	}


output:

	rsp = &objMessages.SSGetCommentCountResponse{
		Counts: cnts,
	}

	if errAll != nil{

		rsp.Stat = -1
	}

	if err := connector.WriteObjResponseMesage(reqHeader,0, rsp); err != nil {
		common.ErrorLog("write msg response error", err, " msg id:", int(objMessages.MessageId_Comment_Count))
		return err
	}


	return nil

}











