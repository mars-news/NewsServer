package objHandler


import (
	"MarsXserver/tcpserver"
	"MarsXserver/common"
	"MarsXserver/objMessages"
	"MarsXserver/redserver/app"
)

type SSRefreshCommentHandler struct{

}


func (handler *SSRefreshCommentHandler) Handle(connector *tcpserver.Connector, header interface{}, body interface{}) error{

	var err, errAll error
	var ok bool
	var nid, offset int
	var rsp *objMessages.SSRefreshCommentResponse
	var req *objMessages.SSRefreshCommentRequest
	var comments []*objMessages.SSComment


	reqHeader := header.(*objMessages.ObjRequestHeader)

	suInf := connector.Server.GetParentContext()
	server, ok := suInf.(*app.RedServer)
	if !ok {
		errAll = common.ErrorLog("red server is not in parent context")
		goto output
	}

	req, ok = body.(*objMessages.SSRefreshCommentRequest)
	if ok == false{
		errAll = common.ErrorLog("body is not publish refresh comment request")
		goto output
	}

	nid = req.Nid
	offset = req.Offset

	comments, err = server.GetRefreshComments(nid, offset)
	if err != nil{
		errAll = err
		goto output
	}



output:

	rsp = &objMessages.SSRefreshCommentResponse{
		Comments: comments,
	}

	if errAll != nil{

		if _, ok = errAll.(*common.UsedUpError); ok {

			rsp.Stat = int(common.ErrorCode_Used_Up)
		}else{
			rsp.Stat = -1
		}
	}

	if err := connector.WriteObjResponseMesage(reqHeader,0, rsp); err != nil {
		common.ErrorLog("write msg response error", err, " msg id:", int(objMessages.MessageId_Refresh_Comment))
		return err
	}


	return nil

}











