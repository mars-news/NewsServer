package objHandler


import (
	"MarsXserver/tcpserver"
	"MarsXserver/common"
	"MarsXserver/objMessages"
	"MarsXserver/redserver/app"
)

type SSCollectHotNewsHandler struct{

}


func (handler *SSCollectHotNewsHandler) Handle(connector *tcpserver.Connector, header interface{}, body interface{}) error{

	var errAll, err error
	var ok bool
	var hourIdx int64
	var res []int
	var req *objMessages.SSCollectHotNewsRequest
	var rsp *objMessages.SSCollectHotNewsResponse


	reqHeader := header.(*objMessages.ObjRequestHeader)

	suInf := connector.Server.GetParentContext()
	server, ok := suInf.(*app.RedServer)
	if !ok {
		errAll = common.ErrorLog("red server is not in parent context")
		goto output
	}

	req, ok = body.(*objMessages.SSCollectHotNewsRequest)
	if ok == false{
		errAll = common.ErrorLog("body is not publish comment request")
		goto output
	}

	hourIdx = req.HourIdx

	res, err = server.CollectAllHotNewsNids(hourIdx)
	if err != nil{
		errAll = err
		goto output
	}


output:

	rsp = &objMessages.SSCollectHotNewsResponse{
		Stat: 0,
		Nids: res,
	}

	if errAll != nil{

		rsp.Stat = -1
	}

	if err := connector.WriteObjResponseMesage(reqHeader,0, rsp); err != nil {
		common.ErrorLog("write msg response error", err, " msg id:", int(objMessages.MessageId_Collect_Hot_News))
		return err
	}


	return nil

}




















