package redserver

import (
	"MarsXserver/redserver/app"

	"MarsXserver/tcpserver"
	"reflect"
	"MarsXserver/redserver/objHandler"
	"MarsXserver/objMessages"
)


func initRegisterTcpHandlers(sServer *app.RedServer){


	tcpserver.RegisterHandler(
		int(objMessages.MessageId_Publish_Comment),
		&tcpserver.HandlerItem{
			MsgProtoType: tcpserver.MessageProtoType_ObjRequest,
			Handler:      &objHandler.SSPublishCommentHandler{},
			ReqMsgType:   reflect.TypeOf(objMessages.SSPublishCommentRequest{}),
			RspMsgType:   reflect.TypeOf(objMessages.SSPublishCommentResponse{}),
		})


	tcpserver.RegisterHandler(
		int(objMessages.MessageId_Comment_Count),
		&tcpserver.HandlerItem{
			MsgProtoType: tcpserver.MessageProtoType_ObjRequest,
			Handler:      &objHandler.SSGetCommentCountHandler{},
			ReqMsgType:   reflect.TypeOf(objMessages.SSGetCommentCountRequest{}),
			RspMsgType:   reflect.TypeOf(objMessages.SSGetCommentCountResponse{}),
		})


	tcpserver.RegisterHandler(
		int(objMessages.MessageId_Reply_Count),
		&tcpserver.HandlerItem{
			MsgProtoType: tcpserver.MessageProtoType_ObjRequest,
			Handler:      &objHandler.SSGetReplyCountHandler{},
			ReqMsgType:   reflect.TypeOf(objMessages.SSGetReplyCountRequest{}),
			RspMsgType:   reflect.TypeOf(objMessages.SSGetReplyCountResponse{}),
		})


	tcpserver.RegisterHandler(
		int(objMessages.MessageId_Refresh_Comment),
		&tcpserver.HandlerItem{
			MsgProtoType: tcpserver.MessageProtoType_ObjRequest,
			Handler:      &objHandler.SSRefreshCommentHandler{},
			ReqMsgType:   reflect.TypeOf(objMessages.SSRefreshCommentRequest{}),
			RspMsgType:   reflect.TypeOf(objMessages.SSRefreshCommentResponse{}),
		})


	tcpserver.RegisterHandler(
		int(objMessages.MessageId_Refresh_Reply),
		&tcpserver.HandlerItem{
			MsgProtoType: tcpserver.MessageProtoType_ObjRequest,
			Handler:      &objHandler.SSRefreshReplyHandler{},
			ReqMsgType:   reflect.TypeOf(objMessages.SSRefreshReplyRequest{}),
			RspMsgType:   reflect.TypeOf(objMessages.SSRefreshReplyResponse{}),
		})


	tcpserver.RegisterHandler(
		int(objMessages.MessageId_Vote),
		&tcpserver.HandlerItem{
			MsgProtoType: tcpserver.MessageProtoType_ObjRequest,
			Handler:      &objHandler.SSVoteHandler{},
			ReqMsgType:   reflect.TypeOf(objMessages.SSVoteRequest{}),
			RspMsgType:   reflect.TypeOf(objMessages.SSVoteResponse{}),
		})

	tcpserver.RegisterHandler(
		int(objMessages.MessageId_Rating_Save),
		&tcpserver.HandlerItem{
			MsgProtoType: tcpserver.MessageProtoType_ObjRequest,
			Handler:      &objHandler.SSSaveRatingsHandler{},
			ReqMsgType:   reflect.TypeOf(objMessages.SSSaveRatingRequst{}),
			RspMsgType:   reflect.TypeOf(objMessages.SSSaveRatingResponse{}),
		})

	tcpserver.RegisterHandler(
		int(objMessages.MessageId_Rating_Collect),
		&tcpserver.HandlerItem{
			MsgProtoType: tcpserver.MessageProtoType_ObjRequest,
			Handler:      &objHandler.SSCollectRatingsHandler{},
			ReqMsgType:   reflect.TypeOf(objMessages.SSCollectRatingsRequest{}),
			RspMsgType:   reflect.TypeOf(objMessages.SSCollectRatingsResponse{}),
		})


	tcpserver.RegisterHandler(
		int(objMessages.MessageId_Collect_Hot_News),
		&tcpserver.HandlerItem{
			MsgProtoType: tcpserver.MessageProtoType_ObjRequest,
			Handler:      &objHandler.SSCollectHotNewsHandler{},
			ReqMsgType:   reflect.TypeOf(objMessages.SSCollectHotNewsRequest{}),
			RspMsgType:   reflect.TypeOf(objMessages.SSCollectHotNewsResponse{}),
		})

	tcpserver.RegisterHandler(
		int(objMessages.MessageId_Report_Comment),
		&tcpserver.HandlerItem{
			MsgProtoType: tcpserver.MessageProtoType_ObjRequest,
			Handler:      &objHandler.SSReportCommentHandler{},
			ReqMsgType:   reflect.TypeOf(objMessages.SSReportCommentRequest{}),
			RspMsgType:   reflect.TypeOf(objMessages.SSReportCommentResponse{}),
		})



}


func initRegisterHttpHandlers(aServer *app.RedServer){

	HttpHandlers := make(map[string]interface{})

	aServer.RegisterHttpHandlers(HttpHandlers)
}



func initRegisterFileHandlers(aServer *app.RedServer){



}

func InitRegisterHandlers(aServer *app.RedServer){

	initRegisterTcpHandlers(aServer)
	initRegisterFileHandlers(aServer)
	initRegisterHttpHandlers(aServer)

}

