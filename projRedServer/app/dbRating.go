package app

import (
	"fmt"
	"MarsXserver/objMessages"
	"MarsXserver/common"
	"MarsXserver/redmodule"
	"MarsXserver/proj"
)

const (


	Rating_Users_Time_Set_Key = "Rating_User_Hour"	//used for doop all user in a hourIdx
	User_Rating_Set_Key = "Rating_User"			//used for doop calc,
	Rating_Time_Set_Key = "Rating_Time_Set"		//used for doop all hourIdx,

	Rating_Collect_Max_Once = 100
)


type NewsRate struct{

	HIdx int
	UID int
	NID int
}


func (this *RedServer) SaveRatings(uid int, before int, ratings []objMessages.SSRatingItem){

	for _, rating := range ratings{
		saveOneRating(this, uid, before, &rating)
	}

}


func (this *RedServer) SaveRating(uid int, nid int, score int){


	saveOneRating(this, uid, 0, &objMessages.SSRatingItem{
		Nid: nid,
		Score: score,
	})

}


func getRatingTimeKey(hourIdx int64) string{

	//hourIdx := hour/ common.Collect_Ratings_Time_Span
	key := fmt.Sprintf("%s_%d", Rating_Users_Time_Set_Key, hourIdx)
	return key
}



func getUserRatingSetKey(hourIdx int64, uid int) string{

	key := fmt.Sprintf("%s_%d_%d", User_Rating_Set_Key, hourIdx, uid)
	return key
}



func saveOneRating(this *RedServer , uid int, before int, rating *objMessages.SSRatingItem){

	hourIdx := proj.HourIdxGetCurrIdx()
	hourIdx.HourIdx -= int64(before)

	nid := rating.Nid
	score := rating.Score
	if score > proj.Max_Score_Value{
		score = proj.Max_Score_Value
	}

	this.Red.SetFlag(RedTableRate, hourIdx.HourIdx, uid, nid, score)


	this.voteManager.SetNewsVote(hourIdx.HourIdx, nid, score)

}



func (this *RedServer) CollectAllHotNewsNids(hourIdx int64) ( []int, error ){

	length := this.voteManager.GetNewsCount(hourIdx)

	res := this.voteManager.GetNewsNids(hourIdx, 0, length)

	return res, nil
}







func (this *RedServer) CollectRatings( hourIdx int64, prevCursor int )( ratings []objMessages.SSRatingUserItem, newCursor int, err error ){

	defer func(){
		if err := recover(); err != nil{
			common.ErrorLog("collect panic", err)
		}
	}()

	var redRes *redmodule.RedFlagCollectRes

	redRes, newCursor, err = this.Red.CollectFlagsByIdx(RedTableRate, 1, prevCursor, 2, hourIdx)

	ratings = make([]objMessages.SSRatingUserItem, 0)
	collectRatings := make(map[int]*objMessages.SSRatingUserItem)

	for _, resRow := range redRes.Res{

		if len(resRow) != 3{  //uid nid score

			common.ErrorLog("rate res row len err", len(resRow))
			continue
		}

		uid := resRow[0].(int)
		nid := resRow[1].(int)
		score := resRow[2].(int)

		ssItem, ok := collectRatings[uid]
		if ok == false{
			collectRatings[uid] = &objMessages.SSRatingUserItem{
				Uid: uid,
				Ratings: make([]objMessages.SSRatingItem, 0),
			}
			ssItem = collectRatings[uid]
		}

		ssItem.Ratings = append(ssItem.Ratings, objMessages.SSRatingItem{
				Nid: 	nid,
				Score:	score,
		})
	}

	for _, ssItem := range collectRatings{
		ratings = append(ratings, *ssItem)
	}

	err = nil

	return

}



































