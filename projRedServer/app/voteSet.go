package app

import (
	"sync"
	"container/heap"
	"MarsXserver/common"
)

type VoteItem struct {

	Key   int
	Vote  int
	index int
}


type VoteHeap []*VoteItem


func (this VoteHeap) Len() int { return len(this)}

func (this VoteHeap) Less(i, j int) bool{
	return this[i].Vote > this[j].Vote
}

func (this VoteHeap) Swap(i, j int){

	this[i], this[j] = this[j], this[i]
	this[i].index = i
	this[j].index = j
}

func (this *VoteHeap) Push(x interface{}){
	n := len(*this)
	item := x.(*VoteItem)
	item.index = n
	*this = append(*this, item)
}


func (this *VoteHeap) Pop() interface{} {
	old := *this
	n := len(*this)
	item := old[n-1]
	item.index = -1
	*this = old[0: n-1]
	return item
}


func (this *VoteHeap) update(item *VoteItem, vote int){
	item.Vote = vote
	heap.Fix(this, item.index)
}



type VoteSet struct{
	heap VoteHeap
	lock sync.RWMutex
}



func (this *VoteSet) GetVoteByKey(pid int) (*VoteItem, bool){  //get vote cannot have lock, because it is called by others


	for _, item := range this.heap{

		if item.Key == pid{
			return item, true
		}
	}

	return nil, false
}





func (this *VoteSet) SetVote(pid int, vote int){

	this.lock.Lock()
	defer this.lock.Unlock()

	voteItem, ok := this.GetVoteByKey(pid)
	if !ok{

		heap.Push(&this.heap, &VoteItem{
			Key:  pid,
			Vote: vote,
		})
		return
	}


	this.heap.update(voteItem, vote)


	for idx, val := range this.heap{

		common.InfoLog("heap:", idx, " :", val.Key, " vote:", val.Vote)

	}
}

func (this *VoteSet) AddVote(pid int, vote int){

	this.lock.Lock()
	defer this.lock.Unlock()

	voteItem, ok := this.GetVoteByKey(pid)
	if !ok{

		heap.Push(&this.heap, &VoteItem{
			Key:  pid,
			Vote: vote,
		})
		return
	}


	this.heap.update(voteItem, voteItem.Vote + vote)


	for idx, val := range this.heap{

		common.InfoLog("heap:", idx, " :", val.Key, " vote:", val.Vote)

	}
}




func (this *VoteSet) SubVote(key int){

	this.lock.Lock()
	defer this.lock.Unlock()

	voteItem, ok := this.GetVoteByKey(key)
	if !ok{

		heap.Push(&this.heap, &VoteItem{
			Key:  key,
			Vote: 1,
		})
		return
	}

	vote := voteItem.Vote - 1
	if vote < 0{
		vote = 0
	}


	this.heap.update(voteItem, vote)

}

func (this *VoteSet) GetKeys(offset int, length int) ([]int){

	this.lock.RLock()
	defer this.lock.RUnlock()

	if offset >= len(this.heap){
		return []int{}
	}

	res := make([]int, length)

	ii := 0
	for ; ii+offset < len(this.heap); ii++ {
		res[ii] = this.heap[ii].Key
	}

	return res[0:ii]

}

func (this *VoteSet) GetAllKeys() ([]*VoteItem){

	this.lock.RLock()
	defer this.lock.RUnlock()

	return this.heap


}


func (this *VoteSet) GetCount() int{

	this.lock.RLock()
	defer this.lock.RUnlock()

	return len(this.heap)

}


func (this *VoteSet) RemoveByKey(key int) {

	this.lock.Lock()
	defer this.lock.Unlock()

	voteItem, ok := this.GetVoteByKey(key)
	if !ok{
		return
	}

	maxVote := this.heap[0].Vote + 10000

	this.heap.update(voteItem, maxVote)

	heap.Pop(&this.heap)

}














