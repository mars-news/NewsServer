package app

import (
	"MarsXserver/common"
	"fmt"
	"MarsXserver/objMessages"
	"MarsXserver/proj"
)



const(

	Nosql_GC_Duration = proj.Collect_Day_Time_Spon * 3

	Comment_Refresh_Count = 20

	Comment_By_User_Max_Day_Count = 100

	Reply_Refresh_Count = 20

	Forbid_Count = 2

)


func (this *RedServer) CommentCheck(comment *objMessages.RedComment) (bool, error){

	defer func(){

		if err:= recover(); err != nil{
			common.ErrorLog(err)
		}
	}()

	/*

	commentUserKey := fmt.Sprintf("%s_%d", Comment_Count_BY_USER_Key, comment.Uid)

	uCount, err := this.Red.Conn.Get(commentUserKey).Int64()
	if err != nil{
		return false, common.ErrorLog("get user comment count failed")
	}

	if uCount > Comment_By_User_Max_Day_Count{
		return false, &common.CommentCntOverflowError{}
	}*/

	return true, nil
}


func (this *RedServer) ReportComment(report *objMessages.SSReportCommentRequest) (uid int32, err error){

	defer func() {

		if err := recover(); err != nil {
			common.ErrorLog(err)
		}
	}()

	if report.IsReply > 0{

		reply := &objMessages.RedReply{
			Nid: report.Nid,
			Pid: report.Pid,
			Id: report.Rid,
		}

		if err := this.Red.ReadHash(reply); err != nil{
			common.InfoLog("report target not exist", fmt.Sprintf("%+v", *report), err)
			return -1, nil
		}

		if reply.Reported + 1 >= Forbid_Count{
			this.RemoveReply(report.Nid, report.Pid, report.Rid)
			common.InfoLog("reply forbid", report.Nid, report.Pid, report.Rid)
		}else{
			reply.Reported += 1

			return -1, this.Red.UpdateHash(reply, []string{"Reported"})
		}

	}else{

		comment := &objMessages.RedComment{
			Nid: report.Nid,
			Id: report.Pid,
		}

		if err := this.Red.ReadHash(comment); err != nil{
			common.InfoLog("report target not exist", fmt.Sprintf("%+v", *report), err)
			return -1, nil
		}

		if comment.Reported + 1 >= Forbid_Count{
			this.RemoveComment(report.Nid, report.Pid)
			common.InfoLog("comment forbid", report.Nid, report.Pid)
		}else{
			comment.Reported += 1

			return comment.Uid, this.Red.UpdateHash(comment, []string{"Reported"})

		}

	}

	return -1, nil
}


func (this *RedServer) CommentSave(comment *objMessages.RedComment) (int, error) {   //todo user comment count

	defer func() {

		if err := recover(); err != nil {
			common.ErrorLog(err)
		}
	}()

	if err := this.Red.SaveHash(comment);err != nil{
		return -1, err
	}
	return comment.Id, nil
}



func (this *RedServer) ReplySave(reply *objMessages.RedReply) (int, error) {

	defer func() {

		if err := recover(); err != nil {
			common.ErrorLog(err)
		}
	}()

	if err := this.Red.SaveHash(reply); err != nil{
		return -1, err
	}

	return reply.Id, nil

}



func (this *RedServer) GetCommentCount(nid int) int{

	count, err := this.Red.GetCount(&objMessages.RedComment{
		Nid: nid,
	}, 1)
	if err != nil{
		return 0
	}
	return count
}

func (this *RedServer) GetReplyCount(nid int, pid int) int{

	comment := &objMessages.RedComment{
		Nid: nid,
		Id: pid,
	}

	if err := this.Red.ReadHash(comment); err != nil{
		return 0
	}

	return comment.ReplyCount
}


func (this *RedServer) GetCommentVoteCount(nid int, pid int) (int, bool){

	comment := &objMessages.RedComment{
		Nid: nid,
		Id: pid,
	}

	if err := this.Red.ReadHash(comment); err != nil{
		return 0, false
	}

	return comment.Vote, true
}


func (this *RedServer) GetReplyVoteCount(nid int, pid int, rid int) (int, bool){

	reply := &objMessages.RedReply{
		Nid: nid,
		Pid: pid,
		Id: rid,
	}

	if err := this.Red.ReadHash(reply); err != nil{
		return 0, false                               //todo test
	}

	return reply.Vote, true


}



func (this *RedServer) GetRefreshComments(nid int, offset int) ([]*objMessages.SSComment, error){

	defer func(){

		if err:= recover(); err != nil{
			common.ErrorLog(err)
		}
	}()

	if offset <= 0{
		return nil, common.ErrorLog("offset error", offset)
	}
	if offset > this.GetCommentCount(nid){
		common.InfoLog("no available offset:", offset, "nid", nid)

		return []*objMessages.SSComment{}, new(common.UsedUpError)
	}

	pids := this.voteManager.GetCommentPids(nid, offset - 1, Comment_Refresh_Count)

	redOffset := offset - this.voteManager.GetCommentPidCount(nid)
	redLength := Comment_Refresh_Count - len(pids)

	count := this.GetCommentCount(nid)

	if len(pids) < Comment_Refresh_Count && redOffset >0 && redOffset <= int(count) && redLength > 0{

		jj := 0
		for ii := redOffset; ii <= int(count) && jj < redLength; ii++{

			cnt, ok := this.GetCommentVoteCount(nid, ii)

			if ok == false{
				continue
			}

			if cnt == 0{
				pids = append(pids, ii)
				jj+=1
			}
		}
	}


	res := make([]*objMessages.SSComment, 0, len(pids))

	for _, pid := range pids{


		comment, err := this.CommentGet(nid, pid)
		if err != nil{
			continue
		}

		res = append(res, comment)
	}

	return res, nil


}


func (this *RedServer) GetRefreshReplies(nid int, pid int, offset int) ([]*objMessages.SSReply, error){   //offset 1 based

	defer func(){

		if err:= recover(); err != nil{
			common.ErrorLog(err)
		}
	}()

	zeroBasedOffset := offset - 1
	if zeroBasedOffset < 0 {
		zeroBasedOffset = 0
	}

	rids := this.voteManager.GetReplyRids(nid, pid, zeroBasedOffset, Reply_Refresh_Count)

	redOffset := offset - this.voteManager.GetReplyRidCount(nid, pid)
	redLength := Reply_Refresh_Count - len(rids)

	count := this.GetReplyCount(nid, pid)

	if pid == 1 && count > 0{
		common.InfoLog("test")
	}

	if len(rids) < Reply_Refresh_Count && redOffset >0 && redOffset <= int(count) && redLength > 0{

		jj := 0
		for ii := redOffset; ii <= int(count) && jj < redLength; ii++{

			cnt, ok := this.GetReplyVoteCount(nid, pid, ii)

			if !ok{
				continue
			}

			if cnt == 0{
				rids = append(rids, ii)
				jj+=1
			}
		}
	}


	res := make([]*objMessages.SSReply, 0, len(rids))

	for _, rid := range rids{

		reply, err := this.ReplyGet(nid, pid, rid)
		if err != nil{
			continue
		}

		res = append(res, reply)
	}

	return res, nil

}




func (this *RedServer) RemoveComment(nid int, pid int) {

	defer func(){
		if err:= recover(); err != nil{
			common.ErrorLog(err)
		}
	}()

	this.Red.DeleteHash(&objMessages.RedComment{
		Nid: nid,
		Id: pid,
	})


}



func (this *RedServer) RemoveReply(nid int, pid int, rid int) {

	defer func(){
		if err:= recover(); err != nil{
			common.ErrorLog(err)
		}
	}()

	this.Red.DeleteHash(&objMessages.RedReply{
		Nid: nid,
		Pid: pid,
		Id: rid,
	})

}





func (this *RedServer) CommentGet(nid int, pid int) (*objMessages.SSComment, error){

	defer func(){

		if err:= recover(); err != nil{
			common.ErrorLog(err)
		}
	}()

	comment := &objMessages.RedComment{
		Nid: nid,
		Id: pid,
	}

	if err := this.Red.ReadHash(comment); err != nil{
		return nil, err
	}


	ssComment := &objMessages.SSComment{
		RedComment: *comment,
	}

	return ssComment, nil
}

func (this *RedServer) ReplyGet(nid int, pid int, rid int) (*objMessages.SSReply, error){

	defer func(){

		if err:= recover(); err != nil{
			common.ErrorLog(err)
		}
	}()


	reply := &objMessages.RedReply{
		Nid: nid,
		Pid: pid,
		Id: rid,
	}

	if err := this.Red.ReadHash(reply); err != nil{
		return nil, err
	}



	ssReply := &objMessages.SSReply{
		RedReply: *reply,
	}

	return ssReply, nil
}



func (this *RedServer) AddCommentVote(nid int, pid int) {

	defer func(){
		if err:= recover(); err != nil{
			common.ErrorLog(err)
		}
	}()

	comment := &objMessages.RedComment{
		Nid: nid,
		Id: pid,
	}

	vote, err := this.Red.IncrHashFieldBy(comment, "Vote", 1)
	if err != nil{
		vote = 0
	}

	this.voteManager.SetCommentVote(nid, pid, int(vote))
}



func (this *RedServer) AddReplyVote(nid int, pid int, rid int) {

	defer func(){
		if err:= recover(); err != nil{
			common.ErrorLog(err)
		}
	}()

	reply := &objMessages.RedReply{
		Nid: nid,
		Pid: pid,
		Id: rid,
	}

	vote, err := this.Red.IncrHashFieldBy(reply, "Vote", 1)
	if err != nil{
		vote = 0
	}

	this.voteManager.SetReplyVote(nid, pid, rid, int(vote))

}


/*
func (this *RedServer) NoSqlGC(){

	now := time.Now()
	delTm := now.Add(-Nosql_GC_Duration)

	if err := this.Red.TimeIndex.RemoveDataByLatestTime(nil, &delTm); err != nil{
		common.ErrorLog("no sql gc failed:", err)
	}

}*/




































