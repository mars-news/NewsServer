package app

import(
	"container/heap"
	"MarsXserver/common"
	"MarsXserver/proj"
)


type VoteMap map[interface{}]*VoteSet

type VoteManager struct {
	newsAll VoteMap				//map key: hourIdx	val: nid set
	commentsAll VoteMap			//map key: nid      val: pid set
	replyAll    VoteMap			//map key: nid pid  val: rid set
}


type NewsKey struct{
	HourIdx int64
}

type CommentKey struct {
	Nid int
}

type ReplyKey struct {
	Nid int
	Pid int
}



func NewVoteManager() *VoteManager {

	vMgr := &VoteManager{
		newsAll: make(VoteMap),
		commentsAll: make(VoteMap),
		replyAll:    make(VoteMap),
	}

	return vMgr
}


func (this *VoteManager) getVoteMap(key interface{}) (VoteMap, error){

	switch key.(type) {
	case NewsKey:
		return this.newsAll, nil
	case CommentKey:
		return this.commentsAll, nil
	case ReplyKey:
		return this.replyAll, nil

	default:
		return nil, common.ErrorLog("vote key invalid", key)
	}

}


func (this *VoteManager) getVoteSet(key interface{}) (*VoteSet, error){

	switch key.(type) {
	case NewsKey:
		return this.newsAll[key], nil
	case CommentKey:
		return this.commentsAll[key], nil
	case ReplyKey:
		return this.replyAll[key], nil

	default:
		return nil, common.ErrorLog("vote key invalid", key)
	}

}



func (this *VoteManager) setVote(key interface{}, voteKey, voteVal int){

	voteSet, err := this.getVoteSet(key)
	if err != nil{
		return
	}

	voteMap, err := this.getVoteMap(key)
	if err != nil{
		return
	}

	if voteSet == nil {

		newVoteSet := &VoteSet{
			heap: make(VoteHeap, 0),
		}

		heap.Init(&newVoteSet.heap)

		newVoteSet.SetVote(voteKey, voteVal)

		voteMap[key] = newVoteSet

		return

	}

	voteSet.AddVote(voteKey, voteVal)

}


func (this *VoteManager) getVoteKeys(key interface{}, offset int, length int) []int{

	voteSet, err := this.getVoteSet(key)
	if err != nil || voteSet == nil{
		return []int{}
	}

	return voteSet.GetKeys(offset, length)

}

//how many news in a houridx
//how many comments in a news
//how many replies in a comment

func (this *VoteManager) getVoteCount(key interface{}) int{

	voteSet, err := this.getVoteSet(key)

	if err != nil || voteSet == nil{
		return 0
	}

	return voteSet.GetCount()

}


//each value can be got from nosql


///////////////////////////////////////////////////////////////////////////////////////////

func (this *VoteManager) SetNewsVote(hourIdx int64, nid, score int){

	this.setVote(NewsKey{
		HourIdx: hourIdx,
	}, nid, score)

}


func (this *VoteManager) SetCommentVote(nid int, pid int, vote int){

	this.setVote(CommentKey{
		Nid: nid,
	}, pid, vote)

}

func (this *VoteManager) SetReplyVote(nid int, pid int, rid int, vote int){
	this.setVote(ReplyKey{
		Nid: nid,
		Pid: pid,
	}, rid, vote)

}


func (this *VoteManager) GetNewsNids(hourIdx int64, offset int, length int) []int{

	return this.getVoteKeys(NewsKey{
		HourIdx: hourIdx,
	}, offset, length)

}

func (this *VoteManager) GetCommentPids(nid int, offset int, length int) ([]int){

	return this.getVoteKeys(CommentKey{
		Nid: nid,
	}, offset, length)

}

func (this *VoteManager) GetReplyRids(nid int, pid int, offset int, length int) ([]int){

	return this.getVoteKeys(ReplyKey{
		Nid: nid,
		Pid: pid,
	}, offset, length)

}




func (this *VoteManager) GetNewsCount(hourIdx int64) int{

	return this.getVoteCount(NewsKey{
		HourIdx: hourIdx,
	})
}


func (this *VoteManager) GetCommentPidCount(nid int) int{

	return this.getVoteCount(CommentKey{
		Nid: nid,
	})
}


func (this *VoteManager) GetReplyRidCount(nid int, pid int) int{

	return this.getVoteCount(ReplyKey{
		Nid: nid,
		Pid: pid,
	})

}


func (this *VoteManager) GC(){

	currHourIdx := proj.HourIdxGetCurrIdx()

	earlyHourIdx := currHourIdx.AddDuration(-GC_Keep_Duration)


	delKeysSet := common.NewMSet()


	for hourIdxInf, newsSet := range this.newsAll{

		hourIdxObj := hourIdxInf.(NewsKey)
		hourIdx := hourIdxObj.HourIdx

		if hourIdx < earlyHourIdx.HourIdx{

			newsKeys := newsSet.GetAllKeys()

			for _, newsKey := range newsKeys{

				delKeysSet.Insert(newsKey.Key)

			}
			delete(this.newsAll, hourIdxInf)
		}
	}



	for commentKeyInf := range this.commentsAll{

		commentKeyObj := commentKeyInf.(CommentKey)

		if delKeysSet.Contains(commentKeyObj.Nid){
			delete(this.commentsAll, commentKeyInf)
		}
	}

	for replyKeyInf := range this.replyAll{

		replyKeyObj := replyKeyInf.(ReplyKey)

		if delKeysSet.Contains(replyKeyObj.Nid){
			delete(this.replyAll, replyKeyInf)
		}
	}



}



























































