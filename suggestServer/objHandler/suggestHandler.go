package objHandler


import (
	"MarsXserver/tcpserver"
	"MarsXserver/common"
	"MarsXserver/objMessages"
	"MarsXserver/suggestServer/app"
	"MarsXserver/orm"
)

type SSSuggestRequestHandler struct{

}


func (handler *SSSuggestRequestHandler) Handle(connector *tcpserver.Connector, header interface{}, body interface{}) error{

	reqHeader := header.(*objMessages.ObjRequestHeader)

	suInf := connector.Server.GetParentContext()
	server, ok := suInf.(*app.SuggestServer)
	if !ok {
		return common.ErrorLog("crawler is not in parent context")
	}

	req, ok := body.(*objMessages.SuggestGetSuggestRequest)
	if ok == false{
		connector.SendObjErrorResponse(reqHeader, -1)
		return common.ErrorLog("body is not expr request")
	}

	uid := req.Uid
	var pids []int
	var err error

	if req.IsCommon{
		pids, err = server.Algo.SuggestGeneralForUser(uid)
	}else{
		pids, err = server.Algo.SuggestCatForUser(uid, req.Cat)
	}

	common.InfoLog("suggest for user:", uid, ":", pids)

	err = nil
	//pids = []int{35, 1, 2}

	rsp := &objMessages.SuggestGetSuggestResponse{
		Pids: pids,
	}


	if err != nil{

		if _, ok := err.(*common.SuggestEndError); ok == false{
			common.ErrorLog("get suggest for user failed", uid)
			rsp.Stat = int(common.ErrorCode_Suggest_Failed)
		}else{
			rsp.Stat = int(common.ErrorCode_Suggest_End)
		}
	}

	if err := connector.WriteObjResponseMesage(reqHeader,0, rsp); err != nil {
		common.ErrorLog("write msg response error", err, " msg id:", int(orm.MessageId_DB_Expr))
		return err
	}


	return nil

}











