package objHandler


import (
	"MarsXserver/tcpserver"
	"MarsXserver/common"
	"MarsXserver/objMessages"
	"MarsXserver/suggestServer/app"
)

type SSSuggestStartCollectRequestHandler struct{

}


func (handler *SSSuggestStartCollectRequestHandler) Handle(connector *tcpserver.Connector, header interface{}, body interface{}) error{

	reqHeader := header.(*objMessages.ObjRequestHeader)

	suInf := connector.Server.GetParentContext()
	server, ok := suInf.(*app.SuggestServer)
	if !ok {
		return common.ErrorLog("crawler is not in parent context")
	}

	req, ok := body.(*objMessages.SuggestStartCollectRequest)
	if ok == false{
		connector.SendObjErrorResponse(reqHeader, -1)
		return common.ErrorLog("body is not expr request")
	}

	var err error

	if req.Hot{
		server.Algo.CollectHotNewsPids()
	}

	if req.Latest{
		server.Algo.CollectLatestNews(false, req.Before)
	}

	common.InfoLog("collecting...")

	err = nil

	rsp := &objMessages.SuggestStartCollectResponse{
	}


	if err != nil{

		rsp.Stat = -1
	}

	if err := connector.WriteObjResponseMesage(reqHeader,0, rsp); err != nil {
		common.ErrorLog("write msg response error", err, " msg id:", int(objMessages.MessageId_Suggest_StartCollect))
		return err
	}


	return nil

}











