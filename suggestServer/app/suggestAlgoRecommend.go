package app

import (
	"MarsXserver/common"
	"math"
	"MarsXserver/db"
	"MarsXserver/objMessages"
	"strings"
	"strconv"
	"bytes"
	"fmt"
	"time"
	"MarsXserver/proj"
)


const (
	Suggest_UserCF_Percent = 0.6
	Suggest_HotNews_Percent = 0.2
	Suggest_Random_Percent = 0.2

	Suggest_Cat_HotNews_Percent = 0.5
	Suggest_Cat_Random_Percent = 0.5

	Doop_Backwards_Duration = time.Hour * 4

	Lack_Interest_Value = 5

)

type SuggestWaterfallFunc func(uid int, hourIdx *proj.HourIdx,cnt int, extraData interface{}, black *common.MSet) ([]interface{}, error)


func (this *SuggestAlgo) SuggestWaterfall(uid int, hourIdx *proj.HourIdx, tot int, extraData interface{}, pers []float64, sgFuncs []SuggestWaterfallFunc ) ( []interface{}, error ){


	common.DebugLogPlus("suggest water fall, hourIdx", hourIdx.HourIdx)

	perTot := 0.0

	for _, per := range pers{
		perTot += per
	}

	if math.Abs(perTot - 1.0) > common.Epsilon{
		return nil, common.ErrorLog("waterfall pers not equal to 1")
	}

	if len(pers) != len(sgFuncs){
		return nil, common.ErrorLog("pers num is not equal to func num")
	}

	resArr := make([]interface{}, 0)
	blackList := common.NewMSet()

	perAcc := 0.0

	for ii, per := range pers{

		perAcc += per

		currCnt := int(float64(tot) * per)
		readCnt := len(resArr)
		expectCnt := int(float64(tot) * perAcc)

		if readCnt < expectCnt{
			currCnt += expectCnt - readCnt - currCnt
		}
		res, err := sgFuncs[ii](uid, hourIdx, currCnt, extraData, blackList)

		if _, ok := err.(common.SuggestEndError); ok == true{
			continue
		}

		if err != nil{
			return nil, err
		}

		resArr = append(resArr, res...)

	}

	return resArr, nil
}




type WaterFallExtraData struct {
	pers []float64
	cat int
}


func (this *SuggestServer) UserHasRead(uid int, nid int) bool{

	ok, err := this.Red.ContainsFlag(RedTableRead, uid, nid)
	if err != nil || ok == false{
		return false
	}
	return true

}


func (this *SuggestAlgo) SuggestCatForUser(uid int, cat int)([]int, error){

	count := Default_Once_Refresh_Count

	resInf, err := this.SuggestWaterfall(uid, proj.HourIdxGetCurrIdx(), count,
		WaterFallExtraData{
			cat: cat,
		},
		[]float64{Suggest_Cat_HotNews_Percent, Suggest_Cat_Random_Percent},
		[]SuggestWaterfallFunc{
			this.SuggestFromHotNews,
			this.SuggestFromLatestNews,
		})

	if err != nil{

		return nil, common.ErrorLog("waterfall error", err)
	}


	res := make([]int, len(resInf))

	for ii, resItem := range resInf{
		res[ii] = resItem.(int)
	}


	for ii:= 0; ii < len(res); ii++{

		err = this.server.Red.SetFlag(RedTableRead, uid, res[ii], 1)
		if err != nil{
			return nil, common.ErrorLog("suggest for user set flag failed", uid, res[ii], err)
		}

	}

	return res, nil

}



func (this *SuggestAlgo) SuggestGeneralForUser(uid int) ([]int, error){

	common.InfoLog("init general for user")

	count := Default_Once_Refresh_Count

	user := &db.UserInterest{
		Id: int32(uid),
	}

	if err := this.server.Orm.Read(user, true); err != nil{
		return nil, common.ErrorLog("get user info failed, uid", uid)
	}

	intrsStrs := strings.Split(user.Interests, ",")

	intrs := make([]float64, 0)

	for _, intrItem := range intrsStrs{

		intr, err := strconv.Atoi(intrItem)
		if err != nil{
			common.ErrorLog("")
		}

		intrs = append(intrs, float64(intr))

	}

	for ii := len(intrsStrs); ii < db.CAT_COUNT; ii++{

		intrs = append(intrs, Lack_Interest_Value)

	}


	if len(intrs) != db.CAT_COUNT{
		return nil, common.ErrorLog("interest count mismatched", len(intrs), db.CAT_COUNT)
	}

	resInf, err := this.SuggestWaterfall(uid, proj.HourIdxGetCurrIdx(), count,
		WaterFallExtraData{
			pers:intrs,
			cat: 0,
		},
		[]float64{Suggest_UserCF_Percent, Suggest_HotNews_Percent, Suggest_Random_Percent},
		[]SuggestWaterfallFunc{
			this.SuggestFromDoopRecommends,
			this.SuggestFromHotNews,
			this.SuggestFromLatestNews,
		})

	if err != nil{

		return nil, common.ErrorLog("")
	}


	res := make([]int, len(resInf))

	for ii, resItem := range resInf{
		res[ii] = resItem.(int)
	}


	for ii:= 0; ii < len(res); ii++{

		err = this.server.Red.SetFlag(RedTableRead, uid, res[ii], 1)
		if err != nil{
			return nil, common.ErrorLog("suggest for user set flag failed", uid, res[ii], err)
		}

	}

	return res, nil
}
//////////////////////////////////////////////////////////////////////////////////////////////////////


func printResList(uid int, listName string, list []interface{}){

	var buff bytes.Buffer

	buff.WriteString(fmt.Sprintf("u%d,", uid))

	buff.WriteString(listName)
	buff.WriteString(":")

	for _, item := range list{

		buff.WriteString(fmt.Sprintf("%d, ", item.(int)))

	}

	common.InfoLog(buff.String())


}


func (this *SuggestAlgo) SuggestFromLatestNews(uid int, hourIdx *proj.HourIdx, length int, extraData interface{}, blackList *common.MSet)([]interface{}, error) {

	resList, err := this.SuggestFromNewsData(this.latestNewsData, uid, hourIdx, length, extraData, blackList)

	printResList(uid, "suggest from latest news", resList)

	return resList, err
}


func (this *SuggestAlgo) SuggestFromHotNews(uid int, hourIdx *proj.HourIdx, length int, extraData interface{}, blackList *common.MSet)([]interface{}, error)  {

	resList, err := this.SuggestFromNewsData(this.hotNewsData, uid, hourIdx, length, extraData, blackList)

	printResList(uid, "suggest from hot news", resList)

	return resList, err
}


func (this *SuggestAlgo) SuggestFromNewsData(nData *NewsData, uid int, hourIdx *proj.HourIdx, length int, extraData interface{}, blackList *common.MSet)([]interface{}, error){

	defer func(){
		if err := recover(); err != nil{
			common.ErrorLog("suggest from hot failed", err)
		}
	}()

	eData := extraData.(WaterFallExtraData)

	dayHourIdxCount := int(proj.Collect_Day_Time_Spon/proj.Collect_Ratings_Time_Span)

	ii := 0
	jj := proj.HourIdxGetCurrIdx()

	for ;ii < dayHourIdxCount; ii ++{

		if eData.pers != nil {
			if nData.CanRead(uid, -1, true, jj) {
				break
			}
		}else{
			if nData.CanRead(uid, eData.cat ,false, jj) {
				break
			}
		}

		jj = proj.HourIdxPrevIdx(jj)
	}

	if ii == dayHourIdxCount{
		return nil, common.SuggestEndError{}
	}


	var res []*SimpleCrawlItem
	var err error


	if eData.pers != nil{
		res, err = nData.GetNewsByPercent(uid, jj, length, eData.pers, blackList) //get top items

		if err != nil{
			return nil, common.ErrorLog("get news by percent failed", err)
		}
	}else{

		res, err = nData.GetNewsByCat(uid, jj, length, eData.cat, blackList)    //get cat items

		if err != nil{
			return nil, common.ErrorLog("get news by percent failed", err)
		}

	}

	resNids := make([]interface{}, len(res))

	for ii := 0; ii < len(res); ii++ {
		resNids[ii] = res[ii].Id
	}

	return resNids, nil

}




func (this *SuggestAlgo) DoopHourIdxCanRead(uid int, hourIdx *proj.HourIdx) (bool, error){

	now := time.Now().UTC()

	if this.doopRecData.Contains(uid, hourIdx) == false{

		if this.doopRecData.CanUpdateRecData(uid){

			this.doopRecData.UpdateRecDataTime(uid)

			ret, err := this.server.WriteObjRequestMessageDoopSync(
				&objMessages.SSDoopGetHourRecommendRequest{
					Uid: int(uid),
					HourIdx: hourIdx.HourIdx,
				})

			if err != nil{
				return false, common.ErrorLog("req recommends from doop failed")
			}

			rsp, ok := ret.(*objMessages.SSDoopGetHourRecommendResponse)

			if ok != true{
				return false, common.ErrorLog(" req recommands rsp type error")
			}

			if rsp.Stat < 0{
				return false, common.ErrorLog(" req recommands rsp stat error", rsp.Stat)
			}

			if len(rsp.Recs) > 0{

				this.doopRecData.SaveRecs(uid, hourIdx, rsp.Recs)

			}

			common.DebugLogPlus("get recs from doop for user", uid, " hourIdx:", hourIdx.HourIdx, " recs", rsp.Recs, "time", common.MakeDBTimeString(now))

		}else {

			common.DebugLogPlus("get recs cannot update time user", uid, " hourIdx:", hourIdx.HourIdx, "time", common.MakeDBTimeString(now))

			return false, nil

		}
	}

	return this.doopRecData.CanRead(uid, hourIdx), nil





}




func (this *SuggestAlgo) SuggestFromDoopRecommends(uid int, hourIdx_ *proj.HourIdx, length int, extraData interface{}, blackList *common.MSet)([]interface{}, error) {

	//hourIdx := common.HourIdxPrevIdx(hourIdx_)

	dayHourIdxCount := int(Doop_Backwards_Duration/proj.Collect_Ratings_Time_Span)

	ii := 0
	jj := proj.HourIdxGetNearestPrevIdx()

	for ;ii < dayHourIdxCount; ii++ {

		if ok, err := this.DoopHourIdxCanRead(uid, jj); err == nil && ok == true{
			break
		}
		jj = proj.HourIdxPrevIdx(jj)
	}

	if ii == dayHourIdxCount{
		return nil, common.SuggestEndError{}
	}


	recs := this.doopRecData.GetRecs(uid, jj, length)

	common.DebugLogPlus(uid ,"rec hour Data recs count:", len(recs), "wanted:", length)

	whites := make([]interface{}, 0, len(recs))

	for _, rec := range recs{

		if !blackList.Contains(rec){

			whites = append(whites, rec)
			blackList.Insert(rec)

		}

	}

	//printResList(uid,"suggest from doop", whites, " hourIdx:", jj)

	common.DebugLogPlus(uid ,"suggest from doop", whites, " hourIdx:", jj.HourIdx, "blacklist:", blackList.ToList())

	return whites, nil

}


