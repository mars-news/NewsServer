package app

import (
	"MarsXserver/common"
	"sync"
	"time"
	"MarsXserver/proj"
)

const (

	UpdateRecData_Duration = time.Minute * 5

)

type UHourKey struct {

	uid int
	hourIdx proj.HourIdx

}

type UCatKey struct {

	uid int
	cat int

}



type RecData struct {  //todo clean data

	server *SuggestServer
	hourDatas map[UHourKey]*RecHourData
	hourDataVisitTime map[int]time.Time   //todo gc
	uReadOver map[UHourKey]bool

}


type RecHourData struct {    //each hour recommends for a user
	hub        *RecData
	nids       []int
	readOffset int

	lock sync.Mutex
}


func NewRecData() *RecData{

	return &RecData{
		hourDatas: make(map[UHourKey]*RecHourData),
		hourDataVisitTime: make(map[int]time.Time),
		uReadOver: make(map[UHourKey]bool),
	}
}

func (this *RecData) NewHourData() *RecHourData{

	return &RecHourData{
		hub:        this,
		readOffset: 0,
	}
}

func (this *RecData) Contains(uid int, hourIdx *proj.HourIdx) bool {

	key := UHourKey{uid: uid, hourIdx: *hourIdx}

	_, ok := this.hourDatas[key]

	return ok

}




func (this *RecData) CanUpdateRecData(uid int) bool{

	now := time.Now().UTC()
	key := uid

	tm, ok := this.hourDataVisitTime[key]
	if ok == false{
		return true
	}

	if now.Sub(tm) > UpdateRecData_Duration{

		return true

	}

	return false

}



func (this *RecData) UpdateRecDataTime(uid int){

	now := time.Now().UTC()
	key := uid

	this.hourDataVisitTime[key] = now

}


func (this *RecData) CanRead(uid int, hourIdx *proj.HourIdx) bool {

	key := UHourKey{uid: uid, hourIdx: *hourIdx}


	if over, ok := this.uReadOver[key]; ok == true && over == true{

		return false
	}


	hourData, ok := this.hourDatas[key]

	if ok == false{
		return false
	}

	if hourData.Length() <= 0{
		return false
	}

	offset := hourData.readOffset

	if offset >= len(hourData.nids) {
		return false
	}

	return true

}


func (this *RecData) SaveRecs(uid int, hourIdx *proj.HourIdx, nids []int){

	newHourData := this.NewHourData()

	newHourData.nids = nids

	this.hourDatas[UHourKey{hourIdx: *hourIdx, uid: uid}] = newHourData


	common.DebugLogPlus("save recs:", newHourData.nids)
}


func (this *RecData) GetRecs(uid int, hourIdx *proj.HourIdx, length int) ([]int){

	key := UHourKey{hourIdx: *hourIdx, uid: uid}

	hourData, ok := this.hourDatas[key]
	if ok == false{
		return []int{}
	}

	if this.CanRead(uid, hourIdx) == false{

		return []int{}
	}


	res := hourData.GetRecs(uid, length)

	if len(res) < length{
		this.uReadOver[key] = true
	}

	return res
}



func (this *RecData) GC(delHourIdx *proj.HourIdx){

	for uhKey := range this.hourDatas{
		if proj.HourIdxCompare(&uhKey.hourIdx, delHourIdx) < 0{
			delete(this.hourDatas, uhKey)
		}
	}

	for uhKey := range this.uReadOver{
		if proj.HourIdxCompare(&uhKey.hourIdx, delHourIdx) < 0{
			delete(this.uReadOver, uhKey)
		}
	}
}




func (this *RecHourData) GetRecs(uid int, length int) ([]int){

	this.lock.Lock()
	defer this.lock.Unlock()

	common.DebugLogPlus("hour get recs inner, uid", uid, "wanted", length, "offset", this.readOffset, " nids", this.nids, " nids len", len(this.nids))

	offset := this.readOffset

	if offset >= len(this.nids) {
		return []int{}
	}

	var ii, jj int
	res := make([]int, 0)

	for ii = offset; jj < length && ii < len(this.nids); ii++{

		if this.hub.server.UserHasRead(uid, this.nids[ii]){

			common.DebugLogPlus("get recs user", uid, " read", this.nids[ii], "ii", ii)
			continue
		}

		res = append(res, this.nids[ii])
		jj += 1
		common.DebugLogPlus("get recs user", uid, " add", this.nids[ii], "ii", ii)
	}

	newOffset := offset + ii

	this.readOffset = newOffset

	return res

}


func (this *RecHourData) Length() int{

	return len(this.nids)

}































































