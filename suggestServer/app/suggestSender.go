package app


func (this *SuggestServer) WriteObjRequestMessageDBSync(body interface{}) (interface{}, error){

	return this.TServer.WriteObjRequestMessageToServerByTypeSync(DB_Dialer_Type, body)

}

func (this *SuggestServer) WriteObjRequestMessageRedSync(body interface{}) (interface{}, error){

	return this.TServer.WriteObjRequestMessageToServerByTypeSync(Red_Dialer_Type, body)

}

func (this *SuggestServer) WriteObjRequestMessageDoopSync(body interface{}) (interface{}, error){

	return this.TServer.WriteObjRequestMessageToServerByTypeSync(Doop_Dialer_Type, body)
}


