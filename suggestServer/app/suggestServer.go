package app

import (
	"MarsXserver/orm"
	"MarsXserver/common"
	"MarsXserver/hybridServer"
	"MarsXserver/redmodule"
	"MarsXserver/proj"
)


const (
	DB_Dialer_Type string = "db"
	Red_Dialer_Type string ="red"
	Doop_Dialer_Type string ="doop"

)




const (
	Get_News_Count = 24
	RedGcDurationRate = proj.Collect_Day_Time_Spon
	GC_Interval = proj.Collect_Day_Time_Spon

)

var (
	All_Dialers = []string{DB_Dialer_Type, Red_Dialer_Type, Doop_Dialer_Type}

)


type SimpleCrawlItem struct {
	Id int
	Cat int
}





type SuggestServer struct{
	hybridServer.HybridBaseServer
	Algo *SuggestAlgo


	Sid	int
	Sname string
	Config *SuggestServerConfigData



}

type UserRead struct{
	Uid int32
	Nid int
}

const (
	RedTableRead = "Read"
	RedGcDurationRead = proj.Collect_Day_Time_Spon * 2
)

func init(){

	redmodule.RegisterFlagModel(RedTableRead, new(UserRead), []string{"Uid", "Nid"}, redmodule.RedValueTypeInt, nil, RedGcDurationRate)


}



func NewSuggestServer(configName string) (*SuggestServer, error){

	config_, err := LoadConfig(configName)
	if err != nil{
		return nil, common.ErrorLog("load Config failed", err)
	}

	suggestServer := &SuggestServer{
		Sid: 	config_.Sid,
		Sname:  config_.Name,
		Config: config_,
	}

	suggestServer.InitBaseServer(config_.Name, suggestServer, &config_.TConfig, &config_.HConfig, nil, &config_.RedConfig)

	suggestServer.Orm.RegisterSendFunc(suggestServer.OrmSendFunc)
	suggestServer.Orm.RegisterBytesRspFunc(orm.OrmHandleBytesRspFunc)

	suggestServer.TServer.SetLegalDialerNames(All_Dialers)

	suggestServer.RegisterTcpDialerFinishedHandler(suggestServer.AfterTcpDialerHandler)

	suggestServer.Algo = NewSuggestAlgo(suggestServer)

	suggestServer.AddGcInfoTask("gc", GC_Interval, suggestServer.GC)

	return suggestServer, nil
}




func (this *SuggestServer) OrmSendFunc(expr *orm.XOrmEprData, retCh chan interface{}){

	this.OrmSendFuncByType(DB_Dialer_Type, expr, retCh)
}


func (this *SuggestServer) TcpSendFileFunc(msg interface{},fileName string, data []byte) (interface{}, error){

	return this.TcpSendFileFuncByType(DB_Dialer_Type,
		msg,
		fileName,
		data)

}




func (this *SuggestServer) Run(blockMode bool) error{

	go this.Algo.TimerSchedule()

	return this.RunBaseServer(blockMode)
}



func (this *SuggestServer) AfterTcpDialerHandler(){



}

func (this *SuggestServer) GC(){
	this.Algo.GC()
	this.Red.TimeGCAll()
}



















