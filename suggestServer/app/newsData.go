package app

import (
	"MarsXserver/common"
	"sync"
	"math/rand"
	"math"
	"strconv"
	"MarsXserver/db"
	"MarsXserver/proj"
)

type NewsData struct {

	newsHourMap map[proj.HourIdx]*HourNewsData
	uReadOver map[UHourKey]bool

	lastHourIdx *proj.HourIdx
	newsIdSet *common.MSet

	server *SuggestServer

}


type HourNewsData struct {

	hub	*NewsData

	data      map[int][]*SimpleCrawlItem
	uReadOffset map[UCatKey]int

	lock      sync.RWMutex
}



func NewNewsData() *NewsData{

	return &NewsData{
		newsHourMap: make(map[proj.HourIdx]*HourNewsData),
		uReadOver: make(map[UHourKey]bool),
		newsIdSet: common.NewMSet(),
	}

}




func (this *NewsData) NewHourNewsData() *HourNewsData{

	return &HourNewsData{
		hub:	this,
		data:      make(map[int][]*SimpleCrawlItem),
		uReadOffset: make(map[UCatKey]int),
	}
}



func (this *NewsData) CanRead(uid int, cat_ int, isTop bool, hourIdx *proj.HourIdx)  bool{


	/*uReadKey := UHourKey{
		uid: uid,
		hourIdx: *hourIdx,
	}

	over, ok := this.uReadOver[uReadKey]
	if ok == true && over == true{
		return false
	}*/

	if _, ok := this.newsHourMap[*hourIdx]; ok == false{
		return false
	}

	hourData := this.newsHourMap[*hourIdx]

	if isTop{

		for ii:= 0; ii < db.CAT_COUNT; ii ++ {
			uOffsetKey := UCatKey{uid:uid, cat:ii}

			if _, ok := hourData.data[ii]; ok == true{

				tot := len(hourData.data[ii])
				offset, ok2 := hourData.uReadOffset[uOffsetKey]
				if ok2 == false{
					return true
				}

				if tot > offset{
					return true
				}
			}
		}
	}else{

		uOffsetKey := UCatKey{uid:uid, cat:cat_}

		if _, ok := hourData.data[cat_]; ok == true{

			tot := len(hourData.data[cat_])
			offset, ok2 := hourData.uReadOffset[uOffsetKey]
			if ok2 == false{
				return true
			}

			if tot > offset{
				return true
			}
		}

	}

	return false

}


func (this *NewsData) AddNewsSlice(hourIdx *proj.HourIdx, news []*SimpleCrawlItem){

	if _, ok := this.newsHourMap[*hourIdx]; ok == false{
		this.newsHourMap[*hourIdx] = this.NewHourNewsData()
	}

	this.newsHourMap[*hourIdx].AddNewsSlice(news)

	if this.lastHourIdx == nil || proj.HourIdxCompare(this.lastHourIdx, hourIdx) <= 0{
		this.lastHourIdx = hourIdx
	}
}


func (this *NewsData) GetNewsByCat(uid int, hourIdx *proj.HourIdx, tot int, catid int, blackList *common.MSet)([]*SimpleCrawlItem, error){

	defer func(){

		if err := recover(); err != nil{
			common.ErrorLog("get news by cat", err)
			return
		}
	}()


	newsData, ok := this.newsHourMap[*hourIdx]
	if !ok {
		return nil, common.ErrorLog("no such houridx")
	}

	currRes, err := newsData.GetNews(uid, catid, tot, blackList)
	if err != nil{
		return nil, common.ErrorLog("get cat news failed", uid, hourIdx.HourIdx, tot, err)
	}

	return currRes, nil

}



func (this *NewsData) GetNewsByPercent(uid int, hourIdx *proj.HourIdx, tot int, pers []float64, blackList *common.MSet)([]*SimpleCrawlItem, error){

	defer func(){

		if err := recover(); err != nil{
			common.ErrorLog("get news percent panic:", err)
			return
		}
	}()

/*	common.InfoLog("get news by percent uid:", uid, " hourIdx:", hourIdx.HourIdx)

	uReadKey := UHourKey{
		uid: uid,
		hourIdx: *hourIdx,
	}

	if read, ok := this.uReadOver[uReadKey]; ok == true && read == true{

		return nil, common.ErrorLog("user has read over", *hourIdx)
	}*/


	usable := make(map[int]bool)

	newsData, ok := this.newsHourMap[*hourIdx]
	if !ok {
		return nil, common.ErrorLog("no such houridx")
	}

	for key := range newsData.data{

		usable[key] = true
	}

	res := make([]*SimpleCrawlItem, 0)

	for{
		restTot := tot - len(res)
		if restTot <= 0{
			break
		}

		currPers := make(map[int]float64)
		currPerTot := 0.0

		for key := range newsData.data {

			if usable[key] == false {
				continue
			}

			if key < 0 || key >= len(pers){
				continue
			}

			per := pers[key]

			if per < common.Epsilon{
				usable[key] = false
				continue
			}

			currPers[key] = pers[key]
			currPerTot += pers[key]
		}

		if len(currPers) <= 0 {

			common.InfoLog("no per is available")
			break
		}

		perTimes := 1.0/currPerTot

		for key := range currPers {

			currPers[key] = currPers[key] * perTimes

		}

		has_exec := false

		for key, per := range currPers{

			cnt := int(math.Ceil(per * float64(restTot)))

			if cnt <= 0{
				continue
			}

			currRes, err := newsData.GetNews(uid, key, cnt, blackList)
			if err != nil{
				return nil, common.ErrorLog("get hot cat news failed", uid, key, cnt, err)
			}


			if len(currRes) < cnt {
				usable[key] = false
			}

			restTot -= len(currRes)

			res = append(res, currRes...)

			has_exec = true
		}

		if !has_exec{
			break
		}

	}

	/*if len(res) < tot{

		this.uReadOver[uReadKey] = true

	}*/


	return res, nil

}



func (this *NewsData) GC(delHourIdx *proj.HourIdx){


	for hourIdx := range this.newsHourMap{

		if proj.HourIdxCompare(&hourIdx, delHourIdx) < 0{
			delete(this.newsHourMap, hourIdx)
		}
	}

	for uhKey := range this.uReadOver{

		if proj.HourIdxCompare(&uhKey.hourIdx, delHourIdx) < 0{
			delete(this.uReadOver, uhKey)
		}
	}
}




func (this *HourNewsData) AddNewsSlice(news []*SimpleCrawlItem) {

	this.lock.Lock()
	defer this.lock.Unlock()

	perm := rand.Perm(len(news))

	for ii := range perm{

		newsItem := news[perm[ii]]
		newsId := newsItem.Id

		if this.hub.newsIdSet.Contains(newsId){

			continue

		}

		newsCat := newsItem.Cat

		if _, ok := this.data[newsCat]; ok == false{
			this.data[newsCat] = make([]*SimpleCrawlItem, 0)
		}

		this.data[newsCat] = append( this.data[newsCat], newsItem)

		this.hub.newsIdSet.Insert(newsId)

	}


}


func (this *HourNewsData) GetCatNum(cat int)(int, error){

	this.lock.RLock()
	defer this.lock.RUnlock()

	if _, ok := this.data[cat]; ok == false{
		return 0, common.ErrorLog("cat not exists")
	}

	return len(this.data[cat]), nil


}









func (this *HourNewsData) GetNews(uid int, catid int, length int, blackList *common.MSet) ([]*SimpleCrawlItem, error){

	this.lock.RLock()
	defer this.lock.RUnlock()


	uOffsetKey := UCatKey{uid:uid, cat:catid}

	offset, ok := this.uReadOffset[uOffsetKey]
	if ok == false{
		offset = 0
		this.uReadOffset[uOffsetKey] = 0
	}

	catNews, ok := this.data[catid]
	if ok == false{
		return nil, common.ErrorLog("cat id not exist", catid)
	}
	tot := len(catNews)

	if offset < 0 || offset >= tot{
		common.InfoLog("uid:", strconv.Itoa(uid) , "offset used up catId:", catid)
		return []*SimpleCrawlItem{}, nil
	}

	res := make([]*SimpleCrawlItem, 0)

	cnt := 0
	ii := offset

	common.InfoLog("get news offset:", offset, " len:", length)
	for ;ii < tot && cnt < length; ii++{

		newsItem := catNews[ii]
		if blackList.Contains(newsItem.Id){
			continue
		}

		if this.hub.server.UserHasRead(uid, newsItem.Id){
			common.InfoLog("user ", uid, " has read ", newsItem.Id)
			continue
		}

		blackList.Insert(newsItem.Id)
		res = append(res, newsItem)
		cnt += 1
	}

	this.uReadOffset[uOffsetKey] = ii


	return res, nil

}

























