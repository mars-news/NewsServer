package app

import (
	"encoding/xml"
	"io/ioutil"
	"MarsXserver/common"
	"path/filepath"
	"MarsXserver/tcpserver"
	"MarsXserver/httpserver"
	"MarsXserver/redmodule"
)



type SuggestServerConfigData struct{
	Sid	int 		`xml:"sid"`
	Name	string		`xml:"name"`

	TConfig   tcpserver.TcpConfigData	`xml:"tconfig"`
	HConfig   httpserver.HttpConfigData `xml:"hconfig"`
	RedConfig redmodule.RedisConfigData `xml:"rconfig"`
}



func LoadConfig(name string) (configData *SuggestServerConfigData, err error){

	configData = &SuggestServerConfigData{}

	fileName := filepath.Join(common.DefaultConfPath, name + ".xml")

	content, err := ioutil.ReadFile(fileName)
	if err!= nil{
		common.ErrorLog("read Config failed", err)
		return nil, err
	}

	if err = xml.Unmarshal(content, configData); err != nil{
		common.ErrorLog("unmarshal error", err)
		return nil, err
	}

	configData.RedConfig.Trim()

	return configData, nil
}

























