package app

import (
	"MarsXserver/common"
	"MarsXserver/objMessages"
	"time"
	"MarsXserver/db"
	"bytes"
	"fmt"
	"strconv"
	"MarsXserver/orm"
	"MarsXserver/proj"
)

const (

	Default_Once_Refresh_Count = 5 //how many news to send to users

	Hot_News_Collect_Interval = proj.Collect_Ratings_Time_Span / 2


	Algo_GC_Span = proj.Collect_Day_Time_Spon * 2
)



type SuggestAlgo struct {

	server *SuggestServer

	doopRecData *RecData

	hotNewsData *NewsData
	latestNewsData *NewsData
}



func NewSuggestAlgo(server *SuggestServer) *SuggestAlgo{

	algo := &SuggestAlgo{
		server: server,

		doopRecData: NewRecData(),
		hotNewsData: NewNewsData(),
		latestNewsData: NewNewsData(),
	}

	algo.doopRecData.server = server
	algo.hotNewsData.server = server
	algo.latestNewsData.server = server

	return algo

}


func (this *SuggestAlgo) TimerSchedule(){

	hotNewsTicker := time.Tick(Hot_News_Collect_Interval)
	latestNewsTicker := time.Tick(Hot_News_Collect_Interval)

	for {
		common.InfoLog("suggest timer schedule start...")
		select {
		case <- hotNewsTicker:
			common.InfoLog("suggest timer hot tick")
			if err := this.CollectHotNewsPids(); err != nil{
				common.ErrorLog("collect hot news failed:", err)
			}
		case <- latestNewsTicker:
			common.InfoLog("suggest timer latest tick")
			if err := this.CollectLatestNews(false, 0); err != nil{
				common.ErrorLog("get latest news failed:", err)
			}
		}
	}
}



func (this *SuggestAlgo) CollectLatestNews(admin bool, before int) error {

	retChannel := make(chan []interface{})

	var retErr error
	var expr *orm.XOrmEpr

	if admin && before >= 0{

		hourIdxStart := proj.HourIdxGetNearestPrevIdx()
		hourIdx := proj.CloneHourIdx(hourIdxStart)

		hourIdx.HourIdx -= int64(before)

		dbTime0, dbTime1 := hourIdx.ToDBMinMaxStr()

		expr = this.server.Orm.NewOrmExpr(new(db.CrawlItem)).
			List("id", "category", "timestamp").
			Filter("timestamp", ">", "'" + dbTime0 + "'").
			Filter("timestamp", "<=", "'" + dbTime1 + "'")
	}else{

		now := time.Now().UTC()

		pre := now.Add(-Hot_News_Collect_Interval * 2)
		preStr := common.MakeDBTimeString(pre)

		expr = this.server.Orm.NewOrmExpr(new(db.CrawlItem)).
			List("id", "category", "timestamp").
			Filter("timestamp", ">", "'" + preStr + "'")

	}

	go expr.AsyncMultiRunDefault(retChannel, &retErr)

	refreshCnt := 0

	hourIdxMap := make(map[proj.HourIdx][]*SimpleCrawlItem)

	for retList := range retChannel{
		for _, ret := range retList{

			item := ret.(map[string]interface{})

			id, ok := item["id"].(int32)
			if !ok {
				common.ErrorLog("get id null")
				continue
			}

			kind, ok := item["category"].(int32)
			if !ok{
				common.ErrorLog("get kind null")
				continue
			}

			tm, ok := item["timestamp"].(time.Time)
			if !ok{
				common.ErrorLog("get timestamp null")
				continue
			}

			hourIdx := proj.Time2HourIdx(tm)

			if _, ok := hourIdxMap[*hourIdx]; ok == false{

				hourIdxMap[*hourIdx] = make([]*SimpleCrawlItem, 0)
			}

			hourIdxMap[*hourIdx] = append(hourIdxMap[*hourIdx], &SimpleCrawlItem{
				Id: int(id),
				Cat: int(kind),

			})

			refreshCnt += 1
		}
	}


	if retErr != nil{
		common.ErrorLog(retErr)
	}

	for idx, item := range hourIdxMap{

		this.latestNewsData.AddNewsSlice(&idx, item)

		common.InfoLog("collect hourIdx",  idx, " len", len(item))

	}

	common.InfoLog("collect latest tot:", refreshCnt)

	return nil

}





func (this *SuggestAlgo) CollectHotNewsPids() error {

	var nextHourIdx *proj.HourIdx
	var err error

	currHourIdx := proj.HourIdxGetCurrIdx()

	if this.hotNewsData.lastHourIdx == nil{

		nextHourIdx = proj.HourIdxGetNearestPrevIdx()
	}else{

		nextHourIdx = proj.HourIdxNextIdx(this.hotNewsData.lastHourIdx)     //from prev houridx to next

		if err != nil{
			return err
		}
	}

	if proj.HourIdxCompare(nextHourIdx, currHourIdx) >= 0{   //time of collecting hot cannot ahead of curridx
		common.InfoLog("collect hot news time too early", time.Now().UTC(), " hourIdx:", currHourIdx)

		return nil
	}


	ret, err := this.server.WriteObjRequestMessageRedSync(
		&objMessages.SSCollectHotNewsRequest{
			HourIdx: nextHourIdx.HourIdx,
		})


	rsp, ok := ret.(*objMessages.SSCollectHotNewsResponse)

	if ok == false || rsp.Stat != 0{
		return common.ErrorLog("collect rsp type error", rsp.Stat)
	}

	if len(rsp.Nids) <= 0{
		return common.ErrorLog("get hot news empty")
	}

	var idSetBuff bytes.Buffer

	idSetBuff.WriteString("(")

	for ii := 0; ii < len(rsp.Nids); ii++ {

		nid := rsp.Nids[ii]
		if ii < len(rsp.Nids) -1{
			idSetBuff.WriteString(fmt.Sprintf("%s,", strconv.Itoa(nid)))
		}else{
			idSetBuff.WriteString(fmt.Sprintf("%s", strconv.Itoa(nid)))
		}
	}

	idSetBuff.WriteString(")")

	var retErr error
	retChannel := make(chan []interface{})

	go this.server.Orm.NewOrmExpr(new(db.CrawlItem)).List("id", "category").Filter("id", " in ", idSetBuff.String() ).AsyncMultiRunDefault(retChannel, &retErr)

	newsItems := make([]*SimpleCrawlItem, 0)

	for retlist := range retChannel{

		for _, ret := range retlist{

			item := ret.(map[string]interface{})

			nid, ok := item["id"].(int32)
			if !ok {
				common.ErrorLog("get id null")
				continue
			}

			cat, ok := item["category"].(int32)
			if !ok {
				common.ErrorLog("get category null")
				continue
			}

			newsItems = append(newsItems, &SimpleCrawlItem{Id: int(nid), Cat: int(cat)})
		}
	}

	if retErr != nil{
		return common.ErrorLog("get cats failed")
	}


	this.hotNewsData.AddNewsSlice(nextHourIdx, newsItems)

	common.InfoLog("collect hot news finished", time.Now().UTC(), " new houridx:", *nextHourIdx, " len:", len(newsItems))

	return nil

}


func (this *SuggestAlgo) GC(){

	now := proj.HourIdxGetCurrIdx()

	delHourIdx := now.AddDuration(-Algo_GC_Span)

	common.InfoLog("suggest algo gc hourIdx:", delHourIdx.ToDBStr())

	this.doopRecData.GC(delHourIdx)
	this.latestNewsData.GC(delHourIdx)
	this.hotNewsData.GC(delHourIdx)

}
















