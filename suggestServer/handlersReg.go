package suggestServer

import (
	"MarsXserver/suggestServer/app"

	"MarsXserver/tcpserver"
	"reflect"
	"MarsXserver/suggestServer/objHandler"
	"MarsXserver/objMessages"
)


func initRegisterTcpHandlers(sServer *app.SuggestServer){


	tcpserver.RegisterHandler(
		int(objMessages.MessageId_Suggest_GetSuggest),
		&tcpserver.HandlerItem{
			MsgProtoType: tcpserver.MessageProtoType_ObjRequest,
			Handler:      &objHandler.SSSuggestRequestHandler{},
			ReqMsgType:   reflect.TypeOf(objMessages.SuggestGetSuggestRequest{}),
			RspMsgType:   reflect.TypeOf(objMessages.SuggestGetSuggestResponse{}),
		})

	tcpserver.RegisterHandler(
		int(objMessages.MessageId_Suggest_StartCollect),
		&tcpserver.HandlerItem{
			MsgProtoType: tcpserver.MessageProtoType_ObjRequest,
			Handler:      &objHandler.SSSuggestStartCollectRequestHandler{},
			ReqMsgType:   reflect.TypeOf(objMessages.SuggestStartCollectRequest{}),
			RspMsgType:   reflect.TypeOf(objMessages.SuggestStartCollectResponse{}),
		})

}


func initRegisterHttpHandlers(aServer *app.SuggestServer){

	HttpHandlers := make(map[string]interface{})

	aServer.RegisterHttpHandlers(HttpHandlers)
}



func initRegisterFileHandlers(aServer *app.SuggestServer){



}

func InitRegisterHandlers(aServer *app.SuggestServer){

	initRegisterTcpHandlers(aServer)
	initRegisterFileHandlers(aServer)
	initRegisterHttpHandlers(aServer)

}

