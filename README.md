[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url]

## A News App Server 

This is a server for a news app written in Go, comprising multiple sub-servers.
The web framework used is self-developed and can be found at https://gitlab.com/mars-news/marsXserver

1. Crawler server (in the crawler folder), which reads crawling rules from the database, periodically crawls news web pages, and stores them in the database.
2. Collaborative filtering algorithm server (in the mDoopServer folder), implementing a news data recommendation algorithm based on the collaborative filtering method.
3. Recommendation push module (in the suggestServer folder), integrating the results of hot news and the recommendation algorithm.
4. Web server (in the endServer folder), implementing APIs for App requests.
5. Admin backend server (in the adminServer folder), used for editing crawler rules, managing news and user data.


[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555


[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[linkedin-url]: https://www.linkedin.com/in/shaokun-feng/