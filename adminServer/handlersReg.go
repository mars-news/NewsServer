package adminServer

import (
	"MarsXserver/adminServer/app"
	"MarsXserver/adminServer/httpHandler"
)


func initRegisterHttpHandlers(aServer *app.AdminServer){

	HttpHandlers := make(map[string]interface{})
	/*
	HttpHandlers["/admin/login"] = &httpHandler.LoginController{}

	HttpHandlers["/admin/crawler/info"] = &httpHandler.CrawlerInfoController{}*/

	HttpHandlers["/admin/login"] = &httpHandler.AdminLoginController{}

	HttpHandlers["/admin/crawler/run"] = &httpHandler.CrawlerRunController{}
	HttpHandlers["/admin/rules"] = &httpHandler.RuleIndexController{}
	HttpHandlers["/admin/rules/add"] = &httpHandler.RuleAddController{}
	HttpHandlers["/admin/rules/list"] = &httpHandler.RuleListController{}
	HttpHandlers["/admin/rules/edit"] = &httpHandler.RuleEditController{}
	HttpHandlers["/admin/rules/copy"] = &httpHandler.RuleCopyController{}
	HttpHandlers["/admin/rules/test"] = &httpHandler.RuleTestController{}

	HttpHandlers["/admin/rule-sources/add"] = &httpHandler.RuleSourceAddController{}
	HttpHandlers["/admin/rule-sources/list"] = &httpHandler.RuleSourceListController{}

	HttpHandlers["/admin/suggest/collect"] = &httpHandler.SuggestCollectController{}

	HttpHandlers["/admin/doop/calc"] = &httpHandler.DoopCalcController{}

	aServer.RegisterHttpHandlers(HttpHandlers)
}



func initRegisterFileHandlers(aServer *app.AdminServer){



}

func InitRegisterHandlers(aServer *app.AdminServer){

	initRegisterFileHandlers(aServer)
	initRegisterHttpHandlers(aServer)

}

