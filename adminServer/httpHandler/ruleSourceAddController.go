package httpHandler

import (
	"MarsXserver/httpserver"
	"MarsXserver/common"
	"MarsXserver/adminServer/app"
	"MarsXserver/objMessages"
	"MarsXserver/db"
	"path/filepath"
	"MarsXserver/proj"
)


type RuleSourceAddController struct{
	httpserver.BaseHttpHandler
}



func (this *RuleSourceAddController) Post(ctx *httpserver.HttpContext){

	common.InfoLog("post handler rule source add")

	form, err := this.GetForm()
	if err != nil{
		ctx.Output.WriteJsonError(common.ErrorLog("get form failed", " err:", err))
		return
	}

	sourceName := form.Get("srcName")
	if len(sourceName) <= 0{
		ctx.Output.WriteJsonError(common.ErrorLog("source name empty"))
		return
	}

	showName := form.Get("ShowName")
	if len(showName) <= 0{
		ctx.Output.WriteJsonError(common.ErrorLog("show name empty"))
		return
	}


	fileName, fileData, err := this.GetFormFileBytes("uploadpic")

	if err != nil{
		ctx.Output.WriteJsonError(err)
		return
	}

	thumbData, err := proj.GetThumbnailBytes(fileData)
	if err != nil{
		ctx.Output.WriteJsonError(err)
		return
	}


	aServer := ctx.Server.GetParentContext().(*app.AdminServer)


	//timeFolder := time.Now().Format("2006-01-02")

	rspInf, err := aServer.TcpSendFileFunc(
			&objMessages.CrawlSourceImageRequest{
				SrcName: sourceName,
			},
			fileName,
			thumbData,
		)
	if err != nil{

		common.ErrorLog("write json error:", err)
		ctx.Output.WriteJsonError(err)
		return
	}

	rsp, ok := rspInf.(*objMessages.CrawlSourceImageResponse)
	if ok == false{
		common.ErrorLog("not a source image response:", err)
		ctx.Output.WriteJsonError(err)
		return
	}

	common.InfoLog("file rsp :", rsp.SrcPath)

	crawlSrc := db.CrawlSource{
		Name: sourceName,
		ShowName: showName,
		PicPath: sourceName + filepath.Ext(fileName),
	}

	common.InfoLog("save source:", crawlSrc)

	insertId, err := aServer.Orm.Save(&crawlSrc)
	if err != nil{
		ctx.Output.WriteJsonError(common.ErrorLog("save rule source failed, ", insertId, err))
		return
	}


	ctx.Output.WriteJson(struct {}{})


}
