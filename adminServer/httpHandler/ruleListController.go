package httpHandler

import (
	"MarsXserver/httpserver"
	"MarsXserver/common"
	"MarsXserver/orm"
	"strconv"
	"MarsXserver/db"
	"MarsXserver/adminServer/app"
)


type RuleListController struct{
	httpserver.BaseHttpHandler

}

const (
	DefaultRuleListPageCount = 80

)





func (this *RuleListController) Post(ctx *httpserver.HttpContext){

	common.InfoLog("post handler:", ctx)
	form, err := this.GetForm()
	if err != nil{
		ctx.Output.WriteJsonError(common.ErrorLog("get form vals failed, ", err))
		return
	}

	offsetId_str := form.Get("offset")

	offsetId, err := strconv.ParseInt(offsetId_str, 10, 32)
	if err != nil{
		ctx.Output.WriteJsonError(common.ErrorLog("parse offset id failed, ", offsetId_str, err))
		return
	}

	common.InfoLog("url vals:", offsetId)

	aServer := ctx.Server.GetParentContext().(*app.AdminServer)

	expr := aServer.Orm.NewOrmExpr(&db.CrawlRule{})
	var stat *orm.XOrmEpr

	if offsetId <= 0{
		stat = expr.List().Limit(DefaultRuleListPageCount, 0)
	}else{
		stat =  expr.List().Filter("id", ">", strconv.Itoa(int(offsetId))).Limit(DefaultRuleListPageCount, 0)
	}


	retChannel := make(chan []interface{})

	var retErr error
	var resList = make([]interface{}, 0)


	go stat.AsyncMultiRunDefault(retChannel, &retErr)

	for retlist := range retChannel{

		resList = append(resList, retlist...)

	}

	if retErr != nil{
		ctx.Output.WriteJsonError(common.ErrorLog("run orm expr failed", retErr))
		return
	}

	ctx.Output.WriteJson(resList)

}
