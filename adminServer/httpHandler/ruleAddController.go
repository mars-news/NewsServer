package httpHandler

import (
	"MarsXserver/httpserver"
	"MarsXserver/common"
	"strconv"
	"MarsXserver/db"
	"MarsXserver/adminServer/app"
)


type RuleAddController struct{
	httpserver.BaseHttpHandler

}

type RuleAddResponse struct{

	httpserver.JsonResponse

}


func (this *RuleAddController) Post(ctx *httpserver.HttpContext){

	common.InfoLog("post handler:", ctx)

	var rule db.CrawlRule

	form, err := this.ParseForm(&rule)
	if err != nil{
		ctx.Output.WriteJsonError(common.ErrorLog("parse crawl rule form failed err:", err))
		return
	}

	is_featured := form.Get("IsFeatured")
	common.InfoLog("is featured:", is_featured, "rule:show", rule.ShowKind, "rule:Cat:", rule.Category)
	if is_featured == "true"{
		rule.Feature = rule.Category
	}else{
		rule.Feature = 0
	}

	sourceId_str := form.Get("SourceId")
	sourceId, err := strconv.Atoi(sourceId_str)
	if err != nil{
		ctx.Output.WriteJsonError(common.ErrorLog("parse int failed:", sourceId_str))
		return
	}


	rule.Source = &db.CrawlSource{
		Id: int32(sourceId),
	}

	aServer := ctx.Server.GetParentContext().(*app.AdminServer)

	insertId, err := aServer.Orm.Save(&rule)
	if err != nil{
		ctx.Output.WriteJsonError(common.ErrorLog("save rule failed, ", insertId, err))
		return
	}

	ctx.Output.WriteJson(struct {}{})

}


