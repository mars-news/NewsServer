package httpHandler

import (
	"MarsXserver/httpserver"
	"MarsXserver/common"
	"MarsXserver/adminServer/app"
	"MarsXserver/objMessages"
	"strconv"
)


type SuggestCollectController struct{
	httpserver.BaseHttpHandler
}


type CSSuggestCollectResponse struct {

	Stat	int

}


func (this *SuggestCollectController) Get(ctx *httpserver.HttpContext){

	common.InfoLog("get suggest collect handler")

	var errAll error
	var aServer *app.AdminServer
	var hotStr, latestStr, beforeStr string
	var before int
	var ok bool

	var ret interface{}
	var collectReq *objMessages.SuggestStartCollectRequest
	var collectRsp *objMessages.SuggestStartCollectResponse
	var rsp *CSSuggestCollectResponse


	form, err := this.GetForm()
	if err != nil{
		errAll = common.ErrorLog("parse news refresh form failed err:", err)
		goto output
	}

	collectReq = new(objMessages.SuggestStartCollectRequest)

	hotStr = form.Get("hot")
	if len(hotStr) > 0{
		collectReq.Hot = true
	}

	latestStr = form.Get("latest")
	if len(latestStr) > 0{
		collectReq.Latest = true
	}

	beforeStr = form.Get("before")
	if len(beforeStr) > 0{
		before, err = strconv.Atoi(beforeStr)
		if err != nil{
			errAll = err
			goto output
		}
		collectReq.Before = before
	}else {
		collectReq.Before = -1
	}



	aServer = ctx.Server.GetParentContext().(*app.AdminServer)

	ret, err = aServer.WriteObjRequestMessageSuggestSync(
		collectReq,
	)

	if err != nil{
		errAll = err
		goto output
	}

	collectRsp, ok = ret.(*objMessages.SuggestStartCollectResponse)
	if ok != true{
		errAll = common.ErrorLog("suggest collect rsp type failed")
		goto output
	}

	common.InfoLog("vote rsp ", collectRsp.Stat)

	rsp = &CSSuggestCollectResponse{
		Stat: collectRsp.Stat,
	}

output:

	if errAll != nil {
		common.InfoLog("suggest collect failed, ", err)
		ctx.Output.WriteJsonError(err)
	}else{

		ctx.Output.WriteJson(rsp)

	}

}
























