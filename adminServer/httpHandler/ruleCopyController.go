package httpHandler

import (
	"MarsXserver/httpserver"
	"MarsXserver/common"
	"strconv"
	"MarsXserver/db"
	"MarsXserver/adminServer/app"
)


type RuleCopyController struct{
	httpserver.BaseHttpHandler

}

type RuleCopyResponse struct{

	httpserver.JsonResponse


}


func (this *RuleCopyController) Get(ctx *httpserver.HttpContext){

	common.InfoLog("post handler rule copy")

	var errAll error
	var aServer *app.AdminServer
	var ruleId int
	var ruleStr string

	var rsp *RuleCopyResponse


	aServer = ctx.Server.GetParentContext().(*app.AdminServer)

	var rule db.CrawlRule

	form, err := this.GetForm()
	if err != nil{
		errAll = common.ErrorLog("parse form failed err:", err)
		goto output
	}

	ruleStr = form.Get("ruleId")
	ruleId, err = strconv.Atoi(ruleStr)
	if err != nil{
		errAll = common.ErrorLog("parse int failed:", ruleId)
		goto output
	}

	rule = db.CrawlRule{
		Id: int32(ruleId),
	}

	err = aServer.Orm.Read(&rule, false)
	if err != nil{
		errAll = common.ErrorLog("read rule failed, id:", rule.Id, err)
		goto output
	}


	rule.Id = 0

	_, err = aServer.Orm.Save(&rule)
	if err != nil{
		errAll = common.ErrorLog("save rule failed, id:", rule.Id, err)
		goto output
	}


output:

	if errAll != nil {
		ctx.Output.WriteJsonError(common.ErrorLog("copy rule failed, ", err))
	}else{

		ctx.Output.WriteJson(rsp)

	}


}


