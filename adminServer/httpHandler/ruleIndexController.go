package httpHandler

import (
	"MarsXserver/httpserver"
	"MarsXserver/common"
	"MarsXserver/db"
)


type RuleIndexController struct{
	httpserver.BaseHttpHandler

}




func (this *RuleIndexController) Get(ctx *httpserver.HttpContext){

	common.InfoLog("post handler:", ctx)

	this.NeedRender("admin_rule_index_view.html", map[interface{}]interface{}{
		"host": ctx.Req.Host,
		"Cats": db.GetAllCat(),
	})
}


