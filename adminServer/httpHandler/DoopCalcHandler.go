package httpHandler

import (
	"MarsXserver/httpserver"
	"MarsXserver/common"
	"MarsXserver/adminServer/app"
	"MarsXserver/objMessages"
	"strconv"
)


type DoopCalcController struct{
	httpserver.BaseHttpHandler
}


type CSDoopCalcResponse struct {

	Stat	int

}


func (this *DoopCalcController) Get(ctx *httpserver.HttpContext){

	common.InfoLog("doop calc handler")

	var errAll error
	var aServer *app.AdminServer
	var dayStr, hourStr, beforeStr string
	var before int
	var ok bool

	var ret interface{}
	var doopReq *objMessages.SSDoopStartCalcRequest
	var doopRsp *objMessages.SSDoopStartCalcResponse
	var rsp *CSDoopCalcResponse


	form, err := this.GetForm()
	if err != nil{
		errAll = common.ErrorLog("parse news refresh form failed err:", err)
		goto output
	}


	doopReq = new(objMessages.SSDoopStartCalcRequest)

	dayStr = form.Get("day")
	if len(dayStr) > 0{
		doopReq.Day = true
	}

	hourStr = form.Get("hour")
	if len(hourStr) > 0{
		doopReq.Hour = true
	}

	beforeStr = form.Get("before")
	if len(beforeStr) > 0{
		before, err = strconv.Atoi(beforeStr)
		if err != nil{
			errAll = err
			goto output
		}
		doopReq.Before = before
	}


	aServer = ctx.Server.GetParentContext().(*app.AdminServer)

	ret, err = aServer.WriteObjRequestMessageDoopSync(
		doopReq,
	)

	if err != nil{
		errAll = err
		goto output
	}

	doopRsp, ok = ret.(*objMessages.SSDoopStartCalcResponse)
	if ok != true{
		errAll = common.ErrorLog("start calc rsp type failed")
		goto output
	}

	common.InfoLog("start calc rsp ", doopRsp.Stat)

	rsp = &CSDoopCalcResponse{
		Stat: doopRsp.Stat,
	}

output:

	if errAll != nil {
		ctx.Output.WriteJsonError(common.ErrorLog("doop calc failed, ", err))
	}else{

		ctx.Output.WriteJson(rsp)

	}

}
























