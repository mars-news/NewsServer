package httpHandler

import (
	"MarsXserver/httpserver"
	"MarsXserver/common"
	"strconv"
	"MarsXserver/db"
	"MarsXserver/adminServer/app"
)


type RuleEditController struct{
	httpserver.BaseHttpHandler

}

type RuleEditResponse struct{

	httpserver.JsonResponse


}


func (this *RuleEditController) Post(ctx *httpserver.HttpContext){

	common.InfoLog("post handler rule edit")

	aServer := ctx.Server.GetParentContext().(*app.AdminServer)

	var rule db.CrawlRule

	form, err := this.ParseForm(&rule)
	if err != nil{
		ctx.Output.WriteJsonError(common.ErrorLog("parse crawl rule form failed err:", err))
		return
	}

	sourceId_str := form.Get("SourceId")
	sourceId, err := strconv.Atoi(sourceId_str)
	if err != nil{
		ctx.Output.WriteJsonError(common.ErrorLog("parse int failed:", sourceId_str))
		return
	}

	is_featured := form.Get("IsFeatured")
	common.InfoLog("is featured:", is_featured, "rule:show", rule.ShowKind, "rule:Cat:", rule.Category)
	if is_featured == "featured"{
		rule.Feature = rule.Category
	}else{
		rule.Feature = 0
	}

	ruleId_str := form.Get("RuleId")
	ruleId, err := strconv.Atoi(ruleId_str)
	if err != nil{
		ctx.Output.WriteJsonError(common.ErrorLog("parse int failed:", ruleId_str))
		return
	}

	if ruleId <= 0{
		ctx.Output.WriteJsonError(common.ErrorLog("rule id failed:", ruleId))
		return
	}

	rule.Id = int32(ruleId)

	rule.Source = &db.CrawlSource{
		Id: int32(sourceId),
	}

	err = aServer.Orm.Update(&rule)
	if err != nil{
		ctx.Output.WriteJsonError(common.ErrorLog("update rule failed, id:", rule.Id, err))
		return
	}

	ctx.Output.WriteJson(struct {}{})

}


