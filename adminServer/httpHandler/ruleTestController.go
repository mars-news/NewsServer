package httpHandler

import (
	"MarsXserver/httpserver"
	"MarsXserver/common"
	"MarsXserver/db"
	"fmt"
	"encoding/json"
	"time"
)


type RuleTestController struct{
	httpserver.BaseHttpHandler

}

const (
	Rule_Value_Type_Attr = 1
	Rule_Value_Type_Text = 2
	Rule_Value_Type_Html = 3

	Test_Each_Sleep = time.Second * 3

)

type RuleObject struct {

	Key string
	ValType int
	Val string

}

func NewRuleObject(jstr string) *RuleObject{

	ruleObj := new(RuleObject)
	json.Unmarshal([]byte(jstr), ruleObj)
	return ruleObj
}


func (this *RuleObject) String() string{

	return fmt.Sprintf("key:%s, valType:%d, val:%s", this.Key, this.ValType, this.Val)

}

func (this *RuleTestController) Post(ctx *httpserver.HttpContext){

	common.InfoLog("rule test post handler:", ctx)

	var rule = new(db.CrawlRule)

	form, err := this.ParseForm(rule)
	if err != nil{
		ctx.Output.WriteJsonError(common.ErrorLog("parse crawl rule form failed err:", err))
		return
	}

	testName := form.Get("item")

	//aServer := ctx.Server.GetParentContext().(*app.AdminServer)

	switch testName {
	case "listRule":

		listRuleObj := httpserver.NewRuleObject(rule.ListRule)

		res, err := httpserver.NewHtmlPageAnalyzer(rule.Addr, false).
			ForEach("list", listRuleObj).
			GetHtml().Run()

		if err != nil{
			ctx.Output.WriteJsonError(err)
		}else{

			scanList := make([]string, 0)

			length := res.At("list").Len()

			for ii := 0; ii < length; ii++{

				item, err := res.At("list").Idx(ii).Html()
				if err != nil{
					ctx.Output.WriteJsonError(err)
					break

				}
				scanList = append(scanList, item)
			}

			ctx.Output.WriteJson(scanList)
		}
		break
	case "listTitle":
		listRuleObj := httpserver.NewRuleObject(rule.ListRule)
		titleRuleObj := httpserver.NewRuleObject(rule.ListTitleRule)

		res, err := httpserver.NewHtmlPageAnalyzer(rule.Addr, false).
			ForEach("list", listRuleObj).
			Get("title", titleRuleObj).Run()

		if err != nil{
			ctx.Output.WriteJsonError(err)
		}else{

			scanList := make([]string, 0)

			length := res.At("list").Len()

			for ii := 0; ii < length; ii++{

				item, err := res.At("list").Idx(ii).Get("title")
				if err != nil{
					ctx.Output.WriteJsonError(err)
					break

				}
				scanList = append(scanList, item)
			}

			ctx.Output.WriteJson(scanList)
		}

		break

	case "listUrl":
		listRuleObj := httpserver.NewRuleObject(rule.ListRule)
		listUrlObj := httpserver.NewRuleObject(rule.ListUrlRule)

		res, err := httpserver.NewHtmlPageAnalyzer(rule.Addr, false).
			ForEach("list", listRuleObj).
			Get("lurl", listUrlObj).Run()

		if err != nil{
			ctx.Output.WriteJsonError(err)
		}else{

			scanList := make([]string, 0)

			length := res.At("list").Len()

			for ii := 0; ii < length; ii++{

				item, err := res.At("list").Idx(ii).Get("lurl")
				if err != nil{
					ctx.Output.WriteJsonError(err)
					break

				}
				scanList = append(scanList, item)
			}

			ctx.Output.WriteJson(scanList)
		}
		break

	case "contentTitle":
		listRuleObj := httpserver.NewRuleObject(rule.ListRule)
		listUrlObj := httpserver.NewRuleObject(rule.ListUrlRule)
		contentTitleObj := httpserver.NewRuleObject(rule.ContentTitleRule)

		res, err := httpserver.NewHtmlPageAnalyzer(rule.Addr, false).
			ForEach("list", listRuleObj).
			Enter("content", listUrlObj).
			Get("title", contentTitleObj).Run()

		if err != nil{
			ctx.Output.WriteJsonError(err)
		}else{

			scanList := make([]string, 0)

			length := res.At("list").Len()

			for ii := 0; ii < length; ii++{

				item, err := res.At("list").Idx(ii).At("content").Get("title")
				if err != nil{
					ctx.Output.WriteJsonError(err)
					break

				}
				scanList = append(scanList, item)
			}

			ctx.Output.WriteJson(scanList)
		}
		break

	case "contentTitleImage":
		listRuleObj := httpserver.NewRuleObject(rule.ListRule)
		listUrlObj := httpserver.NewRuleObject(rule.ListUrlRule)
		contentTitleImageObj := httpserver.NewRuleObject(rule.ContentTitleImageRule)

		res, err := httpserver.NewHtmlPageAnalyzer(rule.Addr, true).SetTest(0).
		ForEach("list", listRuleObj).
		Enter("content", listUrlObj).
		Get("title", contentTitleImageObj).Run()


		if res == nil{
			ctx.Output.WriteJsonError(err)
			return
		}

		scanList := make([]string, 0)

		length := res.At("list").Len()

		for ii := 0; ii < length && ii < httpserver.Test_For_Each_Count; ii++{

			item, err := res.At("list").Idx(ii).At("content").Get("title")
			if err != nil{
				scanList = append(scanList, "")

			}

			scanList = append(scanList, item)
			res.Err = nil
		}

		ctx.Output.WriteJson(scanList)

		break
	case "contentTitleImageText":
		listRuleObj := httpserver.NewRuleObject(rule.ListRule)
		listUrlObj := httpserver.NewRuleObject(rule.ListUrlRule)
		contentTitleImageTextObj := httpserver.NewRuleObject(rule.ContentTitleImageTextRule)

		res, err := httpserver.NewHtmlPageAnalyzer(rule.Addr, true).SetTest(0).
			ForEach("list", listRuleObj).
			Enter("content", listUrlObj).
			Get("title", contentTitleImageTextObj).Run()


		if res == nil{
			ctx.Output.WriteJsonError(err)
			return
		}

		scanList := make([]string, 0)

		length := res.At("list").Len()

		for ii := 0; ii < length && ii < httpserver.Test_For_Each_Count; ii++{

			item, err := res.At("list").Idx(ii).At("content").Get("title")
			if err != nil{
				scanList = append(scanList, "")

			}

			scanList = append(scanList, item)
			res.Err = nil
		}

		ctx.Output.WriteJson(scanList)

		break
	case "contentBody":
		listRuleObj := httpserver.NewRuleObject(rule.ListRule)
		listUrlObj := httpserver.NewRuleObject(rule.ListUrlRule)
		contentBodyObj := httpserver.NewRuleObject(rule.ContentBodyRule)

		res, err := httpserver.NewHtmlPageAnalyzer(rule.Addr, true).SetTest(0).
			ForEach("list", listRuleObj).
			Enter("content", listUrlObj).
			Get("body", contentBodyObj).Run()


		if res == nil{
			ctx.Output.WriteJsonError(err)
			return
		}

		scanList := make([]string, 0)

		length := res.At("list").Len()

		for ii := 0; ii < length && ii < httpserver.Test_For_Each_Count; ii++{

			item, err := res.At("list").Idx(ii).At("content").Get("body")
			if err != nil{
				scanList = append(scanList, "")

			}

			scanList = append(scanList, item)
			res.Err = nil
		}

		ctx.Output.WriteJson(scanList)
		break
	case "contentBodyExcludeRule":
		listRuleObj := httpserver.NewRuleObject(rule.ListRule)
		listUrlObj := httpserver.NewRuleObject(rule.ListUrlRule)
		contentBodyExcludeObj := httpserver.NewRuleObject(rule.ContentBodyExcludeRule)

		res, err := httpserver.NewHtmlPageAnalyzer(rule.Addr, true).
			ForEach("list", listRuleObj).
			Enter("content", listUrlObj).
			Get("body", contentBodyExcludeObj).Run()


		if res == nil{
			ctx.Output.WriteJsonError(err)
			return
		}

		scanList := make([]string, 0)

		length := res.At("list").Len()

		for ii := 0; ii < length; ii++{

			item, err := res.At("list").Idx(ii).At("content").Get("body")
			if err != nil{
				scanList = append(scanList, "")

			}

			scanList = append(scanList, item)
			res.Err = nil
		}

		ctx.Output.WriteJson(scanList)
		break
	}


}













