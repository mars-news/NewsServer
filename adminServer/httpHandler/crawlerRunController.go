package httpHandler

import (
	"MarsXserver/httpserver"
	"MarsXserver/common"
	"github.com/gorilla/websocket"
	"MarsXserver/adminServer/app"
	"MarsXserver/objMessages"
)


type CrawlerRunController struct{
	httpserver.BaseHttpHandler
}

type CrawlerRunResponse struct{
	httpserver.JsonResponse
}


func (this *CrawlerRunController) Get(ctx *httpserver.HttpContext){

	common.InfoLog("get handler crawler run")

	//id := conn.Request().URL.Query().Get("Id")

	ws, err := websocket.Upgrade(ctx.Rsp, ctx.Req, nil, 1024, 1024)
	if _, ok := err.(websocket.HandshakeError); ok{
		ctx.Output.WriteJsonError(common.ErrorLog("not a websocket handshake"))
		return
	}else if err != nil{
		return
	}

	common.InfoLog("websocket is built")

	ctx.Ws = ws

	sr := ctx.Server.WebSocketHub.NewWebSocketRouter(0, ctx)

	ctx.WsRouter = sr

	go sr.Run()

	type tmpCb struct{
		id int
		ctx *httpserver.HttpContext
	}

	common.InfoLog("observer started")


	root := ctx.Server.GetParentContext()
	aServer, ok := root.(*app.AdminServer)

	if ok == false{
		sr.WriteMessage(common.ErrorLog("root context not admin server ").Error(), struct {}{})
		return
	}


	aServer.WriteObjRequestMessageToCrawlerCallback(
		&objMessages.CrawlerStartRequest{

		},
		func(rsp interface{}, err error){

			if err != nil {
				sr.WriteMessage("crawler error", struct {}{})
			}else{
				sr.WriteMessage("crawler started", struct {}{})
			}


		},
	)



}
