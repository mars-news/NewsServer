package httpHandler

import (
	"MarsXserver/httpserver"
	"MarsXserver/common"
	"MarsXserver/db"
	"MarsXserver/adminServer/app"
)


type RuleSourceListController struct{
	httpserver.BaseHttpHandler

}




func (this *RuleSourceListController) Post(ctx *httpserver.HttpContext){

	common.InfoLog("post handler rule source list")

	aServer := ctx.Server.GetParentContext().(*app.AdminServer)

	list, err :=  aServer.Orm.NewOrmExpr(&db.CrawlSource{}).List().RunDirectReturn()
	if err != nil{
		ctx.Output.WriteJsonError(common.ErrorLog("run orm expr failed", err))
		return
	}

	ctx.Output.WriteJson(list)

}
