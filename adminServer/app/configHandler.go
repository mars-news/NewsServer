package app

import (
	"encoding/xml"
	"io/ioutil"
	"MarsXserver/common"
	"path/filepath"
	"MarsXserver/tcpserver"
	"MarsXserver/httpserver"
)



type adminServerConfigData struct{
	Sid	int 		`xml:"sid"`
	Name	string		`xml:"name"`

	TConfig tcpserver.TcpConfigData	`xml:"tconfig"`
	HConfig httpserver.HttpConfigData `xml:"hconfig"`
}



func LoadConfig(name string) (configData *adminServerConfigData, err error){

	configData = &adminServerConfigData{}

	fileName := filepath.Join(common.DefaultConfPath, name + ".xml")

	content, err := ioutil.ReadFile(fileName)
	if err!= nil{
		common.ErrorLog("read Config failed", err)
		return nil, err
	}

	if err = xml.Unmarshal(content, configData); err != nil{
		common.ErrorLog("unmarshal error", err)
		return nil, err
	}

	return configData, nil
}

























