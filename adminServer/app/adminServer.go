package app

import (
	"MarsXserver/orm"
	"MarsXserver/common"
	"MarsXserver/hybridServer"
	"MarsXserver/httpserver"
)


const (
	DB_Dialer_Type string = "db"
	Crawler_Dialer_Type string = "crawler"
	Doop_Dialer_Type string = "doop"
	Suggest_Dialer_Type string = "suggest"
)

var (
	Admin_Dialers = []string{DB_Dialer_Type, Crawler_Dialer_Type, Doop_Dialer_Type, Suggest_Dialer_Type}

)


type AdminServer struct{
	hybridServer.HybridBaseServer

	Sid	int
	Sname string
	Config *adminServerConfigData

}



func NewAdminServer(configName string) (*AdminServer, error){

	config_, err := LoadConfig(configName)
	if err != nil{
		return nil, common.ErrorLog("load Config failed", err)
	}

	adminServer := &AdminServer{
		Sid: 	config_.Sid,
		Sname:  config_.Name,
		Config: config_,
	}


	adminServer.InitBaseServer(config_.Name, adminServer, &config_.TConfig, &config_.HConfig, nil, nil)

	adminServer.Orm.RegisterSendFunc(adminServer.OrmSendFunc)
	adminServer.Orm.RegisterBytesRspFunc(orm.OrmHandleBytesRspFunc)

	adminServer.TServer.SetLegalDialerNames(Admin_Dialers)

	adminServer.HServer.RegisterAuthFunc(httpserver.Auth)

	return adminServer, nil
}




func (this *AdminServer) OrmSendFunc(expr *orm.XOrmEprData, retCh chan interface{}){

	this.OrmSendFuncByType(DB_Dialer_Type, expr, retCh)
}


func (this *AdminServer) TcpSendFileFunc(msg interface{},fileName string, data []byte) (interface{}, error){

	return this.TcpSendFileFuncByType(DB_Dialer_Type,
		msg,
		fileName,
		data)

}




func (this *AdminServer) Run(blockMode bool) error{

	return this.RunBaseServer(blockMode)
}

























