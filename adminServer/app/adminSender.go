package app

import "MarsXserver/tcpserver"

func (this *AdminServer) WriteObjRequestMessageDBSync(body interface{}) (interface{}, error){

	return this.TServer.WriteObjRequestMessageToServerByTypeSync(DB_Dialer_Type, body)

}

func (this *AdminServer) WriteObjRequestMessageCrawlerSync(body interface{}) (interface{}, error){

	return this.TServer.WriteObjRequestMessageToServerByTypeSync(Crawler_Dialer_Type, body)

}


func (this *AdminServer) WriteObjRequestMessageToCrawlerCallback(body interface{}, cb tcpserver.ObjRequestCallbackFunc){

	this.TServer.WriteObjRequestMessageToServerByTypeCallback(Crawler_Dialer_Type, body, cb)

}

func (this *AdminServer) WriteObjRequestMessageSuggestSync(body interface{}) (interface{}, error){

	return this.TServer.WriteObjRequestMessageToServerByTypeSync(Suggest_Dialer_Type, body)

}

func (this *AdminServer) WriteObjRequestMessageDoopSync(body interface{}) (interface{}, error){

	return this.TServer.WriteObjRequestMessageToServerByTypeSync(Doop_Dialer_Type, body)

}





























