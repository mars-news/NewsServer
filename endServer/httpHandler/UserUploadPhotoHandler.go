package httpHandler

import (
	"MarsXserver/httpserver"
	"MarsXserver/common"
	"MarsXserver/endServer/app"
	"MarsXserver/db"
	"strconv"
	"MarsXserver/objMessages"
	"net/http"
	"MarsXserver/proj"
)


type UserUploadPhotoController struct{
	httpserver.BaseHttpHandler
}

type UserPhotoUploadResponse struct {
	Stat int
	UserName string
}


func (this *UserUploadPhotoController) Get(ctx *httpserver.HttpContext){

	if proj.IsDebug{
		this.Post(ctx)
	}else{
		http.Error(ctx.Rsp, "Get Method Not Allowed", 405)
	}

}



func (this *UserUploadPhotoController) Post(ctx *httpserver.HttpContext){

	common.InfoLog("user photo upload handler")

	var errAll, err error
	var server *app.EndServer
	var uid int32
	var ok bool
	var oldInfo *db.UserInfo

	var fileName string
	var fileData []byte


	server = ctx.Server.GetParentContext().(*app.EndServer)

	uid, ok = ctx.Session.Get(common.SessionKey_UID).(int32)
	if ok == false{
		ctx.Output.WriteJsonError(common.ErrorLog("get session uid failed:"))
		return
	}


	fileName, fileData, err = this.GetFormFileBytes("photo")

	if err != nil{

		if _, ok := err.(*common.DefaultEmptyError); ok != true{
			errAll = common.ErrorLog("get update photo failed", uid, err)
			goto OutPut
		}
	}


	if fileData != nil && len(fileData) > 0{

		rspInf, err := server.TcpSendFileFunc(
			&objMessages.UserPhotoImageRequest{
				Uid: int(uid),
			},
			fileName,
			fileData,
		)
		if err != nil{

			common.ErrorLog("write json error:", err)
			ctx.Output.WriteJsonError(err)
			return
		}

		rsp, ok := rspInf.(*objMessages.UserPhotoImageResponse)
		if ok == false{
			common.ErrorLog("not a user image response:", err)
			ctx.Output.WriteJsonError(err)
			return
		}

		common.InfoLog("user " + strconv.Itoa(int(uid)) + " photo image path:", rsp.Path)
	}

	oldInfo = &db.UserInfo{Id: uid}

	if err = server.Orm.Read(oldInfo, false); err != nil{
		errAll = common.ErrorLog("read old user info failed")
		goto OutPut
	}

	if err = oldInfo.SetHasPhoto(); err != nil{
		errAll = common.ErrorLog("set has photo failed uid:", uid)
		goto OutPut
	}

	if err = server.Orm.Update(oldInfo); err != nil{
		errAll = common.ErrorLog("orm update user name failed", uid)
		goto OutPut
	}


	common.InfoLog("user:", uid, " has change photo")
OutPut:

	if errAll != nil{
		ctx.Output.WriteJsonError(common.ErrorLog("upload user photo failed, ", errAll))
	}else{
		//ctx.Output.WriteJson(struct {}{})
		ctx.Output.WriteJson(&UserPhotoUploadResponse{
			Stat: 0,
			UserName: oldInfo.Name,
		})
	}

}





















