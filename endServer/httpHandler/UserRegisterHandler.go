package httpHandler

import (
	"MarsXserver/httpserver"
	"MarsXserver/common"
	"MarsXserver/endServer/app"
	"MarsXserver/db"
	"time"
	"strings"
	"strconv"
	"bytes"
	"fmt"
	"MarsXserver/objMessages"
	"net/http"
	"MarsXserver/proj"
)


type UserRegisterController struct{
	httpserver.BaseHttpHandler
}

type RegisterUserResponse struct {

	Id int32
	Name string
	Token string

}


func (this *UserRegisterController) NeedAuth() bool{

	return false

}


func (this *UserRegisterController) Get(ctx *httpserver.HttpContext){

	if proj.IsDebug{
		this.Post(ctx)
	}else{
		http.Error(ctx.Rsp, "Get Method Not Allowed", 405)
	}

}


func (this *UserRegisterController) Post(ctx *httpserver.HttpContext){

	common.InfoLog("get reg user handler")

	newUser := new(db.UserInfo)

	var errAll error
	var server *app.EndServer
	var uid int32

	var interestStr string
	var newName string
	var token string
	var fileName string
	var fileData []byte

	formVals, err := this.ParseForm(newUser)

	if err != nil{
		errAll = common.ErrorLog("get reg user form failed", err)
		goto OutPut
	}

	newUser.LastLogin = time.Now().UTC()

	server = ctx.Server.GetParentContext().(*app.EndServer)

	token = common.GenerateRandString(common.Default_User_Token_Length)
	newUser.Pwd = token

	uid, err = server.Orm.Save(newUser)
	if err != nil{
		errAll = common.ErrorLog("orm save new user failed, id:", uid)
		goto OutPut
	}

	newUser.Id = uid

	newName = "Guest" + strconv.Itoa(int(uid))

	newUser.Name = db.GetCombinedNameFromExtInfo(db.NewUserNameExtInfo(newName))

	if err = server.Orm.Update(newUser); err != nil{
		errAll = common.ErrorLog("orm update user name failed", uid)
		goto OutPut
	}

	fileName, fileData, err = this.GetFormFileBytes("photo")

	if err != nil{

		if _, ok := err.(*common.DefaultEmptyError); ok != true{
			errAll = common.ErrorLog("get update photo failed", uid, err)
			goto OutPut
		}
	}


	if fileData != nil && len(fileData) > 0{

		rspInf, err := server.TcpSendFileFunc(
			&objMessages.UserPhotoImageRequest{
				Uid: int(uid),
			},
			fileName,
			fileData,
		)
		if err != nil{

			common.ErrorLog("write json error:", err)
			ctx.Output.WriteJsonError(err)
			return
		}

		rsp, ok := rspInf.(*objMessages.UserPhotoImageResponse)
		if ok == false{
			common.ErrorLog("not a user image response:", err)
			ctx.Output.WriteJsonError(err)
			return
		}

		common.InfoLog("user " + strconv.Itoa(int(uid)) + " photo image path:", rsp.Path)
	}


	interestStr = formVals.Get("interests")

	if err = this.SaveUserInterest(int(uid), interestStr, server); err != nil{
		errAll = err
		goto OutPut
	}

	if err = this.CreateUserSimilarityRecord(int(uid), server); err != nil{
		errAll = err
		goto OutPut
	}

	if err = this.CreateUserBlock(int(uid), server); err != nil{
		errAll = err
		goto OutPut
	}

OutPut:

	if errAll != nil{
		ctx.Output.WriteJsonError(common.ErrorLog("register user failed, ", errAll))
	}else{

		common.InfoLog("new user created", newUser.Name)
		//ctx.Output.WriteJson(struct {}{})
		ctx.Output.WriteJson(&RegisterUserResponse{
			Id: uid,
			Token: newUser.Pwd,
			Name: newUser.Name,
		})
	}

}



func (this *UserRegisterController) SaveUserInterest(uid int, interest string, server *app.EndServer) error{

	newInterest := new(db.UserInterest)

	newInterest.Id = int32(uid)

	buff := new(bytes.Buffer)

	inters := strings.Split(interest, ",")

	if len(interest) <= 0 || len(inters) <= 0{

		avg := 100/db.CAT_COUNT
		for ii:= 0; ii < db.CAT_COUNT; ii++ {
			buff.WriteString(fmt.Sprintf("%d,",avg))
		}
	}else{
		hotAvg := 100.0/float32(len(inters))

		coldAvg := 1.0 * hotAvg * 0.2

		tot := 1.0 * hotAvg * float32(len(inters)) + float32(db.CAT_COUNT - len(inters)) * coldAvg

		scale := 100.0/tot

		hotAvg = hotAvg *scale
		coldAvg = coldAvg * scale

		if coldAvg < common.Epsilon{
			coldAvg = 1
		}

		interArr := make([]int, db.CAT_COUNT)

		for ii:= 0; ii < db.CAT_COUNT; ii++{
			interArr[ii] = int(coldAvg)
		}

		for _, inter := range inters{

			interD, err := strconv.Atoi(inter)
			if err != nil{
				return err
			}

			if interD >= db.CAT_COUNT || interD < 0{
				return common.ErrorLog("client interest cat error", interD)
			}

			interArr[interD] = int(hotAvg)
		}

		for ii:= 0; ii < db.CAT_COUNT; ii++{

			buff.WriteString(fmt.Sprintf("%d,", interArr[ii]))
		}

	}

	interRes := buff.String()

	interRes = interRes[0: len(interRes) -1]


	newInterest.Interests = interRes

	common.InfoLog("new user" , uid , " intrs:" , interRes)

	_, err := server.Orm.Save(newInterest)
	if err !=  nil{
		return err
	}

	return nil

}



func (this *UserRegisterController) CreateUserSimilarityRecord(uid int, server *app.EndServer) error{

	newSim := new(db.UserSimilarity)
	newSim.Id = int32(uid)
	newSim.Similarities = ""

	if _, err := server.Orm.Save(newSim); err != nil{

		return err

	}

	return nil
}



func (this *UserRegisterController) CreateUserBlock(uid int, server *app.EndServer) error{

	userBlock := &db.UserBlock{
		Id: int32(uid),
		IsForbidComment: 0,
		ForbidCommentTime: time.Now(),
		ReportedCount: 0,
	}

	if _, err := server.Orm.Save(userBlock); err != nil{

		return err

	}

	return nil
}


















