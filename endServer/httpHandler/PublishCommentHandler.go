package httpHandler

import (
	"MarsXserver/httpserver"
	"MarsXserver/common"
	"strconv"
	"MarsXserver/endServer/app"
	"MarsXserver/objMessages"
	"net/http"
	"MarsXserver/db"
	"time"
	"MarsXserver/proj"
)


type PublishCommentController struct{
	httpserver.BaseHttpHandler
}

type PublishCommentResponse struct {

	Stat	int
	Pid int

}


func (this *PublishCommentController) Get(ctx *httpserver.HttpContext){

	if proj.IsDebug{
		this.Post(ctx)
	}else{
		http.Error(ctx.Rsp, "Get Method Not Allowed", 405)
	}

}


func (this *PublishCommentController) Post(ctx *httpserver.HttpContext){

	common.InfoLog("publish comment send handler")

	var errAll error
	var aServer *app.EndServer
	var nid int
	var uid int32
	var nidStr, replyStr string
	var comment string
	var reply int
	var isReply int32
	var ok bool

	var ret interface{}
	var newComment *objMessages.SSPublishContent
	var testUserBlock *db.UserBlock

	now := time.Now().UTC()

	form, err := this.GetForm()
	if err != nil{
		errAll = common.ErrorLog("parse news refresh form failed err:", err)
		goto output
	}

	uid, ok = ctx.Session.Get(common.SessionKey_UID).(int32)
	if ok == false{
		ctx.Output.WriteJsonError(common.ErrorLog("get session uid failed:"))
		return
	}

	aServer = ctx.Server.GetParentContext().(*app.EndServer)

	testUserBlock = &db.UserBlock{Id: uid}

	if err = aServer.Orm.Read(testUserBlock, true); err != nil{
		errAll = common.ErrorLog("read user block failed")
		goto output
	}

	if testUserBlock.IsForbidComment > 0{

		if now.Sub(testUserBlock.ForbidCommentTime) > 0{

			testUserBlock.IsForbidComment = 0
			testUserBlock.ReportedCount = 0

			if err = aServer.Orm.Update(testUserBlock); err != nil{
				errAll = common.ErrorLog("save user block failed")
				goto output
			}
			common.InfoLog("user is unblocked", uid, now)

		}else {
			common.InfoLog("user is in block time", uid, testUserBlock.ForbidCommentTime)

			ctx.Output.WriteJson(&PublishCommentResponse{
				Pid: 0,
				Stat: int(common.ErrorCode_User_Blocked),
			})
			return

		}
	}

	nidStr = form.Get("nid")
	nid, err = strconv.Atoi(nidStr)
	if err != nil{
		errAll = common.ErrorLog("parse nid failed", nidStr)
		goto output
	}

	comment = form.Get("comment")
	if len(comment) <= 0{
		errAll = common.ErrorLog("comment is empty")
		goto output
	}


	replyStr = form.Get("reply")
	if len(replyStr) <= 0{
		reply = 0
		isReply = 0
	}else{
		reply, err = strconv.Atoi(replyStr)
		if err != nil{
			errAll = common.ErrorLog("parse reply failed", reply)
			goto output
		}
		isReply = 1
	}


	newComment = &objMessages.SSPublishContent{
		Nid: nid,
		Uid: int32(uid),
		Reply:reply,
		Content: comment,
		IsReply: isReply,
	}

	ret, err = aServer.WriteObjRequestMessageToRedSync(
		&objMessages.SSPublishCommentRequest{
			Content: newComment,
		})

	errAll = err

output:

	if errAll != nil {
		ctx.Output.WriteJsonError(common.ErrorLog("public comment failed, ", err))
	}else{
		rsp, ok := ret.(*objMessages.SSPublishCommentResponse)
		if ok != true{
			err := common.ErrorLog("suggest rsp type failed")
			ctx.Output.WriteJsonError(err)
			return
		}


		if rsp.Stat == int(common.ErrorCode_CommentCnt_Overflow){

			common.InfoLog("suggest end")
			ctx.Output.WriteJson(&PublishCommentResponse{Stat: rsp.Stat})
			return

		}


		ctx.Output.WriteJson(&PublishCommentResponse{
			Pid: rsp.Pid,
			Stat: rsp.Stat,
		})

	}

}
























