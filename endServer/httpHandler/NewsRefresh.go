package httpHandler

import (
	"MarsXserver/httpserver"
	"MarsXserver/common"
	"strconv"
	"MarsXserver/endServer/app"
	"MarsXserver/objMessages"
	"MarsXserver/db"
	"bytes"
	"fmt"
	"net/http"
	"MarsXserver/proj"
)


type NewsRefreshController struct{
	httpserver.BaseHttpHandler

}

type NewsRefreshResponse struct {
	Stat int
	News []*db.NewsItem
	Srcs []*db.CrawlSource
}


func (this *NewsRefreshController) Get(ctx *httpserver.HttpContext){

	if proj.IsDebug{
		this.Post(ctx)
	}else{
		http.Error(ctx.Rsp, "Get Method Not Allowed", 405)
	}

}



func (this *NewsRefreshController) Post(ctx *httpserver.HttpContext){

	common.InfoLog("post handler:", ctx)

	var ret interface{}
	var err error

	form, err := this.GetForm()
	if err != nil{
		ctx.Output.WriteJsonError(common.ErrorLog("parse news refresh form failed err:", err))
		return
	}

	uid, ok := ctx.Session.Get(common.SessionKey_UID).(int32)
	if ok == false{
		ctx.Output.WriteJsonError(common.ErrorLog("get session uid failed:"))
		return
	}

	aServer := ctx.Server.GetParentContext().(*app.EndServer)

	var catid int
	catidStr := form.Get("cat")
	if len(catidStr) <= 0{

		ret, err = aServer.WriteObjRequestMessageSuggestSync(
			&objMessages.SuggestGetSuggestRequest{
				Uid: int(uid),
				IsCommon: true,
			})

	}else{
		catid, err = strconv.Atoi(catidStr)
		if err != nil{
			ctx.Output.WriteJsonError(common.ErrorLog("parse cat failed", catidStr))
			return
		}

		if catid <0 || catid >= db.CAT_COUNT{

			ctx.Output.WriteJsonError(common.ErrorLog("cat id exceeds limits", catid))
			return
		}

		ret, err = aServer.WriteObjRequestMessageSuggestSync(
			&objMessages.SuggestGetSuggestRequest{
				Uid: int(uid),
				Cat: catid,
			})

	}


	if err != nil {
		ctx.Output.WriteJsonError(common.ErrorLog("refresh news failed, ", err))
	}else{
		rsp, ok := ret.(*objMessages.SuggestGetSuggestResponse)
		if ok != true{
			err := common.ErrorLog("suggest rsp type failed")
			ctx.Output.WriteJsonError(err)
			return
		}


		if rsp.Stat == int(common.ErrorCode_Suggest_End){

			common.InfoLog("suggest end")
			ctx.Output.WriteJson(&NewsRefreshResponse{Stat:int(common.ErrorCode_Suggest_End)})
			return

		}else if rsp.Stat == int(common.ErrorCode_Suggest_Failed){
			common.InfoLog("suggest failed")
			ctx.Output.WriteJson(&NewsRefreshResponse{Stat:int(common.ErrorCode_Suggest_Failed)})
			return
		}

		this.rspHandle(int(uid), ctx, aServer, rsp)
	}




}





func (this *NewsRefreshController) rspHandle(uid int, ctx *httpserver.HttpContext, server *app.EndServer, rsp *objMessages.SuggestGetSuggestResponse){

	pids := rsp.Pids

	sourceIdSet := common.NewMSet()

	srcs := make([]*db.CrawlSource, 0)
	items := make([]*db.NewsItem, 0)

	var ii int
	var err, forErr error
	var ok bool
	var commentCntsRsp *objMessages.SSGetCommentCountResponse
	var infoBuff bytes.Buffer

	newsIds := make([]int, 0, len(pids))
	for _, pid := range pids{
		item := &db.CrawlItem{
			Id:	int32(pid),
		}

		if err := server.Orm.Read(item, true); err != nil{
			forErr = common.ErrorLog("orm read crawl item failed, id:", pid)
			break
		}

		if !sourceIdSet.Contains(item.SourceId){
			src := &db.CrawlSource{
				Id:	int32(item.SourceId),
			}

			if err := server.Orm.Read(src, true); err != nil{
				forErr = common.ErrorLog("orm read src failed, id:", src.Id)
				break
			}
			srcs = append(srcs, src)
			sourceIdSet.Insert(item.SourceId)
		}

		items = append(items, &db.NewsItem{CrawlItem:*item})
		newsIds = append(newsIds, int(item.Id))
	}

	commentCntsReq := &objMessages.SSGetCommentCountRequest{
		newsIds,
	}

	ret, err := server.WriteObjRequestMessageToRedSync(
		commentCntsReq,
	)

	if err != nil{
		forErr = err
		goto output
	}

	commentCntsRsp, ok = ret.(*objMessages.SSGetCommentCountResponse)
	if ok != true{
		forErr = common.ErrorLog("news cnt rsp type failed")
		goto output
	}

	if len(commentCntsRsp.Counts) != len(items){
		forErr = common.ErrorLog("count length is not equal to news")
		goto output
	}

	infoBuff.WriteString(fmt.Sprintf("refresh for uid:%d, news:", uid))
	for ii = range items{
		items[ii].CommentCnt = commentCntsRsp.Counts[ii]
		infoBuff.WriteString(fmt.Sprintf("%d,", items[ii].Id))
	}

	common.InfoLog(infoBuff.String())

output:

	if forErr != nil{
		ctx.Output.WriteJsonError(common.ErrorLog("news cnt rsp failed, ", forErr))
	}else{
		//ctx.Output.WriteJson(struct {}{})
		ctx.Output.WriteJson(&NewsRefreshResponse{
			News: items,
			Srcs: srcs,
		})
	}


}





























