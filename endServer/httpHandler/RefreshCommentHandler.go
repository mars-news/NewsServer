package httpHandler

import (
	"MarsXserver/httpserver"
	"MarsXserver/common"
	"strconv"
	"MarsXserver/endServer/app"
	"MarsXserver/objMessages"
	"MarsXserver/db"
	"net/http"
	"MarsXserver/proj"
)


type RefreshCommentController struct{
	httpserver.BaseHttpHandler
}






type CSRefreshCommentResponse struct {

	Stat	int

	Comments []*objMessages.CSComment

}


func (this *RefreshCommentController) Get(ctx *httpserver.HttpContext){

	if proj.IsDebug{
		this.Post(ctx)
	}else{
		http.Error(ctx.Rsp, "Get Method Not Allowed", 405)
	}

}


func (this *RefreshCommentController) Post(ctx *httpserver.HttpContext){

	common.InfoLog("refresh comment send handler")

	var errAll error
	var aServer *app.EndServer
	var nid int
	var offset int
	var nidStr, offsetStr string
	var ok bool

	var ret interface{}
	var refreshCommentsReq *objMessages.SSRefreshCommentRequest
	var refreshCommentsRsp *objMessages.SSRefreshCommentResponse
	var rsp *CSRefreshCommentResponse


	form, err := this.GetForm()
	if err != nil{
		errAll = common.ErrorLog("parse news refresh form failed err:", err)
		goto output
	}

	nidStr = form.Get("nid")
	nid, err = strconv.Atoi(nidStr)
	if err != nil{
		errAll = common.ErrorLog("parse nid failed", nidStr)
		goto output
	}

	offsetStr = form.Get("offset")
	offset, err = strconv.Atoi(offsetStr)
	if err != nil{
		errAll = common.ErrorLog("name is empty")
		goto output
	}


	refreshCommentsReq = &objMessages.SSRefreshCommentRequest{
		Nid: nid,
		Offset: offset,
	}

	aServer = ctx.Server.GetParentContext().(*app.EndServer)

	ret, err = aServer.WriteObjRequestMessageToRedSync(
		refreshCommentsReq,
	)

	if err != nil{
		errAll = err
		goto output
	}

	refreshCommentsRsp, ok = ret.(*objMessages.SSRefreshCommentResponse)
	if ok != true{
		errAll = common.ErrorLog("suggest rsp type failed")
		goto output
	}


	rsp = &CSRefreshCommentResponse{
		Comments: make([]*objMessages.CSComment, 0, len(refreshCommentsRsp.Comments)),
	}

	if refreshCommentsRsp.Stat < 0 || refreshCommentsRsp.Stat == int(common.ErrorCode_Used_Up){
		rsp.Stat = int(common.ErrorCode_Comment_Refresh_End)
		goto output
	}


	for _, comment := range refreshCommentsRsp.Comments{

		user := &db.UserInfo{
			Id: comment.Uid,
		}

		err = aServer.Orm.Read(user, true)
		if err != nil{
			continue
		}

		rsp.Comments = append(rsp.Comments, &objMessages.CSComment{
			SSComment: *comment,
			Name: user.Name,
		})
	}




output:

	if errAll != nil {
		ctx.Output.WriteJsonError(common.ErrorLog("refresh news failed, ", err))
	}else{

		ctx.Output.WriteJson(rsp)

	}

}
























