package httpHandler

import (
	"MarsXserver/httpserver"
	"MarsXserver/common"
	"MarsXserver/endServer/app"
	"MarsXserver/db"
	"net/http"
	"MarsXserver/proj"
)


type UserInfoUpdateController struct{
	httpserver.BaseHttpHandler
}


type CSUserInfoUpdateControllerResponse struct {

	Stat	int
	NewName string

}

func (this *UserInfoUpdateController) Get(ctx *httpserver.HttpContext){

	if proj.IsDebug{
		this.Post(ctx)
	}else{
		http.Error(ctx.Rsp, "Get Method Not Allowed", 405)
	}

}



func (this *UserInfoUpdateController) Post(ctx *httpserver.HttpContext){

	common.InfoLog("user info update handler")

	var err, errAll error
	var aServer *app.EndServer
	var uid int32
	var newInfo, oldInfo *db.UserInfo

	var ok bool

	var rsp *CSUserInfoUpdateControllerResponse


	aServer = ctx.Server.GetParentContext().(*app.EndServer)

	newInfo = new(db.UserInfo)

	uid, ok = ctx.Session.Get(common.SessionKey_UID).(int32)
	if ok == false{
		ctx.Output.WriteJsonError(common.ErrorLog("get session uid failed:"))
		return
	}

	oldInfo = &db.UserInfo{Id: uid}

	if err = aServer.Orm.Read(oldInfo, false); err != nil{
		errAll = common.ErrorLog("read old user info failed")
		goto output
	}


	_, err = this.ParseForm(newInfo)

	if err != nil{
		errAll = common.ErrorLog("get get new info form failed", err)
		goto output
	}

	if err = oldInfo.ChangeName(newInfo.Name); err != nil{
		errAll = err
		goto output
	}

	if err = aServer.Orm.Update(oldInfo); err != nil{
		errAll = common.ErrorLog("orm update user name failed", uid)
		goto output
	}

	common.InfoLog("user update rsp ", uid)

	rsp = &CSUserInfoUpdateControllerResponse{
		Stat: 0,
		NewName: oldInfo.Name,
	}

output:

	if errAll != nil {
		ctx.Output.WriteJsonError(common.ErrorLog("update user info failed, ", err))
	}else{

		ctx.Output.WriteJson(rsp)

	}

}
























