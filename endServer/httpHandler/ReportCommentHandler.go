package httpHandler

import (
	"MarsXserver/httpserver"
	"MarsXserver/common"
	"MarsXserver/endServer/app"
	"MarsXserver/objMessages"
	"MarsXserver/db"
	"time"
	"net/http"
	"MarsXserver/proj"
)


type ReportCommentController struct{
	httpserver.BaseHttpHandler
}



type CSReportCommentResponse struct {
	Stat	int
}


func (this *ReportCommentController) Get(ctx *httpserver.HttpContext){

	if proj.IsDebug{
		this.Post(ctx)
	}else{
		http.Error(ctx.Rsp, "Get Method Not Allowed", 405)
	}

}


func (this *ReportCommentController) Post(ctx *httpserver.HttpContext){

	common.InfoLog("object comment send handler")

	var errAll error
	var aServer *app.EndServer
	var uid int32
	var ok bool

	var ret interface{}
	var ssReq *objMessages.SSReportCommentRequest
	var ssRsp *objMessages.SSReportCommentResponse
	var rsp *CSReportCommentResponse
	var testUserBlock *db.UserBlock

	ssReq = new(objMessages.SSReportCommentRequest)
	_, err := this.ParseForm(ssReq)
	if err != nil{
		ctx.Output.WriteJsonError(common.ErrorLog("parse report req failed err:", err))
		return
	}

	uid, ok = ctx.Session.Get(common.SessionKey_UID).(int32)
	if ok == false{
		ctx.Output.WriteJsonError(common.ErrorLog("get session uid failed:"))
		return
	}

	aServer = ctx.Server.GetParentContext().(*app.EndServer)

	ret, err = aServer.WriteObjRequestMessageToRedSync(
		ssReq,
	)

	if err != nil{
		common.InfoLog("write to red to report comment err", err)
	}

	ssRsp, ok = ret.(*objMessages.SSReportCommentResponse)
	if ok != true{
		common.ErrorLog("report comment rsp type failed")
		goto output
	}

	testUserBlock = &db.UserBlock{Id: ssRsp.Uid}

	if err = aServer.Orm.Read(testUserBlock, true); err != nil{
		errAll = common.ErrorLog("read user block failed")
		goto output
	}


	if testUserBlock.IsForbidComment > 0{
		common.InfoLog("user is already forbid", uid)
		goto output
	}

	if testUserBlock.ReportedCount + 1 >= db.UserForbid_Count{

		testUserBlock.IsForbidComment = 1
		testUserBlock.ReportedCount += 1
		testUserBlock.ForbidCommentTime = time.Now().UTC().Add(db.UserForbid_Duration)

		common.InfoLog("user is blocked", uid, "until", testUserBlock.ForbidCommentTime)
	}else{
		testUserBlock.ReportedCount += 1
	}

	if err = aServer.Orm.Update(testUserBlock); err != nil{
		errAll = common.ErrorLog("save user block failed")
		goto output
	}

	rsp = &CSReportCommentResponse{
		Stat: ssRsp.Stat,
	}

output:

	if errAll != nil {
		ctx.Output.WriteJsonError(common.ErrorLog("report comment failed, ", err))
	}else{

		ctx.Output.WriteJson(rsp)

	}

}
























