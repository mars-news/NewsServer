package httpHandler

import (
	"MarsXserver/httpserver"
	"MarsXserver/common"
	"strconv"
	"MarsXserver/endServer/app"
	"MarsXserver/objMessages"
	"encoding/json"
	"net/http"
	"MarsXserver/proj"
)


type VoteController struct{
	httpserver.BaseHttpHandler
}


type CSVoteResponse struct {

	Stat	int

}


func (this *VoteController) Get(ctx *httpserver.HttpContext){

	if proj.IsDebug{
		this.Post(ctx)
	}else{
		http.Error(ctx.Rsp, "Get Method Not Allowed", 405)
	}

}



func (this *VoteController) Post(ctx *httpserver.HttpContext){

	common.InfoLog("vote comment send handler")

	var errAll error
	var aServer *app.EndServer
	var uid int32
	var nid, score int
	var nidStr, votesStr, scoreStr string
	var votes []objMessages.SSVoteItem
	var ok bool

	var ret interface{}
	var voteReq *objMessages.SSVoteRequest
	var voteRsp *objMessages.SSVoteResponse
	var rsp *CSVoteResponse


	form, err := this.GetForm()
	if err != nil{
		errAll = common.ErrorLog("parse news refresh form failed err:", err)
		goto output
	}

	uid, ok = ctx.Session.Get(common.SessionKey_UID).(int32)
	if ok == false{
		ctx.Output.WriteJsonError(common.ErrorLog("get session uid failed:"))
		return
	}


	nidStr = form.Get("nid")
	nid, err = strconv.Atoi(nidStr)
	if err != nil{
		errAll = common.ErrorLog("parse nid failed", nidStr)
		goto output
	}

	scoreStr = form.Get("score")
	score, err = strconv.Atoi(scoreStr)
	if err != nil{
		errAll = common.ErrorLog("parse score failed", scoreStr)
		goto output
	}


	votesStr = form.Get("votes")
	if len(votesStr) <= 0{
		errAll = common.ErrorLog(" no votes")
		goto output
	}


	votes = make([]objMessages.SSVoteItem, 0)
	err = json.Unmarshal([]byte(votesStr), &votes)
	if err != nil{
		errAll = common.ErrorLog("parse votes failed", err)
		goto output
	}


	voteReq = &objMessages.SSVoteRequest{
		Uid: int(uid),
		Nid: nid,
		Score: score,
		Votes: votes,
	}


	aServer = ctx.Server.GetParentContext().(*app.EndServer)

	ret, err = aServer.WriteObjRequestMessageToRedSync(
		voteReq,
	)

	if err != nil{
		errAll = err
		goto output
	}

	voteRsp, ok = ret.(*objMessages.SSVoteResponse)
	if ok != true{
		errAll = common.ErrorLog("suggest rsp type failed")
		goto output
	}

	common.InfoLog("vote rsp ", voteRsp.Stat)

	rsp = &CSVoteResponse{
		Stat: voteRsp.Stat,
	}

output:

	if errAll != nil {
		ctx.Output.WriteJsonError(common.ErrorLog("vote news failed, ", err))
	}else{

		ctx.Output.WriteJson(rsp)

	}

}
























