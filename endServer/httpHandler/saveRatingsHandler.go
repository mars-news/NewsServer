package httpHandler

import (
	"MarsXserver/httpserver"
	"MarsXserver/common"
	"MarsXserver/endServer/app"
	"MarsXserver/objMessages"
	"encoding/json"
	"strconv"
	"net/http"
	"MarsXserver/proj"
)


type SaveRatingsController struct{
	httpserver.BaseHttpHandler
}


type CSSaveRatingsResponse struct {
	Stat	int
}


func (this *SaveRatingsController) Get(ctx *httpserver.HttpContext){

	if proj.IsDebug{
		this.Post(ctx)
	}else{
		http.Error(ctx.Rsp, "Get Method Not Allowed", 405)
	}

}


func (this *SaveRatingsController) Post(ctx *httpserver.HttpContext){

	common.InfoLog("save ratings handler")

	var errAll error
	var aServer *app.EndServer
	var uid int32
	var ratingStr, beforeStr string
	var before int
	var ratings []objMessages.SSRatingItem
	var ok bool

	var ret interface{}
	var voteReq *objMessages.SSSaveRatingRequst
	var ratingsRsp *objMessages.SSSaveRatingResponse
	var rsp *CSSaveRatingsResponse


	form, err := this.GetForm()
	if err != nil{
		errAll = common.ErrorLog("parse news refresh form failed err:", err)
		goto output
	}

	uid, ok = this.GetSessinon(common.SessionKey_UID).(int32)
	if ok != true{
		errAll = common.ErrorLog("get  uid from session failed")
		goto output
	}

	ratingStr = form.Get("rating")
	if len(ratingStr) <= 0{
		errAll = common.ErrorLog(" no ratings")
		goto output
	}

	if proj.IsDebug{
		beforeStr = form.Get("before")
		if len(beforeStr) > 0 {
			before, err = strconv.Atoi(beforeStr)
			if err != nil {
				errAll = err
				goto output
			}
		}
	}


	ratings = make([]objMessages.SSRatingItem, 0)
	err = json.Unmarshal([]byte(ratingStr), &ratings)
	if err != nil{
		errAll = common.ErrorLog("parse ratings failed")
		goto output
	}


	voteReq = &objMessages.SSSaveRatingRequst{
		Uid:   int(uid),
		Ratings:ratings,
		Before: before,
	}


	aServer = ctx.Server.GetParentContext().(*app.EndServer)


	ret, err = aServer.WriteObjRequestMessageToRedSync(
		voteReq,
	)

	if err != nil{
		errAll = err
		goto output
	}

	ratingsRsp, ok = ret.(*objMessages.SSSaveRatingResponse)
	if ok != true{
		errAll = common.ErrorLog("suggest rsp type failed")
		goto output
	}

	common.DebugLogPlus("save ratings for uid", uid, "before", before, "ratings:", ratings)

	rsp = &CSSaveRatingsResponse{
		Stat: ratingsRsp.Stat,
	}



output:

	if errAll != nil {
		ctx.Output.WriteJsonError(common.ErrorLog("vote news failed, ", err))
	}else{
		ctx.Output.WriteJson(rsp)
	}

}












































