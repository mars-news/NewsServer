package httpHandler

import (
	"MarsXserver/httpserver"
	"MarsXserver/common"
	"strconv"
	"MarsXserver/endServer/app"
	"MarsXserver/objMessages"
	"MarsXserver/db"
	"fmt"
	"net/http"
	"MarsXserver/proj"
)


type RefreshReplyController struct{
	httpserver.BaseHttpHandler
}






type CSRefreshReplyResponse struct {

	Stat	int

	Replies []*objMessages.CSReply

}


func (this *RefreshReplyController) Get(ctx *httpserver.HttpContext){

	if proj.IsDebug{
		this.Post(ctx)
	}else{
		http.Error(ctx.Rsp, "Get Method Not Allowed", 405)
	}

}


func (this *RefreshReplyController) Post(ctx *httpserver.HttpContext){

	common.InfoLog("get reply send handler")

	var errAll error
	var aServer *app.EndServer
	var nid, pid int
	var offset int
	var nidStr, pidStr, offsetStr string
	var ok bool

	var ret interface{}
	var refreshRepliesReq *objMessages.SSRefreshReplyRequest
	var refreshRepliesRsp *objMessages.SSRefreshReplyResponse
	var rsp *CSRefreshReplyResponse


	form, err := this.GetForm()
	if err != nil{
		errAll = common.ErrorLog("parse news refresh form failed err:", err)
		goto output
	}

	nidStr = form.Get("nid")
	nid, err = strconv.Atoi(nidStr)
	if err != nil{
		errAll = common.ErrorLog("parse nid failed", nidStr)
		goto output
	}

	pidStr = form.Get("pid")
	pid, err = strconv.Atoi(pidStr)
	if err != nil{
		errAll = common.ErrorLog("parse pid failed", pidStr)
		goto output
	}



	offsetStr = form.Get("offset")
	offset, err = strconv.Atoi(offsetStr)
	if err != nil{
		errAll = common.ErrorLog("name is empty")
		goto output
	}


	refreshRepliesReq = &objMessages.SSRefreshReplyRequest{
		Nid: nid,
		Pid: pid,
		Offset: offset,
	}


	aServer = ctx.Server.GetParentContext().(*app.EndServer)

	ret, err = aServer.WriteObjRequestMessageToRedSync(
		refreshRepliesReq,
	)

	if err != nil{
		errAll = err
		goto output
	}

	refreshRepliesRsp, ok = ret.(*objMessages.SSRefreshReplyResponse)
	if ok != true{
		errAll = common.ErrorLog("suggest rsp type failed")
		goto output
	}


	rsp = &CSRefreshReplyResponse{

		Replies: make([]*objMessages.CSReply, 0, len(refreshRepliesRsp.Replies)),
	}

	if refreshRepliesRsp.Stat < 0{
		rsp.Stat = int(common.ErrorCode_Comment_Refresh_End)
		goto output
	}


	for _, reply := range refreshRepliesRsp.Replies{

		user := &db.UserInfo{
			Id: reply.Uid,
		}

		err = aServer.Orm.Read(user, true)
		if err != nil{
			continue
		}

		rsp.Replies = append(rsp.Replies, &objMessages.CSReply{
			SSReply: *reply,
			Name: user.Name,
		})

	}

	common.InfoLog(fmt.Sprintf("rsp replies count: %d" , len(rsp.Replies)), " uid:", nid)


output:

	if errAll != nil {
		ctx.Output.WriteJsonError(common.ErrorLog("refresh news failed, ", err))
	}else{

		ctx.Output.WriteJson(rsp)

	}

}
























