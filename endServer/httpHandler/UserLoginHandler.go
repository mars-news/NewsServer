package httpHandler

import (
	"MarsXserver/httpserver"
	"MarsXserver/common"
	"MarsXserver/db"
	"MarsXserver/endServer/app"
	"net/http"
	"MarsXserver/proj"
)


type UserLoginController struct{
	httpserver.BaseHttpHandler
}

type UserLoginResponse struct {

}

type UserLoginRequest struct {

	Uid int32
	Pwd int32
}


func (this *UserLoginController) NeedAuth() bool{

	return false

}


func (this *UserLoginController) Get(ctx *httpserver.HttpContext){

	if proj.IsDebug{
		this.Post(ctx)
	}else{
		http.Error(ctx.Rsp, "Get Method Not Allowed", 405)
	}

}


func (this *UserLoginController) Post(ctx *httpserver.HttpContext){

	common.InfoLog("user login handler")

	var server *app.EndServer
	var testUser *db.UserInfo
	var testUserBlock *db.UserBlock
	var errAll, err error

	userInfo := new(UserLoginRequest)

	_, err = this.ParseForm(userInfo)

	if err != nil{
		errAll = common.ErrorLog("get reg user form failed", err)
		goto OutPut
	}

	server = ctx.Server.GetParentContext().(*app.EndServer)

	testUser = &db.UserInfo{Id: userInfo.Uid}

	if err = server.Orm.Read(testUser, true); err != nil{
		errAll = common.ErrorLog("read user failed")
		goto OutPut
	}

	testUserBlock = &db.UserBlock{Id: userInfo.Uid}

	if err = server.Orm.Read(testUserBlock, false); err != nil{
		errAll = common.ErrorLog("read user block failed")
		goto OutPut
	}

	common.InfoLog("uid:", userInfo.Uid)

	this.SetSession(common.SessionKey_UID, userInfo.Uid)


OutPut:

	if errAll != nil{
		ctx.Output.WriteJsonError(common.ErrorLog("register user failed, ", errAll))
	}else{
		common.InfoLog("login successed:", userInfo.Uid)
		ctx.Output.WriteJson(&UserLoginResponse{
		})
	}

}



















