package app

import (
	"MarsXserver/orm"
	"MarsXserver/common"
	"MarsXserver/hybridServer"
	"MarsXserver/httpserver"
)


const (
	Dialer_Type_DB      string = "db"
	Dialer_Type_Suggest string = "suggest"
	Dialer_Type_Red	    string = "red"
)

var (
	All_Dialers = []string{Dialer_Type_DB, Dialer_Type_Suggest, Dialer_Type_Red}

)


type EndServer struct{
	hybridServer.HybridBaseServer

	Sid	int
	Sname string
	Config *EndServerConfigData

}



func NewEndServer(configName string) (*EndServer, error){

	config_, err := LoadConfig(configName)
	if err != nil{
		return nil, common.ErrorLog("load Config failed", err)
	}

	endServer := &EndServer{
		Sid: 	config_.Sid,
		Sname:  config_.Name,
		Config: config_,
	}


	endServer.InitBaseServer(config_.Name, endServer, &config_.TConfig, &config_.HConfig, nil, nil)

	endServer.Orm.RegisterSendFunc(endServer.OrmSendFunc)
	endServer.Orm.RegisterBytesRspFunc(orm.OrmHandleBytesRspFunc)

	endServer.TServer.SetLegalDialerNames(All_Dialers)

	endServer.HServer.RegisterAuthFunc(httpserver.Auth)

	return endServer, nil
}




func (this *EndServer) OrmSendFunc(expr *orm.XOrmEprData, retCh chan interface{}){

	this.OrmSendFuncByType(Dialer_Type_DB, expr, retCh)
}


func (this *EndServer) TcpSendFileFunc(msg interface{},fileName string, data []byte) (interface{}, error){

	return this.TcpSendFileFuncByType(Dialer_Type_DB,
		msg,
		fileName,
		data)

}




func (this *EndServer) Run(blockMode bool) error{

	return this.RunBaseServer(blockMode)
}

























