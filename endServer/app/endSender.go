package app

import "MarsXserver/tcpserver"

func (this *EndServer) WriteObjRequestMessageDBSync(body interface{}) (interface{}, error){

	return this.TServer.WriteObjRequestMessageToServerByTypeSync(Dialer_Type_DB, body)

}

func (this *EndServer) WriteObjRequestMessageSuggestSync(body interface{}) (interface{}, error){

	return this.TServer.WriteObjRequestMessageToServerByTypeSync(Dialer_Type_Suggest, body)

}


func (this *EndServer) WriteObjRequestMessageToSuggestCallback(body interface{}, cb tcpserver.ObjRequestCallbackFunc){

	this.TServer.WriteObjRequestMessageToServerByTypeCallback(Dialer_Type_Suggest, body, cb)

}

func (this *EndServer) WriteObjRequestMessageToRedSync(body interface{}) (interface{}, error){

	return this.TServer.WriteObjRequestMessageToServerByTypeSync(Dialer_Type_Red, body)

}