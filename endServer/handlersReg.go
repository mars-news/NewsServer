package endServer

import (
	"MarsXserver/endServer/app"
	"MarsXserver/endServer/httpHandler"

)


func initRegisterHttpHandlers(aServer *app.EndServer){

	HttpHandlers := make(map[string]interface{})

	HttpHandlers["/user/register"] = &httpHandler.UserRegisterController{}

	HttpHandlers["/user/login"] = &httpHandler.UserLoginController{}

	HttpHandlers["/user/photo/upload"] = &httpHandler.UserUploadPhotoController{}

	HttpHandlers["/user/info/update"] = &httpHandler.UserInfoUpdateController{}

	HttpHandlers["/news/refresh"] = &httpHandler.NewsRefreshController{}

	HttpHandlers["/news/rate"] = &httpHandler.SaveRatingsController{}

	HttpHandlers["/comment/publish"] = &httpHandler.PublishCommentController{}

	HttpHandlers["/comment/refresh"] = &httpHandler.RefreshCommentController{}

	HttpHandlers["/reply/refresh"] = &httpHandler.RefreshReplyController{}

	HttpHandlers["/vote"] = &httpHandler.VoteController{}

	HttpHandlers["/report"] = &httpHandler.ReportCommentController{}

	aServer.RegisterHttpHandlers(HttpHandlers)
}



func initRegisterFileHandlers(aServer *app.EndServer){



}

func InitRegisterHandlers(aServer *app.EndServer){

	initRegisterFileHandlers(aServer)
	initRegisterHttpHandlers(aServer)

}

