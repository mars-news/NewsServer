package messageHandler

import (
	"reflect"
	"MarsXserver/tcpserver"
	"MarsXserver/protos"
)



func init(){

	tcpserver.RegisterHandler(
		int(protos.MSGID_LoginServer_Message),
		&tcpserver.HandlerItem{
			MsgProtoType:  tcpserver.MessageProtoType_Proto,
			ReqMsgType:    reflect.TypeOf(protos.LoginServerMessage{}),
			Handler:       nil,
			MsgGetIdFunc:  LoginServerMsgGetMsgId,
			MsgGetSeqFunc: LoginServerMsgGetSeq,
			MsgSetSeqFunc: LoginServerMsgSetSeq,
	})

}
