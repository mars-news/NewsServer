package messageHandler

import (
	"MarsXserver/protos"
	"MarsXserver/protos/loginServerMessages"
	"fmt"
	"MarsXserver/tcpserver"
	"MarsXserver/common"
)

type CSLoginRequestHandler struct{

}

func init(){
	tcpserver.RegisterHandler(
		int(protos.MSGID_CSLogin_Request),
		&tcpserver.HandlerItem{
			ReqMsgType:   nil,
			Handler:      &CSLoginRequestHandler{},
			MsgGetIdFunc: LoginServerMsgGetMsgId,
		})

	tcpserver.RegisterHandler(
		int(protos.MSGID_CSLogin_Response),
		&tcpserver.HandlerItem{
			ReqMsgType:   nil,
			Handler:      &CSLoginResponseHandler{},
			MsgGetIdFunc: LoginServerMsgGetMsgId,
		})
}

func (handler *CSLoginRequestHandler) Handle(connector *tcpserver.Connector, header interface{}, message interface{}) error{

	//todo what if conn is null nor not writable
	//close conn error
	//write back error
	var ret int32

	msgInfo,err := LoginServerMsgUnpack(message, true)
	if err != nil{
		common.ErrorLog(fmt.Sprintf("unpack msg failed"))
		return err
	}

	req := msgInfo.req.GetLogin()
	if req == nil{
		common.ErrorLog(fmt.Sprintf("req null"))
		return err
	}

	if req.Uid == nil || req.Password == nil{
		common.ErrorLog(fmt.Sprintf("login req incomplete, %d, %d", req.GetUid(), req.GetPassword()))
		ret = 20001
	}


	rootRsp, rsp, err := LoginServerMsgPack(&LoginServerMsgPackInfo{
		seq:msgInfo.seq,
		msgId:protos.MSGID_CSLogin_Response,
		status:ret,

	})
	if err != nil{
		common.ErrorLog("login req pack failed")
		return err
	}

	rsp.Login = &loginServerMessages.CSLoginResponse{}

	if err := connector.WriteMessage(rootRsp, int(protos.MSGID_LoginServer_Message)); err != nil{
		common.ErrorLog("write error", err)
		return err
	}

	return nil

}



type CSLoginResponseHandler struct{

}


func (handler *CSLoginResponseHandler) Handle(connector *tcpserver.Connector,header interface{}, message interface{}) error{

	msgInfo,err := LoginServerMsgUnpack(message, false)
	if err != nil{
		common.ErrorLog(fmt.Sprintf("unpack msg failed"))
		return err
	}

	rsp := msgInfo.rsp.GetLogin()
	if rsp == nil{
		common.ErrorLog(fmt.Sprintf("rsp null"))
		return err
	}



	if rsp != nil{

		common.InfoLog("got login rsp")

	}



	return nil

}















