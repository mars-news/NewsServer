package messageHandler

import (
	"MarsXserver/protos"
	"log"
	"errors"
	"MarsXserver/common"
)


type LoginSeverMsgUnpackedInfo struct{
	req *protos.Request
	rsp *protos.Response
	seq *int64
}

type LoginServerMsgPackInfo struct {
	seq *int64
	msgId protos.MSGID
	status int32
}


func LoginServerMsgGetMsgId(message interface{}) (msgId int, err error){

	msg, ok := message.(*protos.LoginServerMessage)
	if ok != true{
		return 0, common.ErrorLog("msg is not login tcpserver msg")
	}

	return int(*msg.MsgId), nil
}

func LoginServerMsgGetSeq(message interface{}) (seq int64, err error){

	msg, ok := message.(*protos.LoginServerMessage)
	if ok != true{
		return 0, common.ErrorLog("msg is not login tcpserver msg")
	}

	return *msg.Seq, nil

}

func LoginServerMsgSetSeq(message interface{}, seq int64) error{

	msg, ok := message.(*protos.LoginServerMessage)
	if ok != true{
		return common.ErrorLog("msg is not login tcpserver msg")
	}

	msg.Seq = &seq

	return nil
}




func LoginServerMsgUnpack(message interface{}, isReq bool) (unpackedInfo *LoginSeverMsgUnpackedInfo, err error){

	msg, ok := message.(*protos.LoginServerMessage)
	if ok != true{
		common.ErrorLog("msg is not login tcpserver msg")
		return nil, errors.New("msg is not login tcpserver msg")
	}

	req := msg.GetRequest()
	if req == nil && isReq{
		log.Fatal("msg no req")
		return nil, errors.New("msg no req")
	}

	rsp := msg.GetResponse()
	if rsp == nil && !isReq{
		log.Fatal("msg no rsp")
		return nil, errors.New("msg no rsp")
	}

	return &LoginSeverMsgUnpackedInfo{
		req: req,
		rsp: rsp,
		seq: msg.Seq,
	}, nil
}

func LoginServerMsgPack(info *LoginServerMsgPackInfo) (msg *protos.LoginServerMessage,rsp *protos.Response, err error){

	rspMsg := &protos.LoginServerMessage{
		MsgId: &info.msgId,
		Seq: info.seq,
		Response: &protos.Response{
			Ret: &info.status,
		},
	}

	if rspMsg == nil{
		common.ErrorLog("init login tcpserver msg failed, info:", info)
		return nil, nil, errors.New("init login tcpserver msg failed")
	}

	if rspMsg.GetResponse() == nil{
		common.ErrorLog("init login tcpserver msg rsp nil , info:", info)
		return nil, nil, errors.New("init login tcpserver msg rsp nil")
	}

	return rspMsg, rspMsg.GetResponse(), nil


}



















