package objHandler

import (
	"MarsXserver/tcpserver"
	"MarsXserver/objMessages"
	"MarsXserver/common"
	"MarsXserver/crawler/app"
	"MarsXserver/bridge"
)

type ObjMessageStartHandler struct{

}



func (handler *ObjMessageStartHandler) Handle(connector *tcpserver.Connector, header interface{}, body interface{}) error {

	reqHeader := header.(*objMessages.ObjRequestHeader)

	_, ok := body.(*objMessages.CrawlerStartRequest)
	if ok == false{
		connector.SendObjErrorResponse(reqHeader, -1)
		return common.ErrorLog("body is not expr request")
	}

	crawlerInf := connector.Server.GetParentContext()
	cr, ok := crawlerInf.(*app.Crawler)
	if !ok {
		return common.ErrorLog("crawler is not in parent context")
	}

	cr.Observer.RegisterObserver(app.Observer_Crawl_Start, false, nil, func(ctx *bridge.ObserverContext){

		if err := connector.WriteObjResponseMesage(reqHeader, 0, &objMessages.CrawlerStartResponse{}); err != nil {
			common.ErrorLog("write msg response error", err, " msg id:", int(objMessages.MessageId_Crawler_StartResponse))
			return
		}

	})

	cr.Start()

	return nil
}


