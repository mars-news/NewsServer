package crawler

import (
	"MarsXserver/crawler/app"
	"MarsXserver/tcpserver"
	"MarsXserver/objMessages"
	"MarsXserver/crawler/objHandler"
	"reflect"
)

func initRegisterTcpHandlers(sServer *app.Crawler){

	/*
	tcpserver.RegisterHandler(
		int(objMessages.MessageId_Crawler_GetInfoRequest),
		&tcpserver.HandlerItem{
			Handler: &objHandler.ObjMessageGetInfoHandler{},
		})
	*/


	tcpserver.RegisterHandler(
		int(objMessages.MessageId_Crawler_StartRequest),
		&tcpserver.HandlerItem{
			MsgProtoType: tcpserver.MessageProtoType_ObjRequest,
			Handler:      &objHandler.ObjMessageStartHandler{},
			ReqMsgType:   reflect.TypeOf(objMessages.CrawlerStartRequest{}),
			RspMsgType:   reflect.TypeOf(objMessages.CrawlerStartResponse{}),
		})


}



func InitRegisterHandlers(aServer *app.Crawler){

	initRegisterTcpHandlers(aServer)

}
