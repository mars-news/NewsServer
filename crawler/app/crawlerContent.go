package app

import (
	"MarsXserver/common"
	"github.com/PuerkitoBio/goquery"
	"net/http"
	"MarsXserver/objMessages"
	"io/ioutil"
	"path/filepath"
	"fmt"
	"strings"
)




func (this *Crawler) saveContent(cell *WorkCell, content string) (filePath string, imgPaths []string, err error) {

	dec := fmt.Sprintf("<xxxxx>%s</xxxxx>", content)

	doc, _ := goquery.NewDocumentFromReader(strings.NewReader(dec))

	imgPaths = make([]string, 0)

	doc.Find("img").Each(func(index int, s *goquery.Selection){

		imgSrc, ok := s.Attr("src")
		if ok == false{
			common.ErrorLog("image node no src attr", cell.url)
			return
		}

		newPath, err := this.saveOneImageNode(cell, imgSrc)
		if err != nil{
			common.ErrorLog("save image node failed", imgSrc)
			return
		}

		s.SetAttr("src", newPath)

		imgPaths = append(imgPaths, newPath)

	})

	doc.Find("a").Each(func(index int, s *goquery.Selection) {
		//s.SetAttr("href", "#")
		newHtmlNode := fmt.Sprintf("<span>%s</span>", s.Text())
		s.ReplaceWithHtml(newHtmlNode)
	})

	doc.Find("script").Each(func(index int, s *goquery.Selection) {

		s.Remove()
	})

	doc.Find(cell.rule.ContentBodyExcludeRule).Each(func(index int, s *goquery.Selection){
		s.Remove()
	})



	htmlContent, err := doc.Find("xxxxx").Html()
	if err != nil{
		return "", nil, common.ErrorLog("get content html failed", cell.url.String(), err)
	}

	frsp, err := this.TcpSendFileFunc(
		&objMessages.CrawlNewsContentRequest{
			SrcName: cell.rule.Source.Name,
			TimeFolderName: cell.timeFolder,
		},
		"xxxxx",
		[]byte(htmlContent),
	)
	if err != nil{

		return "", nil, common.ErrorLog("write json error:", err)
	}

	rsp, ok := frsp.(*objMessages.CrawlNewsContentResponse)
	if !ok{
		return "", nil, common.ErrorLog("rsp is not news content")
	}

	return rsp.SrcPath, imgPaths, nil
}




func (this *Crawler) saveOneImageNode(cell *WorkCell, imageSrc string) (picPath string, err error){

	parsedUrl, err := cell.url.Parse(imageSrc)
	if err != nil{
		common.ErrorLog("parse image src failed:", imageSrc)
		return
	}

	_url := cell.url.ResolveReference(parsedUrl)
	if _url == nil{
		err = common.ErrorLog("resolve image src failed:", imageSrc)
		return
	}

	rsp, err := http.Get(_url.String())
	if err != nil || rsp == nil || rsp.StatusCode < 200 || rsp.StatusCode >= 300{
		err = common.ErrorLog("image rsp is err :", imageSrc)
		return
	}

	defer rsp.Body.Close()

	imgBytes, err := ioutil.ReadAll(rsp.Body)
	if err != nil{
		err = common.ErrorLog("read image bytes failed", imageSrc)
		return
	}


	frspInf, err := this.TcpSendFileFunc(
		&objMessages.CrawlContentImageRequest{
			SrcName: cell.rule.Source.Name,
			TimeFolderName: cell.timeFolder,
		},
		filepath.Base(imageSrc),
		imgBytes,
	)
	if err != nil{
		err = common.ErrorLog("write json error:", err)
		return
	}

	frsp, ok := frspInf.(*objMessages.CrawlContentImageResponse)
	if !ok{
		err = common.ErrorLog("rsp is not content image")
		return
	}


	return frsp.SrcPath, nil

}


















