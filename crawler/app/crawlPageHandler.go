package app

import (
	"MarsXserver/common"
	"time"
	"MarsXserver/db"
	"MarsXserver/httpserver"
	"path/filepath"
)


func (this *Crawler) getTimeFolder() string{

	timeFolder := time.Now().UTC().Format("2006-01-02")
	return timeFolder

}

func (this *Crawler) getList(cell *WorkCell, srcId int32){

	common.InfoLog("get list of ", cell.rule.Addr, " list rule:", cell.rule.ListRule)

	listRuleObj := httpserver.NewRuleObject(cell.rule.ListRule)
	titleRuleObj := httpserver.NewRuleObject(cell.rule.ListTitleRule)
	urlRuleObj := httpserver.NewRuleObject(cell.rule.ListUrlRule)

	res, _ := httpserver.NewHtmlPageAnalyzer(cell.rule.Addr, true).
		ForEach("list", listRuleObj).
		Get("title", titleRuleObj).
		Get("url", urlRuleObj).
		Run()

	if res == nil{
		common.ErrorLog("get list failed", cell.rule.Addr)
		return
	}

	length := res.At("list").Len()

	if length <= 0{
		common.ErrorLog("get list empty", cell.rule.Addr)
		return
	}

	smallListSet := common.NewMSet()

	for ii := 0; ii < length; ii++{

		_urlSub, err := res.At("list").Idx(ii).Get("url")
		if err != nil{
			common.ErrorLog("get list", ii, "url failed", cell.rule.Addr)
			continue
		}

		_url, err := res.UrlRef.Parse(_urlSub)
		if err != nil{
			common.ErrorLog("parse url failed:", _urlSub, " when getting list of ", cell.rule.Addr, err)
			continue
		}

		_url.RawQuery = ""

		_urlId := _url.String()[cell.rule.ListUrlIdPartOffset:]
		common.InfoLog("url id:", _urlId)

		if smallListSet.Contains(_urlId){
			common.InfoLog("this addr is repeated in curr loop list, ", _urlId)
			continue
		}

		if this.crawledSet.Contains(_urlId){
			common.InfoLog("this addr is crawled, ", _urlId)
			continue
		}

		cell := &WorkCell{rule: cell.rule, time: time.Now().UTC(), workfunc:this.getContent, url:_url, timeFolder:cell.timeFolder	}

		this.workQueues[srcId].PushWork(cell)

		smallListSet.Insert(_urlId)

	}

	common.InfoLog("curr work set count:", this.workQueues[srcId].Length())


}





func (this *Crawler) getContent(cell *WorkCell, srcId int32 ){

	this.srcWaitMap.Set(cell.rule.Source.Id, time.Now().UTC())

	idUrl := cell.url.String()[cell.rule.ListUrlIdPartOffset:]
	this.crawledSet.Insert(idUrl)

	common.InfoLog("get from item:", cell.url.String())

	titleRuleObj := httpserver.NewRuleObject(cell.rule.ContentTitleRule)
	bodyRuleObj := httpserver.NewRuleObject(cell.rule.ContentBodyRule)

	analyzer := httpserver.NewHtmlPageAnalyzer(cell.url.String(), true).
		Get("title", titleRuleObj).
		Get("body", bodyRuleObj)


	if len(cell.rule.ContentTitleImageRule) > 0{
		titleImageObj := httpserver.NewRuleObject(cell.rule.ContentTitleImageRule)
		analyzer = analyzer.Get("timage", titleImageObj)
	}

	if len(cell.rule.ContentTitleImageTextRule) > 0{
		titleImageTextObj := httpserver.NewRuleObject(cell.rule.ContentTitleImageTextRule)
		analyzer = analyzer.Get("timaget", titleImageTextObj)
	}

	res, _ := analyzer.Run()

	if res == nil{
		common.ErrorLog("get list failed", cell.rule.Addr)
		return
	}


	var outErr error
	var titleStr string
	var bodyPath string
	var bodyImgPaths []string
	var titleImagePath string
	var titleImageText string
	var err error

	for{
		titleStr, err = res.Get("title")
		if err != nil || len(titleStr) <= 0{

			outErr = common.ErrorLog("title none")
			break
		}

		bodyContent, err := res.Get("body")
		if err != nil || len(bodyContent) <= 0{
			outErr = common.ErrorLog("body none")
			break
		}

		bodyPath, bodyImgPaths, err = this.saveContent(cell, bodyContent)
		if err != nil || len(bodyPath) <= 0{
			outErr = common.ErrorLog("body none")
			break
		}

		titleImageUrl, err := res.Get("timage")
		if err != nil || len(titleImageUrl) <= 0{
			break
		}

		titleImagePath, err = this.saveOneImageNode(cell, titleImageUrl)
		if err != nil{
			common.InfoLog("save image node failed", err)
			break
		}

		titleImageText, err = res.Get("timaget")

		break
	}


	thumbs := ""

	if len(titleImagePath) <= 0 {

		if len(bodyImgPaths) > 0 {

			thumbs = thumbs + bodyImgPaths[0]
		}
	}


	if len(bodyImgPaths) > 1{

		ii := 0

		for _, path := range bodyImgPaths{

			baseName := filepath.Base(path)
			thumbs = thumbs + ";" + baseName
			ii += 1
			if ii >= 2{
				break
			}
		}
	}



	if outErr != nil{
		return
	}

	crawlItem := &db.CrawlItem{
		Addr:	cell.url.String(),
		FilePath: bodyPath,
		RuleId: cell.rule.Id,
		SourceId: cell.rule.Source.Id,
		Title: titleStr,
		Thumbs: thumbs,
		TitleImage: titleImagePath,
		TitleImageText: titleImageText,
		Timestamp: time.Now().UTC(),
		Category: cell.rule.Category,
		Feature: cell.rule.Feature,
		ShowKind: cell.rule.ShowKind,
	}

	this.Orm.Save(crawlItem)

}

