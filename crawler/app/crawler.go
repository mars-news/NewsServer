package app

import (
	"time"
	"net/http"
	"github.com/PuerkitoBio/goquery"
	"net/url"
	"MarsXserver/bridge"
	"MarsXserver/common"
	"sync"
	"MarsXserver/db"
	"fmt"
	"MarsXserver/orm"
	"MarsXserver/hybridServer"
	"strconv"
	"MarsXserver/proj"
)

const (
	DefaultUserAgent                   string        = "Mozilla/5.0 (Windows NT 6.1; rv:15.0) Gecko/20120716 Firefox/15.0a2"
	DefaultCrawlSleep                  time.Duration = time.Second * 4
	DefaultEatTimeOut		   time.Duration = time.Second * 4
	DefaultCrawlRefreshHistoryDuration time.Duration = time.Hour * 24 //read from database again to restore history data
	InitRuleListLen                                  = 10

)


const (
	_ bridge.ObserverType = iota
	Observer_Crawl_Start

)

const (
	DB_Dialer_Type string = "db"
)

var (
	Crawl_Dialers = []string{DB_Dialer_Type}
)

type AnalyzeFunc func(response *http.Response, document *goquery.Document)

type UrlTargets struct{
	urls []* url.URL
}


type WorkFunc func(cell *WorkCell, srcId int32)

type WorkCell struct{

	rule *db.CrawlRule
	time time.Time
	timeFolder string
	workfunc WorkFunc

	url *url.URL
}





type Crawler struct{

	hybridServer.HybridBaseServer

	IsStart bool

	httpClient *http.Client

	configData *crawlerConfigData

	srcs       map[int32]*db.CrawlSource
	rules      map[int32][]*db.CrawlRule
	scanIndex  *common.MMap
	scanChannels map[int32]chan interface{}
	srcWaitMap *common.MMap

	crawledSet  *common.MSet
	crawledLastTime time.Time

	workQueues map[int32]*common.MList

	ShutDown chan struct{}

}


func NewCrawler(configName string) (*Crawler, error){

	config, err := LoadConfig(configName)
	if err != nil{
		return nil, common.ErrorLog("load Config failed", err)
	}

	crawler := &Crawler{
		IsStart: false,

		httpClient: &http.Client{},

		configData: config,

		srcs:       make(map[int32]*db.CrawlSource),
		rules:      make(map[int32][]*db.CrawlRule),
		scanIndex:  common.NewMMap(),
		scanChannels: make(map[int32]chan interface{}),
		srcWaitMap: common.NewMMap(),

		crawledSet: common.NewMSet(),

		workQueues: make(map[int32]*common.MList),

		ShutDown: make(chan struct{}, 8),

	}

	crawler.InitBaseServer(config.Name, crawler, &config.TConfig, &config.HConfig, nil, nil)

	crawler.Orm.RegisterSendFunc(crawler.OrmSendFunc)
	crawler.Orm.RegisterBytesRspFunc(orm.OrmHandleBytesRspFunc)

	crawler.TServer.SetLegalDialerNames(Crawl_Dialers)

	return crawler, nil
}


func (this *Crawler) Run(blockMode bool) error{
	return this.RunBaseServer(blockMode)
}



func (this *Crawler) Start(){

	initRuleChannel := make(chan int)

	go this.initRules(initRuleChannel)
	ret, ok := <-initRuleChannel
	if !ok || ret != 0{
		common.ErrorLog("init rule channel failed")
		return
	}

	go this.feedSchedule()

	this.IsStart = true

	this.Observer.EventHappened(Observer_Crawl_Start, struct {}{})

	common.InfoLog("crawler inited")

	return
}


func (this *Crawler) TcpSendFileFunc(msg interface{},fileName string, data []byte) (interface{}, error){

	return this.TcpSendFileFuncByType(DB_Dialer_Type,
		msg,
		fileName,
		data)

}


func (this *Crawler) OrmSendFunc(expr *orm.XOrmEprData, retCh chan interface{}){

	common.InfoLog("send func:", expr.ModelName, " optype", expr.OpType)
	this.OrmSendFuncByType(
		DB_Dialer_Type,
		expr,
		retCh,
	)
}



func (this *Crawler) initCrawlScanHistory(initHistoryChannel chan int){

	common.InfoLog("init crawl history")

	defer func() {
		if err := recover(); err != nil{
			initHistoryChannel <- -1
		}

	}()

	scanKeys := this.scanIndex.Keys()

	for _, scanKey := range scanKeys{
		this.scanIndex.Set(scanKey, 0)
	}

	this.crawledSet.Clear()

	now := time.Now().UTC()

	pre := now.Add(-time.Hour * 24)
	preStr := pre.Format("2006-01-02 15:04:05")

	retChannel := make(chan []interface{})

	var retErr error

	go this.Orm.NewOrmExpr(new(db.CrawlItem)).List("addr").Filter("timestamp", ">", "'" + preStr + "'").AsyncMultiRunDefault(retChannel, &retErr)

	for retlist := range retChannel{

		for _, ret := range retlist{

			item := ret.(map[string]interface{})

			addr, ok := item["addr"].(string)
			if !ok {
				common.ErrorLog("get addr null")
				continue
			}

			this.crawledSet.Insert(addr)
		}
	}

	if retErr != nil{

		initHistoryChannel <- -1
	}else{

		initHistoryChannel <- 0
	}

}



func (this *Crawler) initRules(initRuleChannel chan int){

	srcList, err := this.Orm.List(new(db.CrawlSource), 0, 0)
	if err != nil{
		common.ErrorLog("get crawl source failed", err)
		return
	}

	for _, _src := range srcList{
		src, ok := _src.(*db.CrawlSource)
		if ok != true{
			common.ErrorLog("src interface is not crawl type")
			return
		}

		this.srcs[src.Id] = src
		this.scanIndex.Set(src.Id, 0)
	}

	readRuleChannel := make(chan []interface{})

	var retErr error

	go this.Orm.NewOrmExpr(new(db.CrawlRule)).List().AsyncMultiRunDefault(readRuleChannel, &retErr)

	for retlist := range readRuleChannel{

		for _, ret := range retlist{

			item := ret.(*db.CrawlRule)

			rulesBySrc, ok := this.rules[item.Source.Id]
			if ok != true{
				this.rules[item.Source.Id] = make([]*db.CrawlRule, 0, InitRuleListLen)
			}

			rulesBySrc = append(rulesBySrc, item)
			this.rules[item.Source.Id] = rulesBySrc

		}
	}

	if retErr != nil{
		initRuleChannel <- -1
	}else {
		initRuleChannel <- 0
	}


}


func (this *Crawler) feedSchedule(){

	common.InfoLog("feedSchedule")

	now := time.Now().UTC()

	common.InfoLog(fmt.Sprintf("now:%s, last:%s, dur:%d, ddur:%d set:%d", common.PrintTime(now), common.PrintTime(this.crawledLastTime), now.Sub(this.crawledLastTime).Hours(), DefaultCrawlRefreshHistoryDuration.Hours(), this.crawledSet.Size()))

	if this.crawledSet.IsEmpty() || now.Sub(this.crawledLastTime) > DefaultCrawlRefreshHistoryDuration {
		initHistoryChannel := make(chan int)
		go this.initCrawlScanHistory(initHistoryChannel)
		<- initHistoryChannel
		this.crawledLastTime = time.Now().UTC()
		common.InfoLog(fmt.Sprintf("last time:%s set size:%d", common.PrintTime(this.crawledLastTime), this.crawledSet.Size()))
	}


	for _, src := range this.srcs{
		this.workQueues[src.Id] = common.NewMList()
		this.scanChannels[src.Id] = make(chan interface{})
		this.srcWaitMap.Set(src.Id, time.Now().UTC().Add(- DefaultCrawlSleep))
	}

	var group sync.WaitGroup

	group.Add(1)
	go this.feedWork(&group)
	group.Add(1)
	go this.eatWork(&group)

	group.Wait()

	common.InfoLog("schedule finished", common.PrintTime(now))

	timeAfterFeed := time.Now().UTC()

	timePast := timeAfterFeed.Sub(now)

	if timePast < proj.CrawlRefreshInterval {
		timeDiff := proj.CrawlRefreshInterval - timePast
		time.AfterFunc(timeDiff, this.feedSchedule)
		common.InfoLog(" next feed after:", timeDiff.Minutes())
		return
	}

	go this.feedSchedule()

}




func (this *Crawler) feedOneSrc(srcId int32, group *sync.WaitGroup){


	defer func(){
		common.SafeCloseChannel(this.scanChannels[srcId])
		common.InfoLog("group rm")
		group.Done()
		if err := recover(); err != nil{
			common.ErrorLog("feed src panic", srcId)
		}
	}()


	common.InfoLog("feed src start", srcId)

outFor:
	for{
		prevTime := this.srcWaitMap.Get(srcId).(time.Time)

		now := time.Now().UTC()

		currScanIndex := this.scanIndex.Get(srcId).(int)

		if currScanIndex >= len(this.rules[srcId]){
			common.InfoLog("src", srcId, "feed finished")
			break outFor
		}

		diff := now.Sub(prevTime)

		diffSleep := DefaultCrawlSleep - diff


		if diffSleep.Seconds() > 0{

			time.Sleep(diffSleep)

		}

		common.InfoLog("feed src:" + strconv.Itoa(int(srcId)) + " time now:", common.PrintTime(now), " prev:", common.PrintTime(prevTime) )

		cell := &WorkCell{
			rule:this.rules[srcId][currScanIndex],
			workfunc: this.getList,
			time: now.Add(-DefaultCrawlSleep),
			timeFolder: this.getTimeFolder(),

		}

		common.InfoLog("push rule work:", cell.rule.Id )
		this.workQueues[srcId].PushWork(cell)

		this.scanIndex.Set(srcId, currScanIndex + 1)

		this.srcWaitMap.Set(srcId, now)


		select {

		case this.scanChannels[srcId] <- struct {}{}:   //one to one
			continue
		case <- this.ShutDown:
			break outFor
		}
	}

}

func (this *Crawler) eatOneSrc(srcId int32, group *sync.WaitGroup){
	defer func(){
		group.Done()
		common.InfoLog("eat done", srcId)
		if err := recover(); err != nil{
			<- this.scanChannels[srcId]
			common.ErrorLog("eat src panic",srcId, err)
		}
	}()



outerFor:
	for{

		common.InfoLog("eat work once....", srcId)


		select {
		case <- this.ShutDown:
			break outerFor

		case _, ok := <- this.scanChannels[srcId]:
			if ok == false{
				common.InfoLog("eat scan channel closed", srcId)
				break outerFor
			}

		case <- time.After(DefaultEatTimeOut):
			common.InfoLog("eat one src timeout:", srcId)
		}

		queue := this.workQueues[srcId]

		for{

			now := time.Now().UTC()

			frontWork := queue.FrontWork()

			if frontWork == nil{
				common.InfoLog("work queue empty...", srcId)
				//time.Sleep(DefaultCrawlSleep)
				break
			}

			prevScanTime := this.srcWaitMap.Get(srcId).(time.Time)
			elapsedTime := now.Sub(prevScanTime)
			sleepDu := DefaultCrawlSleep - elapsedTime


			if  sleepDu> 0 {
				time.Sleep(sleepDu)
			}


			common.InfoLog("work eat run", common.PrintTime(now))
			work := queue.PopWork().(*WorkCell)
			work.workfunc(work, srcId)

			this.srcWaitMap.Set(srcId, now)

		}



	}
}



func (this *Crawler) feedWork(parentGroup *sync.WaitGroup){

	defer parentGroup.Done()

	var group sync.WaitGroup

	srcIds := this.srcWaitMap.Keys()


	for _, srcId_ := range srcIds {

		srcId := srcId_.(int32)
		group.Add(1)
		common.InfoLog("group add")
		go this.feedOneSrc(srcId, &group)
	}

	group.Wait()
	common.InfoLog("feed work finished")
}




func (this *Crawler) eatWork(parentGroup *sync.WaitGroup){

	defer parentGroup.Done()

	var group sync.WaitGroup

	srcIds := this.srcWaitMap.Keys()


	for _, srcId_ := range srcIds {
		srcId := srcId_.(int32)
		group.Add(1)

		common.InfoLog("group add", srcId)

		go this.eatOneSrc(srcId, &group)

	}

	common.InfoLog("wait eat")

	group.Wait()

	common.InfoLog("eat work finished")
}



























