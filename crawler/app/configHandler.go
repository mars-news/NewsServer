package app

import (
	"encoding/xml"
	"io/ioutil"
	"MarsXserver/common"
	"path/filepath"
	"MarsXserver/tcpserver"
	"MarsXserver/httpserver"
)





type crawlerConfigFileData struct{
	Sid	int 		`xml:"sid"`
	Name	string		`xml:"name"`


	TConfig tcpserver.TcpConfigData	`xml:"tconfig"`
	HConfig httpserver.HttpConfigData `xml:"hconfig"`
}

type crawlerConfigData struct{
	crawlerConfigFileData
}


func LoadConfig(name string) (configData *crawlerConfigData, err error){

	configData = &crawlerConfigData{}

	fileName := filepath.Join(common.DefaultConfPath, name + ".xml")

	content, err := ioutil.ReadFile(fileName)
	if err!= nil{
		common.ErrorLog("read config failed", err)
		return nil, err
	}

	if err = xml.Unmarshal(content, configData); err != nil{
		common.ErrorLog("unmarshal error", err)
		return nil, err
	}

	return configData, nil
}

























