package proj

import (
	"github.com/nfnt/resize"
	"bytes"
	"image"
	"image/gif"
	"image/png"

	"image/jpeg"
	"os"
	"path/filepath"
	"strings"
	"MarsXserver/common"
)

const(

	CONST_THUMBNAIL_HEIGHT = 100
	CONST_THUMBNAIL_WIDTH = 160


	Mime_JPEG = "jpeg"
	Mime_PNG = "png"
	Mime_GIF = "gif"

)

var magicTable = map[string]string{
	"\xff\xd8\xff":      "image/jpeg",
	"\x89PNG\r\n\x1a\n": "image/png",
	"GIF87a":            "image/gif",
	"GIF89a":            "image/gif",
}


func mimeFromImageData(incipit []byte) string {
	incipitStr := string(incipit)
	for magic, mime := range magicTable {
		if strings.HasPrefix(incipitStr, magic) {
			return mime
		}
	}

	return ""
}

func GetThumbnailBytes(oriImage []byte) ([]byte, error){

	reader := bytes.NewReader(oriImage)

	img, fname, err := image.Decode(reader)

	if err != nil{
		return nil, err
	}

	common.InfoLog("thumbnail format", fname)

	newImage := resize.Thumbnail(CONST_THUMBNAIL_WIDTH, CONST_THUMBNAIL_HEIGHT, img, resize.Bilinear)

	res := new(bytes.Buffer)


	switch fname {
	case Mime_JPEG:
		if err := jpeg.Encode(res, newImage, nil); err != nil{
			return nil, err
		}
	case Mime_PNG:
		if err := png.Encode(res, newImage); err != nil{
			return nil, err
		}
	case Mime_GIF:
		if err := gif.Encode(res, newImage,nil); err != nil{
			return nil, err
		}
	default:
		return nil, common.ErrorLog("mime type is illegal")
	}



	return res.Bytes(), nil

}





func GetThumbnailFile(oldPath string, newPath string) error {

	reader, err := os.Open(oldPath)
	if err != nil{
		return err
	}

	defer reader.Close()

	img, fname, err := image.Decode(reader)

	if err != nil{
		return err
	}

	common.InfoLog("thumbnail format", fname)

	newImage := resize.Thumbnail(CONST_THUMBNAIL_WIDTH, CONST_THUMBNAIL_HEIGHT, img, resize.Bilinear)

	if filepath.Ext(newPath) != ".jpg"{

		return common.ErrorLog("thumbnail is not jpg ext")

	}

	out, err := os.Create(newPath)
	if err != nil{
		return common.ErrorLog("create new path failed", err)
	}

	defer out.Close()

	switch fname {
	case Mime_JPEG:
		if err := jpeg.Encode(out, newImage, nil); err != nil{
			return err
		}
	case Mime_PNG:
		if err := png.Encode(out, newImage); err != nil{
			return err
		}
	case Mime_GIF:
		if err := gif.Encode(out, newImage,nil); err != nil{
			return err
		}
	default:
		return common.ErrorLog("mime type is illegal")
	}

	return nil
}


























