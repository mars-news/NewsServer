package proj

import (
	"time"
	"math"
	"MarsXserver/common"
)

const (

	DayHourIdx_Time_Layout = "2006-1-2 15"

)


type HourIdx struct {

	HourIdx int64

}


func CloneHourIdx(old *HourIdx) *HourIdx {

	return &HourIdx{
		HourIdx: old.HourIdx,
	}

}


func Time2HourIdx(tm time.Time) *HourIdx{

	return &HourIdx{
		HourIdx: tm.Unix()/int64(Collect_Ratings_Time_Span.Seconds()),

	}
}


func HourIdxGetPrevDayIdxs() []HourIdx {

	hourIdx := HourIdxGetNearestPrevIdx()

	spanCnt := time.Hour * 24 /Collect_Ratings_Time_Span

	res := make([]HourIdx, 0, spanCnt)

	newIdx := CloneHourIdx(hourIdx)

	for ii:= 0; ii < int(spanCnt); ii++{

		res = append(res, *newIdx)

		newIdx = &HourIdx{
			HourIdx: newIdx.HourIdx - 1,
		}
	}

	return res

}


func HourIdxGetNearestPrevIdx() *HourIdx {

	now := time.Now().UTC()

	hourIdx := Time2HourIdx(now).HourIdx

	newIdx := hourIdx -1

	return &HourIdx{
		HourIdx: newIdx,
	}

}

func HourIdxGetCurrIdx() *HourIdx{

	now := time.Now().UTC()

	return Time2HourIdx(now)

}

func (this *HourIdx) AddDuration(du time.Duration) *HourIdx{

	spanCnt := du /Collect_Ratings_Time_Span

	offset := -1
	if du < 0{
		offset = 1
	}

	if math.Abs(float64(spanCnt * Collect_Ratings_Time_Span)) >= math.Abs(float64(du)){
		return &HourIdx{
			this.HourIdx + int64(spanCnt),
		}
	}else{
		return &HourIdx{
			this.HourIdx + int64(spanCnt) + int64(offset),
		}
	}
}

func (this *HourIdx) ToDBMinMaxStr() (string, string){

	hourMin := this.HourIdx
	hourMax := this.HourIdx + 1

	secMin := hourMin * 3600
	secMax := hourMax * 3600

	tmMin := time.Unix(secMin, 0).UTC()
	tmMax := time.Unix(secMax, 0).UTC()

	strMin := common.MakeDBTimeString(tmMin)
	strMax := common.MakeDBTimeString(tmMax)

	return strMin, strMax
}



func (this *HourIdx) ToDBStr() (string){

	hour := this.HourIdx

	sec := hour * 3600 + 1800

	min := time.Unix(sec, 0).UTC()

	str := common.MakeDBTimeString(min)

	return str
}



func HourIdxPrevIdx(old *HourIdx) (*HourIdx){

	return &HourIdx{
		HourIdx: old.HourIdx -1,
	}

}


func HourIdxNextIdx(old *HourIdx) (*HourIdx){

	return &HourIdx{
		HourIdx: old.HourIdx + 1,
	}

}

func HourIdxCompare(idx1, idx2 *HourIdx) int{

	diff := idx1.HourIdx - idx2.HourIdx
	if diff < 0{
		return -1
	}else if diff == 0{
		return 0
	}else{
		return 1
	}
}







































