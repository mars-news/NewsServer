package proj

import (
	"time"
	"MarsXserver/common"
)


const (
	IsDebug = false
)


const (

	Collect_Ratings_Time_Span = time.Hour * 1
	Collect_Day_Time_Spon = time.Hour * 24 //hour
	Max_Score_Value           = 10

)


const (
	CrawlRefreshInterval               time.Duration = time.Hour * 1
)

const (
	ThumbnailImageNameConcat = "_thumb"
	ThumbnailImageExt	= ".jpg"
)

//app relate

const (
	//notice per project id starts from 10000

	PipeType_DBServer				 = 10000
	PipeType_AdminServer			 = 20000

	MessageId_CrawlerObjMessageIdStart = 30000
	MessageId_SuggestMessageIdStart	   = 40000
	MessageId_DoopMessageIdStart	   = 50000
	MessageId_RedServerMessageIdStart  = 60000
	MessageId_DBServerMessageIdStart   = 70000

)






func init(){

	common.DefaultConfPath = "conf"
	common.DefaultCookieName = "HappyNews"


}
































