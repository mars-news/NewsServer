package db

import "time"

type CrawlSource struct {
	Id int32	`orm:"auto"`
	Name string	`orm:"size(20)"`
	ShowName string `orm:"size(40)"`
	PicPath string	`orm:"size(256)"`
}

type CrawlRule struct {
	Id int32		`orm:"auto"`
	RuleName string		`orm:"size(256)"`
	Addr string 		`orm:"size(256)"`
	ListRule string		`orm:"size(256)"`
	ListTitleRule string 	`orm:"size(256)"`
	ListUrlRule string 	`orm:"size(256)"`
	ListUrlIdPartOffset int32	`orm:""`
	ContentTitleRule string	`orm:"size(256)"`
	ContentBodyRule string	`orm:"size(256)"`
	ContentBodyExcludeRule string `orm:"size(256)"`
	ContentTitleImageRule string 		`orm:"size(256)"`
	ContentTitleImageTextRule string	`orm:"size(256)"`
	Category int32 		`orm:""`
	Feature int32		`orm:""`
	Position int32		`orm:""`
	ShowKind int32		`orm:""` //the news show type: slide , singlePage
	Age int32 		`orm:""`
	Source *CrawlSource	`orm:"rel,default"`
}


type Cat struct{
	Num int
	Name string
}

var (
	CAT_SOCIAL	= Cat{0, "social"}
	CAT_POLITICS = Cat{1, "politics"}
	CAT_WORLD	= Cat{2, "world"}
	CAT_ECONOMY	= Cat{3, "economy"}
	CAT_SPORT	= Cat{4, "sport"}
	CAT_TECH	= Cat{5, "tech"}
	CAT_LIFE	= Cat{6, "life"}
	CAT_CULTURE	= Cat{7, "culture"}
	CAT_HEALTH	= Cat{8, "health"}
	CAT_EDU		= Cat{9, "education"}
	CAT_MAGZINE	= Cat{10, "magzine"}
	CAT_COLUMN = Cat{11, "column"}
	CAT_ENTERTAINMENT = Cat{12, "entertainment"}

	CAT_COUNT 	= 13


	All_CATS = []Cat{CAT_SOCIAL, CAT_POLITICS, CAT_WORLD, CAT_ECONOMY, CAT_SPORT, CAT_TECH, CAT_LIFE, CAT_CULTURE, CAT_HEALTH, CAT_EDU, CAT_MAGZINE, CAT_COLUMN, CAT_ENTERTAINMENT}

)


var (
	Show_Normal = 1
	Show_Slider = 2
	Show_Normal_Slider = 3
)


type CrawlItem struct{
	Id int32		`orm:"auto"`
	RuleId int32 		`orm:""`
	SourceId int32		`orm:""`
	ShowKind int32 		`orm:""`

	Addr	string 		`orm:"size(256)"`
	Title 	string		`orm:"size(1000)"`

	Thumbs	string 		`orm:"size(256)"`

	TitleImage string	`orm:"size(256)"`
	TitleImageText string	`orm:"size(256)"`
	Category int32		`orm:""`
	Feature int32		`orm:""`
	FilePath string 	`orm:"size(256)"`
	Timestamp time.Time	`orm:""`

}



func GetAllCat() []Cat{

	return All_CATS

}



type NewsItem struct {

	CrawlItem
	CommentCnt int

}

















