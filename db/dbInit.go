package db

import (
	"MarsXserver/orm"
	"MarsXserver/common"
)

func init(){

	if err := orm.RegisterModel(&CrawlItem{}, &CrawlRule{}, &CrawlSource{}, &UserInfo{}, &UserInterest{}, &UserSimilarity{}, &UserBlock{}); err != nil{
		common.FatalLog("register model failed")
	}

	if err := orm.Bootstrap(); err != nil{
		common.FatalLog("orm bootstrap failed")
	}
}
