package db

import (
	"time"
	"strings"
	"MarsXserver/common"
	"strconv"
	"regexp"
	"bytes"
)

const(

	UserForbid_Count = 2
	UserForbid_Duration = time.Hour * 24

)


type UserInfo struct {
	Id int32	`orm:"auto"`
	Name string	`orm:"size(20)"`
	MailAddress string `orm:"size(40)"`
	Pwd string	`orm:"size(40)"`
	Platform string	`orm:"size(30)"`
	LastLogin time.Time	`orm:""`

}


type UserNameExtInfo struct {
	Name     string
	HasPhoto bool
}

const(
	UserName_ExtInfo_Count = 1
)

func (this *UserInfo) ChangeName(newName string) error{

	extInfo := GetUserNameExtInfo(this.Name)

	if extInfo == nil{
		return common.ErrorLog("old name to ext info failed")
	}

	extInfo.Name = newName

	this.Name = GetCombinedNameFromExtInfo(extInfo)

	return nil
}

func (this *UserInfo) SetHasPhoto() error{

	extInfo := GetUserNameExtInfo(this.Name)

	if extInfo == nil{
		return common.ErrorLog("old name to ext info failed")
	}

	extInfo.HasPhoto = true

	this.Name = GetCombinedNameFromExtInfo(extInfo)

	return nil


}

func NewUserNameExtInfo(name string) *UserNameExtInfo{
	return &UserNameExtInfo{
		Name: name,
	}
}

func GetUserNameFromCombinedName(name string) string{

	info := GetUserNameExtInfo(name)
	if info == nil{
		common.ErrorLog("get ext info failed", name)
		return ""
	}


	return info.Name
}


func GetCombinedNameFromExtInfo(info *UserNameExtInfo) string{

	if info == nil{
		common.ErrorLog("input ext info empty")
		return ""
	}

	var buff bytes.Buffer

	buff.WriteString(info.Name + "(")

	if info.HasPhoto {
		buff.WriteString("t")
	}else{
		buff.WriteString("f")
	}

	buff.WriteString(")")

	return buff.String()

}


func GetUserNameExtInfo(name string) *UserNameExtInfo{

	extInfo := new(UserNameExtInfo)

	reg, err := regexp.Compile(`([^()]+)\((\w+)\)`)
	if err != nil{
		common.ErrorLog("compile user Name ext regex failed")
		return nil
	}

	res := reg.FindStringSubmatch(name)

	if res == nil || len(res) < UserName_ExtInfo_Count + 2{
		common.ErrorLog("Name ext info match failed", name)
		return nil
	}

	extInfo.Name = res[1]
	extInfo.HasPhoto = res[2] == "t"

	return extInfo

}



type UserInterest struct {

	Id int32	`orm:"pk"`
	Interests string `orm:"size(150)"`
}

func (this *UserInterest) getInterestNumbers()([]int, error){

	inters := strings.Split(this.Interests, ",")
	if len(inters) <= 0{
		return nil, common.ErrorLog("interests split empty", this.Interests)
	}

	res := make([]int, 0)

	for _, intrItem := range inters{

		intr, err := strconv.Atoi(intrItem)
		if err != nil{
			common.ErrorLog("")
		}

		res = append(res, int(intr))
	}

	return res, nil

}


type UserSimilarity struct {

	Id int32	`orm:"pk"`
	Similarities string `orm:"size(1600)"`

}




type UserBlock struct {

	Id int32	`orm:"pk"`
	IsForbidComment int32 `orm:""`   //cannot use bool
	ForbidCommentTime time.Time `orm:""`
	ReportedCount int32 `orm:""`
}
















