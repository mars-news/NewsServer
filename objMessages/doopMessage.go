package objMessages


import (
"MarsXserver/proj"
)

const (
	_  ObjMessageId = proj.MessageId_DoopMessageIdStart + iota
	MessageId_Doop_Get_Hour_Recommends
	MessageId_Doop_Start_Calc

)



type SSDoopGetHourRecommendRequest struct {
	Uid     int
	HourIdx int64
}

type SSDoopGetHourRecommendResponse struct {

	Stat int
	Recs []int

}

type SSDoopStartCalcRequest struct {
	Day bool
	Hour bool
	Before int
}

type SSDoopStartCalcResponse struct {
	Stat int
}