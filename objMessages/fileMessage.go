package objMessages

import (
	"MarsXserver/proj"
)


const (
	_  ObjMessageId = proj.MessageId_DBServerMessageIdStart + iota
	MessageId_File_CrawlSourceImage
	MessageId_File_CrawlNewsContent
	MessageId_File_CrawlContentImage
	MessageId_File_UserPhoto

)



type CrawlSourceImageRequest struct{

	SrcName string
	TimeFolderName string

}

type CrawlSourceImageResponse struct {

	SrcPath string

}


type CrawlNewsContentRequest struct{

	SrcName string
	TimeFolderName string
}

type CrawlNewsContentResponse struct {

	SrcPath string

}


type CrawlContentImageRequest struct {

	SrcName string
	TimeFolderName string

}

type CrawlContentImageResponse struct {
	SrcPath string
}

type UserPhotoImageRequest struct {

	Uid int
}

type UserPhotoImageResponse struct {
	Path string
}














