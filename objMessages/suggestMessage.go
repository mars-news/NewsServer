package objMessages


import (
	"MarsXserver/proj"
)

const (
	_  ObjMessageId = proj.MessageId_SuggestMessageIdStart + iota
	MessageId_Suggest_GetSuggest
	MessageId_Suggest_StartCollect

)


type SuggestGetSuggestRequest struct{
	Uid int
	IsCommon bool
	Cat int
}


type SuggestGetSuggestResponse struct{
	Stat int
	Pids []int

}


type SuggestStartCollectRequest struct {
	Hot bool
	Latest bool
	Before int
}

type SuggestStartCollectResponse struct{
	Stat int
}



