package objMessages

import (
	"time"
	"MarsXserver/proj"
)


const (
	_                                       ObjMessageId = proj.MessageId_CrawlerObjMessageIdStart + iota
	MessageId_Crawler_StartRequest
	MessageId_Crawler_StartResponse
	MessageId_Crawler_GetInfoRequest
	MessageId_Crawler_GetResponseResponse

)


type CrawlerStartRequest struct{
}

type CrawlerStartResponse struct{
}

type CrawlerGetInfoRequest struct {
}

type CrawlerGetInfoResponse struct{
	IsRunning bool
	IsBusy bool
	NextBusyTime time.Time
}

