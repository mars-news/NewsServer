package objMessages

import (
	"time"
	"MarsXserver/proj"
)

const (
	_  ObjMessageId = proj.MessageId_RedServerMessageIdStart + iota
	MessageId_Publish_Comment
	MessageId_Comment_Count
	MessageId_Reply_Count
	MessageId_Refresh_Comment
	MessageId_Refresh_Reply
	MessageId_Vote

	MessageId_Rating_Save
	MessageId_Rating_Collect

	MessageId_Report_Comment


	MessageId_Collect_Hot_News

)



type RedComment struct{
	Id  int
	Nid int
	Uid int32
	Content string
	ReplyCount int
	Vote       int
	Reported 	int
	Timestamp time.Time
}



type RedReply struct{
	Id   int
	Nid int
	Pid int
	Uid int32
	Content string
	Vote int
	Reported int
	Timestamp time.Time
}


type SSComment struct {

	RedComment
}

type SSReply struct {

	RedReply
}

type CSComment struct{

	SSComment

	Name string
}

type CSReply struct {

	SSReply

	Name string
}





type SSPublishContent struct {

	Nid int
	Uid int32
	Content string
	Reply int
	IsReply int32

}


type SSPublishCommentRequest struct{
	Content *SSPublishContent
}


type SSPublishCommentResponse struct{
	Stat int
	Pid int

}


type SSGetCommentCountRequest struct {
	Nids []int

}

type SSGetCommentCountResponse struct {
	Stat int
	Counts []int
}

type SSGetReplyCountRequest struct{
	Nid int
	Pid int
}

type SSGetReplyCountResponse struct{
	Stat int
	Count int
}


type SSRefreshCommentRequest struct {
	Nid	int
	Offset int
}

type SSRefreshCommentResponse struct {
	Stat int
	Comments []*SSComment
}

type SSRefreshReplyRequest struct {
	Nid	int
	Pid 	int
	Offset	int
}

type SSRefreshReplyResponse struct {
	Stat int
	Replies []*SSReply
}


type SSVoteItem struct {
	Pid int
	Rid int
	IsReply bool
}

type SSVoteRequest struct {

	Uid	int
	Nid 	int

	Score  	int
	Votes []SSVoteItem

}

type SSVoteResponse struct {

	Stat int
}






type SSRatingItem struct {
	Nid int
	Score int
}

type SSRatingUserItem struct {
	Uid int
	Ratings []SSRatingItem
}


type SSSaveRatingRequst struct {
	Uid	int
	Ratings []SSRatingItem
	Before	int
}

type SSSaveRatingResponse struct {
	Stat int
}




type SSCollectRatingsRequest struct {

	Offset  int
	HourIdx int64

}

type SSCollectRatingsResponse struct {

	Stat    int
	Records []SSRatingUserItem
	Offset  int
}



type SSCollectHotNewsRequest struct {

	HourIdx int64
}

type SSCollectHotNewsResponse struct {

	Stat int
	Nids []int

}




type SSReportCommentRequest struct {

	Nid int
	Pid int
	Rid int
	IsReply int
	Reason int

}

type SSReportCommentResponse struct {

	Stat int
	Uid int32
}























