package fileHandler

import (
	"os"
	"MarsXserver/tcpserver"
	"MarsXserver/objMessages"
	"MarsXserver/common"
	"MarsXserver/dbserver/app"
	"path/filepath"
	"fmt"
	"MarsXserver/proj"
)




type UserPhotoImageFileHandler struct {

	tcpserver.BaseFileHandlerItem

}

func (this *UserPhotoImageFileHandler) FileTransStartFunc(ctx *tcpserver.FileReceiveContext) (*os.File, error){

	startMsg := ctx.StartMsg.(*objMessages.UserPhotoImageRequest)
	rspMsg := ctx.RspMsg.(*objMessages.UserPhotoImageResponse)

	root := ctx.Conn.Server.GetParentContext()
	dbServer, ok := root.(*app.DbServer)
	if ok == false{
		return nil, common.ErrorLog("parent not a db server")
	}

	picFolder, relPicFolder, err := dbServer.GetUserPhotoFileFolder()
	if err != nil{
		return nil, common.ErrorLog("get pic folder failed for:", startMsg.Uid)
	}

	photoFileName := fmt.Sprintf("%d%s", startMsg.Uid, proj.ThumbnailImageExt)

	photoFilePath := filepath.Join(picFolder, photoFileName)
	relPhotoImageFilePath := filepath.Join(relPicFolder, photoFileName)

	if _, err := os.Stat(photoFilePath); err == nil || !os.IsNotExist(err){
		common.InfoLog("photo pic name updated", startMsg.Uid, " err:", err)
	}

	sourceImageAbsPath, err := filepath.Abs(photoFilePath)
	if err != nil{
		return nil, common.ErrorLog("get abs path failed", " err:", err)
	}

	ctx.SavePath = sourceImageAbsPath

	f, err := os.OpenFile(sourceImageAbsPath, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil{
		return nil, common.ErrorLog("create file failed", " err:", err)
	}

	rspMsg.Path = relPhotoImageFilePath


	return f, nil


}




















