package fileHandler

import (
	"os"
	"MarsXserver/tcpserver"
	"MarsXserver/objMessages"
	"MarsXserver/common"
	"MarsXserver/dbserver/app"
	"path/filepath"
	"MarsXserver/proj"
)




type CrawlResourceImageFileHandler struct {

	tcpserver.BaseFileHandlerItem

}

func (this *CrawlResourceImageFileHandler) FileTransStartFunc(ctx *tcpserver.FileReceiveContext) (*os.File, error){

	startMsg := ctx.StartMsg.(*objMessages.CrawlSourceImageRequest)
	rspMsg := ctx.RspMsg.(*objMessages.CrawlSourceImageResponse)

	root := ctx.Conn.Server.GetParentContext()
	dbServer, ok := root.(*app.DbServer)
	if ok == false{
		return nil, common.ErrorLog("parent not a db server")
	}

	picFolder, relPicFolder, err := dbServer.GetSourceImageFolder(startMsg.SrcName)
	if err != nil{
		return nil, common.ErrorLog("get pic folder failed for:", startMsg.SrcName)
	}

	sourcImageFilePath := filepath.Join(picFolder, startMsg.SrcName + filepath.Ext(ctx.LastReq.FileName))
	relSourceImageFilePath := filepath.Join(relPicFolder, startMsg.SrcName + filepath.Ext(ctx.LastReq.FileName))

	if _, err := os.Stat(sourcImageFilePath); err == nil || !os.IsNotExist(err){
		return nil, common.ErrorLog("source pic name conflict", " err:", err)
	}

	sourceImageAbsPath, err := filepath.Abs(sourcImageFilePath)
	if err != nil{
		return nil, common.ErrorLog("get abs path failed", " err:", err)
	}

	ctx.SavePath = sourceImageAbsPath

	f, err := os.OpenFile(sourceImageAbsPath, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil{
		return nil, common.ErrorLog("create file failed", " err:", err)
	}

	rspMsg.SrcPath = relSourceImageFilePath


	return f, nil


}




func (this *CrawlResourceImageFileHandler) FileTransEndFunc(ctx *tcpserver.FileReceiveContext) error{

	oldPath := ctx.SavePath

	fileExt := filepath.Ext(oldPath)

	newPath := oldPath[0: len(oldPath)-len(fileExt)] + proj.ThumbnailImageNameConcat + proj.ThumbnailImageExt

	return proj.GetThumbnailFile(oldPath, newPath)
}




















