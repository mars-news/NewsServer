package fileHandler

import (
	"os"
	"MarsXserver/tcpserver"
	"MarsXserver/objMessages"
	"MarsXserver/common"
	"MarsXserver/dbserver/app"
	"path/filepath"
	"MarsXserver/proj"
)




type CrawlContentImageFileHandler struct {

	tcpserver.BaseFileHandlerItem

}

func (this *CrawlContentImageFileHandler) FileTransStartFunc(ctx *tcpserver.FileReceiveContext) (*os.File, error){

	startMsg := ctx.StartMsg.(*objMessages.CrawlContentImageRequest)
	rspMsg := ctx.RspMsg.(*objMessages.CrawlContentImageResponse)

	root := ctx.Conn.Server.GetParentContext()
	dbServer, ok := root.(*app.DbServer)
	if ok == false{
		return nil, common.ErrorLog("parent not a db server")
	}

	fileUUid, err := common.NewUUID()
	if err != nil{
		return nil, common.ErrorLog("generate uuid failed")
	}

	contentFolder, relContentFolder, err := dbServer.GetNewsPicFileFolder(startMsg.SrcName, startMsg.TimeFolderName)
	if err != nil{
		return nil, common.ErrorLog("get content folder failed", err)
	}

	path := contentFolder + "/" + fileUUid.String() + filepath.Ext(ctx.LastReq.FileName)
	relPath := relContentFolder + "/" + fileUUid.String() + filepath.Ext(ctx.LastReq.FileName)

	absPath, err := filepath.Abs(path)
	if err != nil{
		return nil, common.ErrorLog("get abs path failed", ctx.LastReq.FileName, err)
	}

	ctx.SavePath = absPath

	f, err := os.OpenFile(absPath, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil{
		return nil, common.ErrorLog("create file failed", " err:", err)
	}

	rspMsg.SrcPath = relPath


	return f, nil


}


func (this *CrawlContentImageFileHandler) FileTransEndFunc(ctx *tcpserver.FileReceiveContext) error{

	oldPath := ctx.SavePath

	fileExt := filepath.Ext(oldPath)

	newPath := oldPath[0: len(oldPath)-len(fileExt)] + proj.ThumbnailImageNameConcat + proj.ThumbnailImageExt

	return proj.GetThumbnailFile(oldPath, newPath)
}
























