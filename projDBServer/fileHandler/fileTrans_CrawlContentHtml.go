package fileHandler

import (
	"os"
	"MarsXserver/tcpserver"
	"MarsXserver/objMessages"
	"MarsXserver/common"
	"MarsXserver/dbserver/app"
	"path/filepath"
)




type CrawlContentHtmlFileHandler struct {

	tcpserver.BaseFileHandlerItem

}

func (this *CrawlContentHtmlFileHandler) FileTransStartFunc(ctx *tcpserver.FileReceiveContext) (*os.File, error){

	startMsg := ctx.StartMsg.(*objMessages.CrawlNewsContentRequest)
	rspMsg := ctx.RspMsg.(*objMessages.CrawlNewsContentResponse)

	root := ctx.Conn.Server.GetParentContext()
	dbServer, ok := root.(*app.DbServer)
	if ok == false{
		return nil, common.ErrorLog("parent not a db server")
	}

	fileUUid, err := common.NewUUID()
	if err != nil{
		return nil, common.ErrorLog("generate uuid failed")
	}

	contentFolder, relContentFolder, err := dbServer.GetContentFileFolder(startMsg.SrcName, startMsg.TimeFolderName)
	if err != nil{
		return nil, common.ErrorLog("get content folder failed", err)
	}

	path := contentFolder + "/" + fileUUid.String() + ".news"
	relPath := relContentFolder + "/" + fileUUid.String() + ".news"

	absPath, err := filepath.Abs(path)
	if err != nil{
		return nil, common.ErrorLog("get abs path failed", ctx.LastReq.FileName, err)
	}

	f, err := os.OpenFile(absPath, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil{
		return nil, common.ErrorLog("create file failed", " err:", err)
	}

	rspMsg.SrcPath = relPath


	return f, nil


}



