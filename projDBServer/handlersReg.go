package dbserver

import (
	"MarsXserver/dbserver/app"
	"MarsXserver/dbserver/objHandler"
	"MarsXserver/dbserver/fileHandler"
	"MarsXserver/tcpserver"
	"MarsXserver/objMessages"
	"reflect"
	"MarsXserver/orm"
)

func initRegisterTcpHandlers(aServer *app.DbServer){

	tcpserver.RegisterHandler(
		int(orm.MessageId_DB_Expr),
		&tcpserver.HandlerItem{
			MsgProtoType: tcpserver.MessageProtoType_ObjRequest,
			Handler:      &objHandler.SSDBExprRequestHandler{},
			ReqMsgType:   reflect.TypeOf(orm.DBExprRequest{}),
			RspMsgType:	reflect.TypeOf(orm.DBExprResponse{}),
			IsByPassSecondRspMsgDecoder: true,			//!important receive buffer bytes

		})
}


func initRegisterFileHandlers(aServer *app.DbServer){

	tcpserver.RegisterFileHandler(
		int(objMessages.MessageId_File_CrawlSourceImage),
		&fileHandler.CrawlResourceImageFileHandler{},
		reflect.TypeOf(objMessages.CrawlSourceImageRequest{}),
		reflect.TypeOf(objMessages.CrawlSourceImageResponse{}),
	)

	tcpserver.RegisterFileHandler(
		int(objMessages.MessageId_File_CrawlNewsContent),
		&fileHandler.CrawlContentHtmlFileHandler{},
		reflect.TypeOf(objMessages.CrawlNewsContentRequest{}),
		reflect.TypeOf(objMessages.CrawlNewsContentResponse{}),
	)


	tcpserver.RegisterFileHandler(
		int(objMessages.MessageId_File_CrawlContentImage),
		&fileHandler.CrawlContentImageFileHandler{},
		reflect.TypeOf(objMessages.CrawlContentImageRequest{}),
		reflect.TypeOf(objMessages.CrawlContentImageResponse{}),
	)

	tcpserver.RegisterFileHandler(
		int(objMessages.MessageId_File_UserPhoto),
		&fileHandler.UserPhotoImageFileHandler{},
		reflect.TypeOf(objMessages.UserPhotoImageRequest{}),
		reflect.TypeOf(objMessages.UserPhotoImageResponse{}),
	)

}



func InitRegisterHandlers(aServer *app.DbServer){

	initRegisterTcpHandlers(aServer)
	initRegisterFileHandlers(aServer)
}































