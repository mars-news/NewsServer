package app

import (
	"MarsXserver/common"
	"path/filepath"
	"fmt"
	"os"
	//"io/ioutil"
)

const(
	Default_Perm = 0777
)


func (this *DbServer) GetSourceImageFolder(sourceName string) (folder string, relPath string, err error){

	srcFolder := fmt.Sprintf("%s/%s", this.Config.StaticFilePath, this.Config.CrawlSourceImagePath)
	srcFolderAbs, err := filepath.Abs(srcFolder)
	if err != nil{
		return "", "", common.ErrorLog("get content abs foler failed:", srcFolderAbs)
	}

	if _, err := os.Stat(srcFolderAbs); os.IsNotExist(err){
		os.MkdirAll(srcFolderAbs, Default_Perm)
	}

	relPath = fmt.Sprintf("%s", this.Config.CrawlSourceImagePath)

	return srcFolder, relPath, nil
}


func (this *DbServer) GetContentFileFolder(sourceName string, timeFolderName string) (folder string, relPath string ,err error){

	//timeFolder := time.Now().Format("2006-01-02")

	srcFolder := fmt.Sprintf("%s/%s/%s/%s/%s", this.Config.StaticFilePath, this.Config.CrawlNewsPath, timeFolderName, sourceName, this.Config.CrawlNewsBodyPath)
	srcFolderAbs, err := filepath.Abs(srcFolder)
	if err != nil{
		return "", "", common.ErrorLog("get content abs foler failed:", srcFolderAbs)
	}

	if _, err := os.Stat(srcFolderAbs); os.IsNotExist(err){
		os.MkdirAll(srcFolderAbs, Default_Perm)
	}

	relPath = fmt.Sprintf("%s/%s/%s/%s", this.Config.CrawlNewsPath, timeFolderName, sourceName, this.Config.CrawlNewsBodyPath)

	return srcFolder, relPath, nil
}

func (this *DbServer) GetNewsPicFileFolder(sourceName string, timeFolderName string) (folder string, relPath string,err error){

	//timeFolder := time.Now().Format("2006-01-02")

	srcFolder := fmt.Sprintf("%s/%s/%s/%s/%s", this.Config.StaticFilePath, this.Config.CrawlNewsPath, timeFolderName, sourceName, this.Config.CrawlNewsImagePath)
	srcFolderAbs, err := filepath.Abs(srcFolder)
	if err != nil{
		return "", "", common.ErrorLog("get content pic abs foler failed:", srcFolderAbs)
	}

	if _, err := os.Stat(srcFolderAbs); os.IsNotExist(err){
		os.MkdirAll(srcFolderAbs, Default_Perm)
	}

	relPath = fmt.Sprintf("%s/%s/%s/%s", this.Config.CrawlNewsPath, timeFolderName, sourceName, this.Config.CrawlNewsImagePath)

	return srcFolder, relPath, nil
}


func (this *DbServer) GetUserPhotoFileFolder() (folder string, relPath string,err error){

	srcFolder := fmt.Sprintf("%s/%s", this.Config.StaticFilePath, this.Config.UserPhotoPath)
	srcFolderAbs, err := filepath.Abs(srcFolder)
	if err != nil{
		return "", "", common.ErrorLog("get user photo folder failed:", srcFolderAbs)
	}

	if _, err := os.Stat(srcFolderAbs); os.IsNotExist(err){
		os.MkdirAll(srcFolderAbs, Default_Perm)
	}

	relPath = fmt.Sprintf("%s", this.Config.UserPhotoPath)

	return srcFolder, relPath, nil


}











