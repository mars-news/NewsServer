package objHandler


import (
	"MarsXserver/tcpserver"
	"MarsXserver/common"
	"MarsXserver/objMessages"
	"MarsXserver/orm"
)

type SSDBExprRequestHandler struct{

}


func (handler *SSDBExprRequestHandler) Handle(connector *tcpserver.Connector, header interface{}, body interface{}) error{

	reqHeader := header.(*objMessages.ObjRequestHeader)

	exprReq, ok := body.(*orm.DBExprRequest)
	if ok == false{
		connector.SendObjErrorResponse(reqHeader, -1)
		return common.ErrorLog("body is not expr request")
	}

	exprData := exprReq.Expr

	expr := connector.Server.Orm.NewOrmExprFromExprData(exprData)
	if expr == nil{
		connector.SendObjErrorResponse(reqHeader, -1)
		return common.ErrorLog("from expr data to struct failed")
	}

	res, err := connector.Server.Orm.Run(expr)
	if err != nil{
		connector.SendObjErrorResponse(reqHeader, -1)
		return common.ErrorLog("orm run expr failed")
	}


	rsp := &orm.DBExprResponse{
			Data: res,
	}


	if err := connector.WriteObjResponseMesage(reqHeader,0, rsp); err != nil {
		common.ErrorLog("write msg response error", err, " msg id:", int(orm.MessageId_DB_Expr))
		return err
	}


	return nil

}











