package mDoopServer

import (
	"MarsXserver/mDoopServer/app"
	"MarsXserver/mDoopServer/objHandler"
	"MarsXserver/tcpserver"
	"MarsXserver/objMessages"
	"reflect"
)


func initRegisterTcpHandlers(sServer *app.MDoopServer){


	tcpserver.RegisterHandler(
		int(objMessages.MessageId_Doop_Get_Hour_Recommends),
		&tcpserver.HandlerItem{
			MsgProtoType: tcpserver.MessageProtoType_ObjRequest,
			Handler:      &objHandler.SSGetHourRecommendsHandler{},
			ReqMsgType:   reflect.TypeOf(objMessages.SSDoopGetHourRecommendRequest{}),
			RspMsgType:   reflect.TypeOf(objMessages.SSDoopGetHourRecommendResponse{}),
		})


	tcpserver.RegisterHandler(
		int(objMessages.MessageId_Doop_Start_Calc),
		&tcpserver.HandlerItem{
			MsgProtoType: tcpserver.MessageProtoType_ObjRequest,
			Handler:      &objHandler.SSStartCalcHandler{},
			ReqMsgType:   reflect.TypeOf(objMessages.SSDoopStartCalcRequest{}),
			RspMsgType:   reflect.TypeOf(objMessages.SSDoopStartCalcResponse{}),
		})
}


func initRegisterHttpHandlers(aServer *app.MDoopServer){

	HttpHandlers := make(map[string]interface{})

	aServer.RegisterHttpHandlers(HttpHandlers)
}



func initRegisterFileHandlers(aServer *app.MDoopServer){



}

func InitRegisterHandlers(aServer *app.MDoopServer){

	initRegisterTcpHandlers(aServer)
	initRegisterFileHandlers(aServer)
	initRegisterHttpHandlers(aServer)

}

