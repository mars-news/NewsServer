package objHandler


import (
	"MarsXserver/tcpserver"
	"MarsXserver/common"
	"MarsXserver/objMessages"
	"MarsXserver/mDoopServer/app"
)

type SSGetHourRecommendsHandler struct{

}


func (handler *SSGetHourRecommendsHandler) Handle(connector *tcpserver.Connector, header interface{}, body interface{}) error{

	var errAll error
	var ok bool
	var uid int
	var hourIdx int64

	var recs []int

	var req *objMessages.SSDoopGetHourRecommendRequest
	var rsp *objMessages.SSDoopGetHourRecommendResponse

	reqHeader := header.(*objMessages.ObjRequestHeader)

	suInf := connector.Server.GetParentContext()
	server, ok := suInf.(*app.MDoopServer)
	if !ok {
		errAll = common.ErrorLog("doop server is not in parent context")
		goto output
	}

	req, ok = body.(*objMessages.SSDoopGetHourRecommendRequest)
	if ok == false{
		errAll = common.ErrorLog("body is not get recommends request")
		goto output
	}

	uid = req.Uid
	hourIdx = req.HourIdx

	recs = server.GetUserHourRecommend(uid, hourIdx)

	if recs == nil{
		recs = []int{}
	}


output:

	rsp = &objMessages.SSDoopGetHourRecommendResponse{
		Recs: recs,
	}

	if errAll != nil{

		rsp.Stat = -1
	}

	if err := connector.WriteObjResponseMesage(reqHeader,0, rsp); err != nil {
		common.ErrorLog("write msg response error", err, " msg id:", int(objMessages.MessageId_Doop_Get_Hour_Recommends))
		return err
	}


	return nil

}











