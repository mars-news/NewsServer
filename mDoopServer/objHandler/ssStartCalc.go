package objHandler


import (
	"MarsXserver/tcpserver"
	"MarsXserver/common"
	"MarsXserver/objMessages"
	"MarsXserver/mDoopServer/app"
)

type SSStartCalcHandler struct{

}


func (handler *SSStartCalcHandler) Handle(connector *tcpserver.Connector, header interface{}, body interface{}) error{

	var errAll error
	var ok bool

	var req *objMessages.SSDoopStartCalcRequest
	var rsp *objMessages.SSDoopStartCalcResponse

	reqHeader := header.(*objMessages.ObjRequestHeader)

	suInf := connector.Server.GetParentContext()
	server, ok := suInf.(*app.MDoopServer)
	if !ok {
		errAll = common.ErrorLog("doop server is not in parent context")
		goto output
	}

	req, ok = body.(*objMessages.SSDoopStartCalcRequest)
	if ok == false{
		errAll = common.ErrorLog("body is not get recommends request")
		goto output
	}

	if req.Day{
		server.DaySimilarityCalc()
	}
	if req.Hour{
		server.HourRecommendCalc(req.Before)
	}


output:

	rsp = &objMessages.SSDoopStartCalcResponse{
	}

	if errAll != nil{
		rsp.Stat = -1
	}

	if err := connector.WriteObjResponseMesage(reqHeader,0, rsp); err != nil {
		common.ErrorLog("write msg response error", err, " msg id:", int(objMessages.MessageId_Doop_Start_Calc))
		return err
	}


	return nil

}











