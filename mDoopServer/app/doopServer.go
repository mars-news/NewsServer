package app

import (
	"MarsXserver/orm"
	"MarsXserver/common"
	"MarsXserver/hybridServer"
	"MarsXserver/cfmodule"
	"container/list"
	"MarsXserver/proj"
)


const (
	Dialer_Type_DB      string = "db"
	Dialer_Type_Red	    string = "red"
)


var (
	All_Dialers = []string{Dialer_Type_DB, Dialer_Type_Red}
)



type MDoopServer struct{
	hybridServer.HybridBaseServer

	simCf *cfmodule.CFAlgo
	recCfs map[proj.HourIdx]*cfmodule.CFAlgo //todo: to mmap
	recCfKeys *list.List
	simHasReadFromDBDayCache map[int]bool

	Sid	int
	Sname string
	Config *MDoopServerConfigData
}



func NewDoopServer(configName string) (*MDoopServer, error){

	config_, err := LoadConfig(configName)
	if err != nil{
		return nil, common.ErrorLog("load Config failed", err)
	}

	doopServer := &MDoopServer{
		Sid:    config_.Sid,
		Sname:  config_.Name,
		recCfs:  make(map[proj.HourIdx]*cfmodule.CFAlgo),
		recCfKeys: list.New(),
		simHasReadFromDBDayCache: make(map[int]bool),
		Config: config_,
	}

	doopServer.simCf = cfmodule.NewCFAlgo()

	doopServer.InitBaseServer(config_.Name, doopServer, &config_.TConfig, nil, nil, &config_.RConfig)

	doopServer.Orm.RegisterSendFunc(doopServer.OrmSendFunc)
	doopServer.Orm.RegisterBytesRspFunc(orm.OrmHandleBytesRspFunc)

	doopServer.TServer.SetLegalDialerNames(All_Dialers)

	doopServer.AddGcInfoTask("gc", GC_Interval, doopServer.GC)
	doopServer.AddGcInfoTask("user read cache info", cfmodule.User_Read_Cache_Info_Time , cfmodule.UReadInfo)
	doopServer.AddGcInfoTask("user read cache gc", cfmodule.User_Read_Cache_GC_Time, cfmodule.UReadGC)

	return doopServer, nil
}




func (this *MDoopServer) OrmSendFunc(expr *orm.XOrmEprData, retCh chan interface{}){

	this.OrmSendFuncByType(Dialer_Type_DB, expr, retCh)
}




func (this *MDoopServer) Run(blockMode bool) error{

	this.LoopSchedule()

	return this.RunBaseServer(blockMode)
}























