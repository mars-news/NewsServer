package app

import (
	"MarsXserver/objMessages"
	"time"
	"MarsXserver/common"
	"sync"
	"MarsXserver/db"
	"MarsXserver/cfmodule"
	"strconv"
	"github.com/robfig/cron"
	"MarsXserver/proj"
)

const (

	Collect_Day_Ratings_Time_Sleep = time.Second * 2

	Day_Sim_Calc_Interval = time.Hour * 24

	Hour_Recommend_Calc_Interval = proj.Collect_Ratings_Time_Span

	GC_Interval = Hour_Recommend_Calc_Interval
)



func (this *MDoopServer) LoopSchedule(){

	common.InfoLog("doop schedule start")

	c := cron.New()
	c.AddFunc("1 1 * * * *", func() {
		common.InfoLog("hour recCalc start...")
		this.HourRecommendCalc(0)
	})

	c.AddFunc("1 0 3 * * *", func() {
		common.InfoLog("day recCalc start...")
		this.DaySimilarityCalc()
	})
	c.Start()


	/*

	daySimCalcTick := time.Tick(Day_Sim_Calc_Interval)
	hourRecCalcTick := time.Tick(Hour_Recommend_Calc_Interval)

	for{
		common.InfoLog(this.Sname, " scheduling...", time.Now())

		select {
		case <- daySimCalcTick:
			common.InfoLog("daySimCalc start...")

			this.DaySimilarityCalc()

		case <- hourRecCalcTick:
			common.InfoLog("hour recCalc start...")

			this.HourRecommendCalc(0)
		}
	}*/

}

/* wrong
func dayHourIdxDiff(tmp1, tmp2 *objMessages.HourIdx) int{

	modN := 24/common.Collect_Ratings_Time_Span

	day1 := tmp1.Day
	day2 := tmp2.Day

	hour1 := tmp1.HourIdx
	hour2 := tmp2.HourIdx

	var resDay, resHourIdx int

	if hour1 < hour2{

		resHourIdx = hour1 + modN - hour2
		resDay = day1 - day2 -1
	}else{
		resHourIdx = hour1 - hour2
		resDay = day1 - day2
	}

	return resDay * modN + resHourIdx
}*/







func (this *MDoopServer) GC() {

	defer func(){

		if err:= recover(); err != nil{
			common.ErrorLog(this.Sname, this.Sid, " panic:", err)
		}

	}()

	common.InfoLog("doop gc start...")

	modN := int(proj.Collect_Day_Time_Spon/proj.Collect_Ratings_Time_Span)

	common.InfoLog("modN:", modN)

	delCnt := this.recCfKeys.Len() - modN

	if  delCnt < 0 {

		return

	}

	for ii := 0; ii < delCnt; ii++{

		idx := this.recCfKeys.Front().Value.(*proj.HourIdx)

		common.InfoLog("remove rec cache houridx:", idx.HourIdx)

		delete(this.recCfs, *idx)


	}




}








func (this *MDoopServer) collectAllRatingsByHourIdx(dayHourIdx *proj.HourIdx, resCh chan []objMessages.SSRatingUserItem, outErr *error){

	defer func(){
		if err:= recover(); err != nil{
			close(resCh)
			*outErr = common.ErrorLog("collect hour panic", err)
		}
	}()

	common.InfoLog("collecting hourIdx ", dayHourIdx.HourIdx)

	var errAll error
	var offset = 0


	for{
		common.InfoLog("collection all ratings, offset:", offset, "hourIdx:", dayHourIdx.HourIdx)

		collectRatingsReq := &objMessages.SSCollectRatingsRequest{
			Offset:offset,
			HourIdx:dayHourIdx.HourIdx,
		}

		ret, err := this.WriteObjRequestMessageToRedSync(collectRatingsReq)
		if err != nil{
			errAll = common.ErrorLog("collect ratings failed", err)
			goto errorRoute
		}

		rsp, ok := ret.(*objMessages.SSCollectRatingsResponse)
		if ok != true{
			errAll = common.ErrorLog("rsp is not collect items")
			goto errorRoute
		}

		resCh <- rsp.Records

		offset = rsp.Offset

		if offset <= 0{
			break
		}

		time.Sleep(Collect_Day_Ratings_Time_Sleep)

	}


errorRoute:
	close(resCh)
	*outErr = errAll


}


func (this *MDoopServer) collectAllDayRatings(resCh chan []objMessages.SSRatingUserItem, outErr *error){

	defer func(){
		if err:= recover(); err != nil{
			close(resCh)
			*outErr = common.ErrorLog("collect daily panic", err)
		}
	}()


	var errAll error

	dayHourIdxs := proj.HourIdxGetPrevDayIdxs()

	for _, dayHourIdx := range dayHourIdxs{

		common.InfoLog("ratings day collect", dayHourIdx)

		hourCh := make(chan []objMessages.SSRatingUserItem)
		var hourErr error

		go this.collectAllRatingsByHourIdx(&dayHourIdx, hourCh, &hourErr)

		for hourRes := range hourCh{

			resCh <- hourRes
		}

		if hourErr != nil{
			errAll = common.ErrorLog("collect day error", hourErr, dayHourIdx)
			goto errorRoute
		}

	}

errorRoute:

	close(resCh)
	*outErr = errAll

}


func (this *MDoopServer) generateNeighborRoutine(uid int, syncGroup *sync.WaitGroup){

	defer syncGroup.Done()

	neighbors :=  this.simCf.GenerateNeighbors(uid)

	bts, err := common.EncodeObjectPacket(&neighbors)

	if err != nil{
		common.ErrorLog("encode obj packet failed", err)
		return
	}

	simStr := strconv.QuoteToASCII(string(bts))

	userSim := &db.UserSimilarity{
		Id:           int32(uid),
		Similarities: simStr,
	}




	if  err := this.Orm.Update(userSim); err != nil{
		common.ErrorLog("update user sim failed", err, uid);
	}

}


func (this *MDoopServer) getUserNeighborsFromDB(uid int) cfmodule.UserDistSlice{

	sim := &db.UserSimilarity{
		Id: int32(uid),
	}

	if err := this.Orm.Read(sim, true); err != nil{
		return nil
	}

	simStr := sim.Similarities

	if len(simStr) <= 0 {
		return nil
	}

	simOriStr, err := strconv.Unquote(simStr)
	if err != nil{
		common.ErrorLog("unquote user sim error, uid:", uid, err)
		return nil
	}

	sims := make(cfmodule.UserDistSlice, 0)

	if err := common.DecodeObjectPacketFromBytes([]byte(simOriStr), &sims); err != nil{
		common.ErrorLog("decode user sim failed", err)
		return nil
	}

	return sims

}


func (this *MDoopServer) generateHourRecommendRoutine(uid int, recCf *cfmodule.CFAlgo , syncGroup *sync.WaitGroup){

	defer syncGroup.Done()

	common.DebugLog("hour calc user:", uid)

	simNeighbors := this.simCf.GetNeighbors(uid)

	if simNeighbors == nil {

		common.DebugLog("hour calc no simNeighbors, uid", uid)

		if res, ok := this.simHasReadFromDBDayCache[uid]; ok == false || res == false{

			simNeighbors = this.getUserNeighborsFromDB(uid)
			this.simHasReadFromDBDayCache[uid] = true

			if simNeighbors != nil && len(simNeighbors) > 0{

				this.simCf.SetNeighbors(uid, simNeighbors)
			}

			common.DebugLog("hour calc get neighbors from db, uid", uid, " db value:", simNeighbors)

		}
	}


	if simNeighbors == nil{

		simNeighbors = recCf.GetNeighbors(uid)

		common.DebugLog("hour calc get neighbors 2rd, uid", uid, " count:", len(simNeighbors))

		if simNeighbors == nil{

			simNeighbors = recCf.GenerateNeighbors(uid)
			if len(simNeighbors) <= 0 || simNeighbors == nil{
				common.ErrorLog("no hour neighbors for user:", uid)
				return
			}
		}else if len(simNeighbors) <= 0{

			common.ErrorLog("hour neigbors len should not be 0")
			return
		}
	}else if len(simNeighbors) <= 0{
		common.ErrorLog("day neighbors len should not be 0")
		return
	}

	recSlice := recCf.RecommendNews(uid, simNeighbors)

	common.DebugLogPlus("hour suggest for user", uid, " arr", recSlice)


	if recSlice == nil || len(recSlice) == 0{

		common.ErrorLog("hour recommend for user", uid, "failed")
		return

	}

}




func (this *MDoopServer) DaySimilarityCalc(){

	this.simCf.Reset()

	resCh := make(chan []objMessages.SSRatingUserItem)
	var resErr error

	go this.collectAllDayRatings(resCh, &resErr)

	for res := range resCh{
		this.simCf.AddRatingsRawData(res)
	}

	if resErr != nil{
		common.ErrorLog("collect day rating data failed")
		return
	}


	uids := this.simCf.GetUids()

	var syncGroup sync.WaitGroup

	for _, uid := range uids{

		syncGroup.Add(1)

		go this.generateNeighborRoutine(uid, &syncGroup)
	}


	syncGroup.Wait()


	common.InfoLog("calc daily similarity finished", common.MakeDBTimeString(time.Now()))
}


func (this *MDoopServer) HourRecommendCalc(before int) {

	dayHourIdx := proj.HourIdxGetNearestPrevIdx()

	dayHourIdx.HourIdx -= int64(before)

	this.recCfs[*dayHourIdx] = cfmodule.NewCFAlgo()

	recCf := this.recCfs[*dayHourIdx]

	resCh := make(chan []objMessages.SSRatingUserItem)
	var resErr error

	go this.collectAllRatingsByHourIdx(dayHourIdx, resCh, &resErr)

	infoRatingsCount := 0

	for res := range resCh{

		recCf.AddRatingsRawData(res)
		infoRatingsCount += len(res)

	}

	common.InfoLog("doop hour calc ratings count:" , infoRatingsCount, " hourIdx:", dayHourIdx.HourIdx)

	if resErr != nil{
		common.ErrorLog("collect hour data failed")
		return
	}

	uids := recCf.GetUids()

	var syncGroup sync.WaitGroup

	for _, uid := range uids{

		syncGroup.Add(1)

		go this.generateHourRecommendRoutine(uid, recCf, &syncGroup)

	}

	syncGroup.Wait()

	this.recCfKeys.PushBack(dayHourIdx)

	common.InfoLog("hour recommend calc finished , houridx", dayHourIdx.HourIdx)


}


func (this *MDoopServer) GetUserHourRecommend(uid int, hourIdx int64) []int {

	dayHourIdx := proj.HourIdx{
		HourIdx: hourIdx,
	}


	recCf, ok := this.recCfs[dayHourIdx]

	if ok == false{
		return []int{}
	}

	recs := recCf.GetRecs(uid)

	if recs == nil || len(recs) <= 0{
		return nil
	}

	res := make([]int, len(recs))

	for ii, rec := range recs{

		res[ii] = rec.Nid
	}


	return res

}



















































