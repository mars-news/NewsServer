package app


func (this *MDoopServer) WriteObjRequestMessageDBSync(body interface{}) (interface{}, error){

	return this.TServer.WriteObjRequestMessageToServerByTypeSync(Dialer_Type_DB, body)

}

func (this *MDoopServer) WriteObjRequestMessageToRedSync(body interface{}) (interface{}, error){

	return this.TServer.WriteObjRequestMessageToServerByTypeSync(Dialer_Type_Red, body)

}