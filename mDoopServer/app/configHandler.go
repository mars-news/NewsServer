package app



import (
	"MarsXserver/tcpserver"
	"path/filepath"
	"MarsXserver/common"
	"io/ioutil"
	"encoding/xml"
	"MarsXserver/redmodule"
)

type MDoopServerConfigData struct{
	Sid	int 		`xml:"sid"`
	Name	string		`xml:"name"`

	TConfig tcpserver.TcpConfigData	`xml:"tconfig"`
	RConfig redmodule.RedisConfigData `xml:"rconfig"`
}



func LoadConfig(name string) (configData *MDoopServerConfigData, err error){

	configData = &MDoopServerConfigData{}

	fileName := filepath.Join(common.DefaultConfPath, name + ".xml")

	content, err := ioutil.ReadFile(fileName)
	if err!= nil{
		common.ErrorLog("read Config failed", err)
		return nil, err
	}

	if err = xml.Unmarshal(content, configData); err != nil{
		common.ErrorLog("unmarshal error", err)
		return nil, err
	}

	configData.RConfig.Trim()


	return configData, nil
}























